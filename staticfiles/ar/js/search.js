
// Import stylesheets
import Trie from "./trie.js";
import countries from "./countries.js";

const input = document.getElementById("search");
const results = document.getElementById("results");
const trie = new Trie();

console.log(trie)
// Load up the trie with keys frp, the 'words' variable.
const words = countries.map(country => country.name.toLowerCase());
words.forEach(word => trie.insert(word));

/**
 * Helper function to create new <p> elements to be added to
 * the results list.
 * @param {String} str The string to be added to the new elements
 * innerHTML for display.
 * @returns {HTMLParagraphElement}
 */
const newEl = str => {
  const p = document.createElement("p");
  p.classList = "results";
  p.innerText = str;
  return p;
};
input.addEventListener("keyup", ev => {
  // Get a list of countries with the current value of #input.
  const countriesResults = trie.autoComplete(input.value.toLowerCase());
  console.log(input.value)
  console.log(results)
  results.innerHTML = ""; 
  if (countriesResults.found) {
    countriesResults.found.forEach(country => {
      const el = newEl(country);
      results.appendChild(el);
    });
  }
});
