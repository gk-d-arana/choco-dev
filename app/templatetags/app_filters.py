from django import template
from app.models import Item
import decimal

register = template.Library()


@register.filter(name="ar_replace_date")
def ar_replace_date(value):

    if value.strftime("%p") == "AM":
        return value.strftime("%I %p").replace("AM", "ص")
    else:
        return value.strftime("%I %p").replace("PM", "م")


@register.filter(name="replace_date")
def replace_date(value):

    if value.strftime("%p") == "AM":
        return value.strftime("%I %p")
    else:
        return value.strftime("%I %p")






@register.filter(name="get_price")
def get_price(value, id):

    item = Item.objects.get(id=id)


    if item.quantity_type == "Weight":
        min = item.weight_minimum
        price = min / decimal.Decimal(0.250) * item.price
    else:
        min = item.pieces_minimum
        price = min * item.price
    
    return price
    


@register.filter(name="sort")
def sort(value, key):

    return sorted(value, key = lambda i: i[key],reverse=True)
