import json
from cloudinary import CloudinaryImage
from django.db.models.fields.mixins import FieldCacheMixin
from django.db.models import FloatField
from django.shortcuts import redirect, render, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.forms.models import model_to_dict
from django.utils import timezone
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.contrib import messages
from django.template.loader import render_to_string
from django.utils.formats import date_format
from django.views.generic import View, UpdateView, CreateView
from django.conf import settings
from django.core import mail, serializers
from django.core.mail import send_mail
from django.db import IntegrityError
from django.db.models import When, Case
from django.core.files import File
from io import BytesIO, StringIO
import cloudinary.uploader
import random
import string
from django.contrib.auth.password_validation import validate_password

from django.utils.html import strip_tags
from urllib.parse import urlencode
import datetime
from django.utils.formats import date_format

import urllib.request as urllib

from .models import Color, Discount, AboutUsContent, AboutUsMainImage, AboutUsTopic, Address, BannerImage, Box, BoxItem, Card, Category, ChocolateType, ContactUs, Coupon, DeliveryCharge, DeliveryDate, Flavor, Flower, Item, MinimumOrder, Occasion, OrderItem, Order, Package, PickupPlace, PopUp, Profile, OrderBoxItem, BoxImage, Ribbon, SubscribedUser, Topper, Transaction, Tray, User, Visit, WorkingDay, DateOff
from .forms import ArChangePasswordForm, ArProfileForm, ArResetPasswordForm, ArUserForm, ChangePasswordForm, ResetPasswordForm, UserForm, ProfileForm, ChocolateTypeForm, QuantityChoiceForm, CheckoutForm, BoxImageForm, AddCategoryItem
from django.db.models import Sum
import requests
import decimal
from django.urls import resolve

from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.utils.translation import gettext as _


from .serializers import DatesSerializer
import cloudinary.uploader


import os

def random_digit(n):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return random.randint(range_start, range_end)

def add_check(request, id):
    print("add-check")
    order_item = OrderItem.objects.get(id=id)
    if order_item.quantity_type == "Pieces":
        if order_item.pieces_quantity < order_item.item.pieces_maximum:
            order_item.pieces_quantity += 1
    else:
        if order_item.weight_quantity < order_item.item.weight_maximum:
            order_item.weight_quantity += decimal.Decimal(0.25)
            order_item.pieces_quantity += 1

    order_item.save()
    return JsonResponse({"item": "item"}, status=200)

def remove_check(request, slug):
    item = Item.objects.get(slug=slug)
    order_item = OrderItem.objects.get(item=item, user=request.user)
    order = Order.objects.get(user=request.user, ordered=False)
    if order_item.quantity > 1:
        order_item.quantity -= 1
        order_item.save()
    else :
        order.items.remove(order_item)
        order_item.delete()
        order.save()
    lst = []
   
    return JsonResponse({"item": "item"}, status=200)


def remove_box_check(request, slug):
    print("f")
    item = Item.objects.get(title="Customize your box")
    order_item = OrderItem.objects.get(item=item, user=request.user, slug=slug)
    order = Order.objects.get(user=request.user, ordered=False)
    if order_item.quantity > 1:
        print("ff")
        order_item.quantity -= 1
        order_item.save()
    else:
        print('ffff')
        order.items.remove(order_item)
        order_item.delete()
        order.save()
    lst = []
    try:
        for i in order_item.box_items.all():
            lst.append(i.item.title)
    except:
        print("hello")

    return JsonResponse({"item": lst}, status=200)




@login_required
def ajax_add(request, id):
    

    
    if request.user.is_authenticated:
        item = get_object_or_404(Item, id=id)
    order_item, created = OrderItem.objects.get_or_create(
        user=request.user,
        item=item,
        ordered=False
    )

    
    if request.user.is_authenticated:
        order_qs = Order.objects.filter(user=request.user, ordered=False)

        choice = request.POST.get("choice")
        flavor = request.POST.get("flavor")
        color = request.POST.get("color")
        if color == "undefined":
            color = None
        
        if flavor == "undefined":
            flavor = None
        print(choice, "CHOICE")
        print(request.POST)
        if order_qs.exists():
            order = order_qs[0]

            if order.items.filter(item__id=item.id, choice=choice, flavor=flavor, color=color).exists():
                print("hello")
                if item.quantity_type == "Weight":
                    order_item.weight_quantity += decimal.Decimal(request.POST.get("quantity").split(" ")[0])
                    order_item.pieces_quantity = float(order_item.weight_quantity) / 0.25
                elif item.quantity_type == "Pieces":
                    order_item.pieces_quantity += int(request.POST.get("quantity"))
                order_item.save()
                Item.objects.all().update(pieces_quantity=1, weight_quantity=0.25)
                return JsonResponse({"item": model_to_dict(order_item)}, status=200)
            else:
                if choice:
                    order_item.has_choice = True
                    order_item.choice = choice
                if color:
                    if Color.objects.filter(id=color).exists():
                        order_item.color = Color.objects.get(id=color)

                if flavor:
                    if Flavor.objects.filter(id=flavor).exists():
                        order_item.flavor = Flavor.objects.get(id=flavor)
                    
                if item.quantity_type == "Weight":
                    quantity = float(request.POST.get('quantity').split(" ")[0])
                    order_item.weight_quantity = quantity
                    order_item.pieces_quantity = quantity / 0.25
                elif item.quantity_type == "Pieces":
                    quantity = int(request.POST.get("quantity"))
                    order_item.pieces_quantity = quantity

                order_item.quantity_type = item.quantity_type               
                order_item.price = item.price
                print("package set")
                order_item.save()   
                order.items.add(order_item)
                order.save()


                Item.objects.all().update(pieces_quantity=1, weight_quantity=0.25)

                return JsonResponse({"item": model_to_dict(order_item)}, status=200)
        else:
            print("hoo")
            
            order = Order.objects.create(
                user=request.user,
            )
            

            order.save()
            order_item.quantity = int(request.POST.get('quantity'))
            if choice:
                order_item.has_choice = True
                order_item.choice = choice
            if color:
                if Color.objects.filter(id=color).exists():
                    order_item.color = Color.objects.get(id=color)

            if flavor:
                if Flavor.objects.filter(id=flavor).exists():
                    order_item.flavor = Flavor.objects.get(id=flavor)
            order_item.save()
            order.items.add(order_item)
            order.save()
            return JsonResponse({"item": model_to_dict(order_item)}, status=200)
    else: 
        print('hello')
        return redirect("login")

def add_category_item(request, id):
    

    if request.method == "POST":
        form = AddCategoryItem(request.POST, request.FILES)

        if form.is_valid():
            item = Item.objects.create(title=form.cleaned_data["title"], price=form.cleaned_data["price"], slug=form.cleaned_data["slug"], category=Category.objects.get(id=id))
            item.description = form.cleaned_data["description"]
            item.image = request.FILES["picture"]
            item.save()
        else:
            print(form.errors)
    else:
        form = AddCategoryItem()
        return render(request, 'add-category-item.html', {"form": form, "opts": None})



def about_us(request):

    about_us_content = AboutUsContent.objects.all()[0]
    topics = AboutUsTopic.objects.all().order_by("number")

    if request.user.is_authenticated:
        try:
            order = Order.objects.get(user=request.user, ordered=False)
            order.order_type = None
            order.coupon = None
            order.save()
        except Order.DoesNotExist:
            order = Order.objects.create(user=request.user, ordered=False)
    else:
        order = None

    if request.method == "POST":
        name = request.POST.get("name")
        email = request.POST.get("email")
        message = request.POST.get("message")

        ContactUs.objects.create(name=name, email=email, message=message)

        return redirect("about-us")

    
    context = {
        "user": request.user,
        "order": order,
        "content": about_us_content,
        "topics": topics,
    }

    return render(request, 'about.html', context)



def contact_us_post(request):
    name = request.POST.get("name")
    email = request.POST.get("email")
    message = request.POST.get("message")

    ContactUs.objects.create(name=name, email=email, message=message)

    return JsonResponse({"status": "success"})


def index(request):

    print(request.GET.get("customized_ordered"))
    if request.user.is_authenticated:
        print(request.user.username, "USERNAME")


    
    Visit.objects.create()



    

    categories = Category.objects.all().order_by("number")
    if request.user.is_authenticated:
        try:
            order = Order.objects.get(user=request.user, ordered=False)
            order.coupon = None
            order.order_type = None
            order.note = None
            order.save()
        except Order.DoesNotExist:
            order = Order.objects.create(user=request.user, ordered=False)
            
        print(order.id, "Order Id")
        item = Item.objects.get(title="Customize your box")
        order_items = OrderItem.objects.filter(user=request.user, item=item)
        for i in order_items:
            if not i.price:
                print(i.id)
                i.delete()
                print("deleted Ohhh")
    else:
        
        username = ''.join(random.choice(string.ascii_lowercase) for i in range(10))

        user = User.objects.create(username=username, password='lkasjdhflksajdhflkLKJHLkdjhf')

        print(username)
        login(request, user)

        order = Order.objects.create(user=user)


    pop_up = PopUp.objects.first()
    
    context = {
        'order': order,
        'categories': categories,
        'customized_ordered': request.GET.get("customized_ordered"),
        'checkout': request.GET.get("checkout"),
        'checkout_failed': request.GET.get("checkout_failed"),
        "banner_image": BannerImage.objects.first(),
        "pop_up": pop_up
    }
    if not pop_up.disabled:
        if request.user.is_authenticated and request.user.password != "lkasjdhflksajdhflkLKJHLkdjhf":
            profile = request.user.profile
            if not profile.has_seen_popup:
                context['show_popup'] = True
                profile.has_seen_popup = True
                request.session['has_seen_popup'] = True
                profile.save()
        else:
            if not request.session.get('has_seen_popup', True):
                request.session['has_seen_popup'] = True
                context['show_popup'] = True


    if request.method == "POST":
        request.session["name"] = request.POST.get("name")
        request.session["number"] = request.POST.get("number")




        print(request.session["name"])
        print(request.session["number"])


        return redirect("visitor")




    return render(request, 'index.html', context)


def add_visits(request):
    visits = Visit.objects.first()
    visits.numbers += 1
    visits.save()
    return JsonResponse({"status": "sucess"})

def detail_view(request, id):
    if request.user.is_authenticated:
        pass
    else:
        username = ''.join(random.choice(string.ascii_lowercase) for i in range(10))

        user = User.objects.create(username=username, password='lkasjdhflksajdhflkLKJHLkdjhf')

        print(username)
        login(request, user)

        order = Order.objects.create(user=user)
    item = get_object_or_404(Item, id=id)

    packages = Package.objects.all()

    items = Item.objects.exclude(id=id).filter(category=item.category)
    if request.user.is_authenticated:
        order = Order.objects.get(user=request.user, ordered=False)
    else:
        order = None
    context = {
        'item': item,
        'order': order,
        'packages': packages,

    }

    return render(request, 'detail.html', context)





def custom_box_done(request):
    item = Item.objects.get(title="Customize your box")
    
    order_item = OrderItem.objects.get(
        user=request.user,
        item=item,
        ordered=False
    )

    items = BoxItem.objects.filter(type=order_item.chocolate_type)

    
    
    return redirect("home")





def box_items_choice_post(request):
    price = 0
    if request.POST.get("tray"):
        
        tray = Tray.objects.get(slug=request.POST.get("tray"))
        price += tray.price
        print(tray, "TRAY")
    else:
        tray = None
    if request.POST.get("topper"):
        print(request.POST.get("topper"))
        topper = Topper.objects.get(slug=request.POST.get("topper"))
        price += topper.price
    else:
        topper = None
    if request.POST.get("flower"):
        flower = Flower.objects.get(slug=request.POST.get("flower"))
        price += flower.price
        
    else:
        flower = None
    if request.POST.get("ribbon"):
        ribbon = Ribbon.objects.get(slug=request.POST.get("ribbon"))
        price += ribbon.price

    else:
        ribbon = None
    if request.POST.get("card"):
        card = Card.objects.get(slug=request.POST.get("card"))
        price += card.price
    else:
        card = None
    print(card)
    

    print(price, "PRICE")
    item = Item.objects.get(title="Customize your box")
    order_items = OrderItem.objects.filter(user=request.user, item=item)
    order_item = OrderItem.objects.create(
        user=request.user,
        item=item,
        tray=tray,
        topper=topper,
        flower=flower,
        ribbon=ribbon,
        card=card

    )
    order_item.save()

    print(order_item.chocolate_type, "CHOCOLATE TYPE")
    print(order_item.last, "ORDER ITEM")

    if order_item.chocolate_type == "Bars" or order_item.chocolate_type == "Pralines":
        items = BoxItem.objects.filter(type=order_item.chocolate_type)
    else:
        items = BoxItem.objects.all()

    order = Order.objects.get(
        user=request.user,
        ordered=False
    )

    
    
    lst = []
    items_list = []
    
    
    for i in BoxItem.objects.all():

        if i.type in tray.type:
            pieces_quantity = float(request.POST.get("quantity-"+ i.slug).split()[0]) / 0.25
            quantity = float(request.POST.get("quantity-"+i.slug).split()[0])
            print(request.POST.get("quantity-"+i.slug))
            print(quantity, "Quantity")
            if float(quantity) > 0:
                print(quantity, decimal.Decimal(quantity), "QUANTITY BEFORE")
                print(i.price, decimal.Decimal(i.price), "I PRICE")
                print(price, decimal.Decimal(price), "PRICE BEFORE")


                price += decimal.Decimal(pieces_quantity) * decimal.Decimal(i.price)
                print(price, decimal.Decimal(price), "PRICE AFTER")
                order_box_item = OrderBoxItem.objects.create(
                    user=request.user,
                    item=i,
                )
                print(quantity, decimal.Decimal(quantity), "QUANTITY AFTER")

                order_box_item.quantity = quantity
                print(order_box_item.quantity, decimal.Decimal(order_box_item.quantity), "ITEM QUANTITY AFTER")
                lst.append(order_box_item.quantity)
                items_list.append(order_box_item)
                print("ADDEDDD")
    print(items_list)
    print("hoooo")
        
    for item in items_list:
        item.save()
        order_item.box_items.add(item)
    order_item.price = price
    order_item.save()
        
   
    print(len(order_items))
    
    print("oh")
        
    for i in order.items.all():
        

        one = []
        two = []

        
        for el in i.box_items.all():
            if el.item.quantity > 0:
                one.append(el.item.title)
                one.append(el.quantity)
        print(order_item)
        for ele in order_item.box_items.all():
            if ele.item.quantity > 0:
                two.append(ele.item.title)
                two.append(ele.quantity)


        print(one)
        print(two)
        
        
        print("nice")
        if i.tray == order_item.tray and i.topper == order_item.topper and i.ribbon == order_item.ribbon and i.flower == order_item.flower and i.card == order_item.card and one == two:
            
            print(3)
            print(i)
            print(order_item)
            print(i.chocolate_type)
            print(order_item.chocolate_type)

            print("yo yo man")
            i.pieces_quantity += 1
            i.save()
            order_item.delete()
            return redirect("home")

    
    order.items.add(order_item)
    order.save()

            
        
    return redirect('home')
        
                
                
    

    

def add_box_quantity(request, slug):
    order_item = OrderItem.objects.get(slug=slug, user=request.user)

    order_item.quantity += 1
    order_item.save()
    return redirect('cart')


def remove_box_quantity(request, id):
    order = Order.objects.get(user=request.user, ordered=False)
    order_item = OrderItem.objects.get(id=id)

    if order_item.pieces_quantity > 1:
        order_item.pieces_quantity -= 1
        order_item.save()
    
    
    return redirect('cart')





def occasions_list(request):
    occasions = Occasion.objects.all()

    context = {
        "occasions": occasions
    }

    return render(request, "occasions.html", context)
    



def chocolate_type(request):
    item = Item.objects.get(title="Customize your box")
    if request.user.is_authenticated:
        try:
            order = Order.objects.get(user=request.user, ordered=False)
            order.coupon = None
            order.order_type = None
            order.save()
        except Order.DoesNotExist:
            order = Order.objects.create(user=request.user, ordered=False)
    else:
        username = ''.join(random.choice(string.ascii_lowercase) for i in range(10))

        user = User.objects.create(username=username, password='lkasjdhflksajdhflkLKJHLkdjhf')

        print(username)
        login(request, user)

        order = Order.objects.create(user=user)
       
        
    
    if request.method == "POST":
        chocolate_type = request.POST.get("chocolate_type")
        quantity_choice = request.POST.get("quantity_choice")
        box_image = request.POST.get("box_image")
        
        lst = []
        items_list = []
        items = BoxItem.objects.filter(type=chocolate_type)
        


        for i in items:
            quantity = request.POST.get("quantity-"+ i.slug)
            if float(quantity) > 0:
                order_box_item = OrderBoxItem.objects.create(
                    user=request.user,
                    item=i,
                )
                order_box_item.quantity = float(quantity)
                lst.append(float(order_box_item.quantity))
                items_list.append(order_box_item)
        last = sum(lst)

        print("Hiiiii")
        
    
        order_item = OrderItem.objects.create(
            user=request.user,
            item=item,
            ordered=False,
            chocolate_type=chocolate_type,
            quantity_choice=quantity_choice,
            box_choice=box_image,

            slug="".join(random.choice(string.ascii_lowercase) for i in range(10))
        )
        order_items = OrderItem.objects.filter(user=request.user, last=False, item=item)
        if order_item.quantity_choice == "Kilogram":
            if last == 60:

                for item in items_list:
                    if int(item.quantity) > 0:
                        item.save()
                        order_item.box_items.add(item)
                order_item.price = 60
                order_item.save()
                order.items.add(order_item)
            else:
                print(last)
                print("error")
            for i in order_items:
                one = []
                two = []

                print("---------------------------------------------------------------")
                print(i.chocolate_type)
                print(order_item.chocolate_type)
                print(i.quantity_choice)
                print(order_item.quantity_choice)
                print(i.box_choice)
                print(order_item.box_choice)
                print(i.box_items.all())
                print(order_item.box_items.all())

                for el in i.box_items.all():
                    if el.item.quantity > 0:
                        one.append(el.item.title)
                        one.append(el.quantity)

                for ele in order_item.box_items.all():
                    if ele.item.quantity > 0:
                        two.append(ele.item.title)
                        two.append(ele.quantity)

                print(one)
                print(two)

                if i.chocolate_type == order_item.chocolate_type and i.quantity_choice == order_item.quantity_choice and i.box_choice == order_item.box_choice and one == two:
                    print(4)
                    print(i.chocolate_type)
                    print(order_item.chocolate_type)
                    
                    
                    print("yo yo man")
                    i.quantity += 1
                    i.save()
                    order_item.delete()
                else:
                    order_item.last = False
                    order_item.save()
                
            return redirect('home')
        elif order_item.quantity_choice == "Half Kilo":
            if last == 30:

                for item in items_list:
                    item.save()
                    order_item.box_items.add(item)
                order_item.price = 30
                order_item.save()
                order.items.add(order_item)
            else:
                print(last)
                print("error")
            for i in order_items:
                one = []
                two = []

                for el in i.box_items.all():
                    if el.item.quantity > 0:
                        one.append(el.item.title)
                        one.append(el.quantity)

                for ele in order_item.box_items.all():
                    if ele.item.quantity > 0:
                        two.append(ele.item.title)
                        two.append(ele.quantity)

                print(one)
                print(two)

                if i.chocolate_type == order_item.chocolate_type and i.quantity_choice == order_item.quantity_choice and i.box_choice == order_item.box_choice and one == two:
                    
                    print(5)
                    print(i.chocolate_type)
                    print(order_item.chocolate_type)
                    
                    print("yo yo man")
                    i.quantity += 1
                    i.save()
                    order_item.delete()

                    


            order_item.last = False
            order_item.save()
                
            return redirect('home')
        elif order_item.quantity_choice == "Quarter Kilo":
            print("hiiii")
            if last == 15:

                for item in items_list:
                    item.save()
                    order_item.box_items.add(item)
                order_item.price = 15
                order_item.save()
                order.items.add(order_item)
            else:
                print(last)
                print("error")
            print(len(order_items))
            if len(order_items) > 0:
                print("ohhhhh")
                for i in order_items:
                    

                    one = []
                    two = []

                    
                    for el in i.box_items.all():
                        if el.item.quantity > 0:
                            one.append(el.item.title)
                            one.append(el.quantity)

                    for ele in order_item.box_items.all():
                        if ele.item.quantity > 0:
                            two.append(ele.item.title)
                            two.append(ele.quantity)


                    print(one)
                    print(two)
                    
                    if i.chocolate_type == order_item.chocolate_type and i.quantity_choice == order_item.quantity_choice and i.box_choice == order_item.box_choice and one == two:
                        
                        print(6)
                        print(i.chocolate_type)
                        print(order_item.chocolate_type)
                        
                        print("yo yo man")
                        i.quantity += 1
                        i.save()
                        order_item.delete()
                    else:
                        order_item.last = False
                        order_item.save()
                        print("nice")
                        
            else:
                order_item.last = False
                order_item.save()
                print("----------------------------------------------")
            return redirect('home')

        return HttpResponseRedirect(reverse("chocolate_weight"))
    
    else:
        chocolate_type_form = ChocolateTypeForm()
        items = BoxItem.objects.all().order_by("type")
        types = ChocolateType.objects.all()
        boxes = Box.objects.all()
        trays = Tray.objects.all()
        toppers = Topper.objects.all()
        flowers = Flower.objects.all()
        ribbons = Ribbon.objects.all()
        cards = Card.objects.all()


        context = {
            "chocolate_form": chocolate_type_form,
            'order': Order.objects.get(
                user=request.user,
                ordered=False
            ),
            "items": items,
            "types": types,
            "boxes": boxes,
            "trays": trays,
            "toppers": toppers,
            "flowers": flowers,
            "ribbons": ribbons,
            "cards": cards,




        }

        return render(request, 'chocolate_type.html', context)


# def chocolate_weight(request):
#     item = Item.objects.get(title="Customize your box")


#     order_item = OrderItem.objects.filter(
#         user=request.user,
#         item=item,
#         ordered=False,
#     ).order_by('-pk')[0]

#     if request.method == "POST":
#         order_item.quantity_choice = request.POST.get('quantity_choice')
#         order_item.save()
#         return HttpResponseRedirect(reverse("chocolate_box"))
#     else:
#         return render(request, 'chocolate_weight.html')



# def chocolate_box(request):
#     item = Item.objects.get(title="Customize your box")

#     order_items = OrderItem.objects.filter(user=request.user)
#     items = []
#     for i in order_items:
#         items.append(i.item.title)
#     if item.title in items:

#         order_item = OrderItem.objects.filter(
#             user=request.user,
#             item=item,
#             ordered=False,
#         ).order_by('-pk')[0]

#         if request.method == "POST":
#             print(request.POST.get('box_image'))
#             order_item.box_choice = request.POST.get('box_image')
#             order_item.save()
#             return HttpResponseRedirect(reverse("box-items-choice"))
#         else:
#             return render(request, 'chocolate_box.html')
#     else:
#         print(item)
#         print(items)
#         return redirect("choclate_type")



@login_required
def box_items_choice(request):
    item = Item.objects.get(title="Customize your box")
    order_items = OrderItem.objects.filter(user=request.user, last=False, item=item)
    order_item = OrderItem.objects.get(
        user=request.user,
        item=item,
        ordered=False,
        last = True
    )

    items = BoxItem.objects.filter(type=order_item.chocolate_type)

    order = Order.objects.get(
        user=request.user,
        ordered=False
    )

    context = {
        'order_item': order_item,
        "items": items,
        'order': order
    }
    lst = []
    items_list = []
    
    if request.method == "POST":
        for i in items:

            quantity = request.POST.get("quantity-"+ i.slug)
            if int(quantity) > 0:
                order_box_item = OrderBoxItem.objects.create(
                    user=request.user,
                    item=i,
                )
                order_box_item.quantity = quantity
                lst.append(int(order_box_item.quantity))
                items_list.append(order_box_item)
        last = sum(lst)
        print(items_list)
        if order_item.quantity_choice == "Kilogram":
            if last == 60:

                for item in items_list:
                    if int(item.quantity) > 0:
                        item.save()
                        order_item.box_items.add(item)
                order_item.price = 60
                order_item.save()
                order.items.add(order_item)
            else:
                print(last)
                print("error")
            for i in order_items:
                one = []
                two = []

                print("---------------------------------------------------------------")
                print(i.chocolate_type)
                print(order_item.chocolate_type)
                print(i.quantity_choice)
                print(order_item.quantity_choice)
                print(i.box_choice)
                print(order_item.box_choice)
                print(i.box_items.all())
                print(order_item.box_items.all())

                for el in i.box_items.all():
                    if el.item.quantity > 0:
                        one.append(el.item.title)
                        one.append(el.quantity)

                for ele in order_item.box_items.all():
                    if ele.item.quantity > 0:
                        two.append(ele.item.title)
                        two.append(ele.quantity)

                print(one)
                print(two)

                if i.chocolate_type == order_item.chocolate_type and i.quantity_choice == order_item.quantity_choice and i.box_choice == order_item.box_choice and one == two:
                   
                    print(7)
                    print(i.chocolate_type)
                    print(order_item.chocolate_type)
                   
                    print("yo yo man")
                    i.quantity += 1
                    i.save()
                    order_item.delete()
                else:
                    order_item.last = False
                    order_item.save()
                
            return redirect('home')
        elif order_item.quantity_choice == "Half Kilo":
            if last == 30:

                for item in items_list:
                    item.save()
                    order_item.box_items.add(item)
                order_item.price = 30
                order_item.save()
                order.items.add(order_item)
            else:
                print(last)
                print("error")
            for i in order_items:
                one = []
                two = []

                for el in i.box_items.all():
                    if el.item.quantity > 0:
                        one.append(el.item.title)
                        one.append(el.quantity)

                for ele in order_item.box_items.all():
                    if ele.item.quantity > 0:
                        two.append(ele.item.title)
                        two.append(ele.quantity)

                print(one)
                print(two)

                if i.chocolate_type == order_item.chocolate_type and i.quantity_choice == order_item.quantity_choice and i.box_choice == order_item.box_choice and one == two:
                    
                    print(8)
                    print(i.chocolate_type)
                    print(order_item.chocolate_type)
                    
                    print("yo yo man")
                    i.quantity += 1
                    i.save()
                    order_item.delete()

                    


            order_item.last = False
            order_item.save()
                
            return redirect('home')
        elif order_item.quantity_choice == "Quarter Kilo":
            print("hiiii")
            if last == 15:

                for item in items_list:
                    item.save()
                    order_item.box_items.add(item)
                order_item.price = 15
                order_item.save()
                order.items.add(order_item)
            else:
                print(last)
                print("error")
            print(len(order_items))
            if len(order_items) > 0:
                print("ohhhhh")
                for i in order_items:
                    

                    one = []
                    two = []

                    
                    for el in i.box_items.all():
                        if el.item.quantity > 0:
                            one.append(el.item.title)
                            one.append(el.quantity)

                    for ele in order_item.box_items.all():
                        if ele.item.quantity > 0:
                            two.append(ele.item.title)
                            two.append(ele.quantity)


                    print(one)
                    print(two)
                    
                    if i.chocolate_type == order_item.chocolate_type and i.quantity_choice == order_item.quantity_choice and i.box_choice == order_item.box_choice and one == two:
                        print(9)
                        print(i.chocolate_type)
                        print(order_item.chocolate_type)
                        
                        
                        print("yo yo man")
                        i.quantity += 1
                        i.save()
                        order_item.delete()
                    else:
                        order_item.last = False
                        order_item.save()
                        print("nice")
                        
            else:
                order_item.last = False
                order_item.save()
                print("----------------------------------------------")
            return redirect('home')
    else:
        return render(request, 'box_items_choice.html', context)


def add_box_quantity_check(request, slug):
    item = OrderItem.objects.get(slug=slug, user=request.user)
    item.quantity += 1
    item.save()
    lst = []
    for i in item.box_items.all():
        lst.append(i.item.title)
    return JsonResponse({"item": lst})

def register(request):
    registered = False
    context_email =''.join(random.choice(string.ascii_lowercase) for i in range(33))

    if request.user.is_authenticated:
        order = Order.objects.get(
            user=request.user,
            ordered=False
        )
    else:
        order = None

    if request.method == "POST":
        if request.user.is_authenticated:
            not_user = request.user
            not_order = Order.objects.get(user=not_user, ordered=False)
                        
            lst = []
            for i in not_order.items.all():
                lst.append(i.item.title)
            print(lst)

        user_form = UserForm(data=request.POST)
        profile_form = ProfileForm(data=request.POST)

        if user_form.is_valid() and profile_form.is_valid():
            try:
                user = user_form.save(commit=False)
                user.username = user_form.cleaned_data.get('email').lower()
                try:
                    validate_password(user.password, user)
                    print(user.password)
                except ValidationError as e:
                    print("--------------------------")
                    user_form.add_error("password", e)
                    return render(request, 'register.html', {'user_form': user_form, 'profile_form': profile_form})
                user.set_password(user.password)
                user.is_active = False
                print(user.username)
                # user.is_active = False
                user.save()
                
                order = Order.objects.create(user=user)
                if request.user.is_authenticated:
                    if User.objects.filter(username=request.user.username, password="lkasjdhflksajdhflkLKJHLkdjhf"):
                        for order_item in not_order.items.all():
                            print("hi")
                            item = OrderItem.objects.create(item=order_item.item, pieces_quantity=order_item.pieces_quantity, weight_quantity=order_item.weight_quantity, user=user, slug=order_item.item.slug, price=order_item.price, quantity_type=order_item.quantity_type)
                            if order_item.item.title == "Customize your box":
                                item.tray = order_item.tray
                                item.flower = order_item.flower
                                item.topper = order_item.topper
                                item.ribbon = order_item.ribbon
                                item.card = order_item.card
                                item.chocolate_type = order_item.chocolate_type
                                item.quantity_choice = order_item.quantity_choice
                                item.box_choice = order_item.box_choice
                                item.price = int(item.pieces_quantity) * int(order_item.price)
                                item.slug = order_item.slug
                                for i in order_item.box_items.all():
                                    order_box_item = OrderBoxItem.objects.create(user=user, item=i.item, quantity=i.quantity, ordered=i.ordered)
                                    item.box_items.add(order_box_item)
                            item.save()
                            print(item.item.title)
                            order.items.add(item)
                order.save()
                

                profile = profile_form.save(commit=False)

                profile.user = user
                profile.pin = random_digit(4)
                profile.save()
                if request.user.is_authenticated:
                    if User.objects.filter(username=request.user.username, password="lkasjdhflksajdhflkLKJHLkdjhf"):
                        request.user.delete()
                address = Address.objects.create(title="main address", area=profile.area, block=profile.block, street=profile.street, house=profile.house, floor=profile.floor, apartment=profile.apartment)
                order.address = address
                order.block = profile.block
                order.area = profile.area
                order.street = profile.street
                order.house = profile.house
                order.floor = profile.floor
                order.apartment = profile.apartment
                profile.addresses.add(address)
                profile.save()
                order.save()

            except IntegrityError:
                error = "user with that info already exists"
                user_form = UserForm()
                profile_form = ProfileForm()
                context = {
                    "error": error,
                    "user_form": user_form,
                    "profile_form": profile_form,
                    "order": order
                }
                print("hello")

                return render(request, "register.html", context)

            if user:

                query_email = urlencode({"email": user.email})
                base_url = reverse("check-pin")

                url = f"https://www.chocolatecokw.com{base_url}?{query_email}"

                subject = 'Verification Link'
                html_message = render_to_string('email_verification.html', {'url': url})
                plain_message = strip_tags(html_message)
                from_email = settings.EMAIL_HOST_USER
                to = user.email

                send_mail(subject, plain_message, from_email, [to], html_message=html_message, fail_silently=False)
                messages.success(request, 'A verification link was sent to your email')
                
                return redirect("/")
            else:
                return HttpResponse("No User")

            registered = True

        else:
            print("Error")
            print(user_form.errors, profile_form.errors)
    else:
        user_form = UserForm()
        profile_form = ProfileForm()

    

    context = {
        "user_form": user_form,
        "profile_form": profile_form,
        "registered": registered,
        'order': order,
        "email": context_email
    }

    return render(request, 'register.html', context)




def user_login(request):

    if request.user.is_authenticated:
        order=Order.objects.get(user=request.user, ordered=False)
    else:
        order=None
    if request.method == "POST":

        if request.user.is_authenticated:
            fake_user = User.objects.filter(username=request.user.username, password="lkasjdhflksajdhflkLKJHLkdjhf")
            if fake_user:
                fake_user.delete()

        username = request.POST.get("username").lower()
        password = request.POST.get("password")

        user = authenticate(username=username, password=password)


        if user:

            if user.is_active:

                login(request, user)

                return redirect("home")
            else:
                return redirect("login")
        else:
            error = True
            return render(request, 'login.html', {'error': error, "order": order})
            
    else:
       
        context = {
            "user": request.user,
            "order": order
        }
        return render(request, 'login.html', context)


def user_logout(request):


    fake_user = User.objects.filter(username=request.user.username, password="lkasjdhflksajdhflkLKJHLkdjhf")
    if fake_user:
        fake_user.delete()
    logout(request)
    return redirect('home')


def add_to_cart(request, id):
    current_url = resolve(request.path_info)
    print(current_url, "URL")
    if request.user.is_authenticated:
        pass
    else:
        username = ''.join(random.choice(string.ascii_lowercase) for i in range(10))

        user = User.objects.create(username=username, password='lkasjdhflksajdhflkLKJHLkdjhf')

        print(username)
        login(request, user)

        order = Order.objects.create(user=user)
        
    item = get_object_or_404(Item, id=id)
    order_item, created = OrderItem.objects.get_or_create(
        user=request.user,
        item=item,
        ordered=False
    )
    category = item.category
    order_qs = Order.objects.filter(user=request.user, ordered=False)
    choice = request.POST.get("choice")
    if order_qs.exists():
        order = order_qs[0]
        print(request.POST.get("quantity"))
        if order.items.filter(item__slug=item.slug).exists():
            if item.quantity_type == "Pieces":
                order_item.pieces_quantity += int(request.POST.get("quantity").split()[0])
            elif item.quantity_type == "Weight":
                order_item.weight_quantity += decimal.Decimal(request.POST.get("quantity").split()[0])
                order_item.pieces_quantity += int(request.POST.get("quantity").split()[0]) / 0.25
            order_item.save()
            Item.objects.all().update(quantity=1)
            return redirect("category-items", item.category.id)
        else:

            if item.quantity_type == "Pieces":
                order_item.pieces_quantity = int(request.POST.get('quantity').split()[0])
            else:
                order_item.weight_quantity = decimal.Decimal(request.POST.get('quantity').split()[0])
                order_item.pieces_quantity = int(float(request.POST.get('quantity').split()[0]) / 0.25)
            order_item.price = item.price
            order_item.save()   
            order.items.add(order_item)
            order.save()

            # Item.objects.all().update(pieces_quantity=1)

            return redirect("category-items", item.category.id)
    else:
        
        order = Order.objects.create(
            user=request.user
        )
        order_item.quantity = int(request.POST.get('quantity'))
        order_item.save()
        order.items.add(order_item)
        order.save()
        return redirect("category-items", item.category.id)




def add_to_cart_detail(request, id):

    if request.user.is_authenticated:
        pass
    else:
        username = ''.join(random.choice(string.ascii_lowercase) for i in range(10))

        user = User.objects.create(username=username, password='lkasjdhflksajdhflkLKJHLkdjhf')

        print(username)
        login(request, user)

        order = Order.objects.create(user=user)
        
    item = get_object_or_404(Item, id=id)
    # order_item, created = OrderItem.objects.get_or_create(
    #     user=request.user,
    #     item=item,
    #     ordered=False
    # )
    category = item.category
    order_qs = Order.objects.filter(user=request.user, ordered=False)
    choice = request.POST.get("choice")
    print(choice, "CHOICE")
    if order_qs.exists():
        order = order_qs[0]
        print(request.POST.get("quantity"))
        if order.items.filter(item__slug=item.slug, choice=choice).exists():
            order_item = OrderItem.objects.get(user=request.user, item=item, ordered=False, choice=choice)
            print("hoooooooooooooooooooooooo")
            if item.quantity_type == "Pieces":
                order_item.pieces_quantity += int(request.POST.get("quantity").split()[0])
            elif item.quantity_type == "Weight":
                order_item.weight_quantity += decimal.Decimal(request.POST.get("quantity").split()[0])
                order_item.pieces_quantity += int(float(request.POST.get("quantity").split()[0])) / 0.25
            order_item.save()
            return JsonResponse({"item": "item"})
        else:
            
            order_item = OrderItem.objects.create(user=request.user, item=item, choice=choice, quantity_type=item.quantity_type)
            if choice:
                order_item.has_choice = True
            print("hiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii")
            if item.quantity_type == "Pieces":
                order_item.pieces_quantity = int(request.POST.get('quantity').split()[0])
            else:
                order_item.weight_quantity = decimal.Decimal(request.POST.get('quantity').split()[0])
                order_item.pieces_quantity = int(float(request.POST.get('quantity').split()[0]) / 0.25)
            order_item.price = item.price
            order_item.save()   
            order.items.add(order_item)
            order.save()

            # Item.objects.all().update(pieces_quantity=1)

            return JsonResponse({"item": "item"})
    else:
        
        order = Order.objects.create(
            user=request.user
        )
        order_item = order_item = OrderItem.objects.create(user=request.user, item=item, choice=choice, quantity_type=item.quantity_type)
        order_item.quantity = int(request.POST.get('quantity'))
        order_item.save()
        order.items.add(order_item)
        order.save()
        return JsonResponse({"item": "item"})



def remove_from_cart(request, id):
    item = get_object_or_404(Item, id=id)

    order_qs = Order.objects.filter(
        user=request.user,
        ordered=False
    )
    if order_qs.exists():
        order = order_qs[0]

        if order.items.filter(item__id=id).exists():
            order_item = OrderItem.objects.filter(
               user=request.user,
               ordered=False,
               item=item
            )[0]
            order.items.remove(order_item)
            order_item.delete()
            return redirect('cart')
        else:
            print(order.items.filter(item__id=id))
            print('not in cart')
            return redirect('cart')
    else:
        print('No order')
        return redirect('cart')


def remove_box_from_cart(request, id):
    try:
        order = Order.objects.get(user=request.user, ordered=False)
        order_item = OrderItem.objects.get(id=id, user=request.user)
        order.items.remove(order_item)
        order_item.delete()
        return redirect("cart")
    except ObjectDoesNotExist:

        return redirect("cart")
    

def cart(request):

    order = Order.objects.get(user=request.user, ordered=False)

    order_items = order.items.all()
    
    print(request.user)

    context = {
        'items': order_items,
        'order': order
    }
    return render(request, 'cart.html', context)




def clear_cart(request):

    order = Order.objects.get(user=request.user, ordered=False)

    for item in order.items.all():
        order.items.remove(item)
        item.delete()
    return redirect('cart')

def add_quantity(request, slug):
    item = Item.objects.get(slug=slug)


    order_item = OrderItem.objects.get(
        user=request.user,
        item=item,
        ordered=False
    )
    
    order_item.quantity += 1
    order_item.save()
    return redirect('cart')



def remove_quantity(request, id):
    order = Order.objects.get(user=request.user, ordered=False)
    order_item = OrderItem.objects.get(
        id=id,
        user=request.user,
        ordered=False
    )   
    if order_item.quantity_type == "Pieces":
        if order_item.pieces_quantity > order_item.item.pieces_minimum:
            order_item.pieces_quantity -= 1
            order_item.save()
        else:
            print("didn't remove")
    else:
        if order_item.weight_quantity > order_item.item.weight_minimum:
            order_item.weight_quantity -= decimal.Decimal(0.25) 
            order_item.pieces_quantity -= 1
            order_item.save()
        else:
            print("didn't remove")

    return redirect('cart')






def checkout(request):
    
    if request.method == "POST":
        
        print("checkout post h h ")
        user = request.user
        order = Order.objects.get(ordered=False, user=user)
        # for item in order.items.all():
            
        #     item.quantity = request.POST.get(f"{ item.item.title }-quantity")
        #     item.save()

        
        print(order.id, "ID")

        note = request.POST.get("note")

        order.order_type = request.POST.get("type")
        order.save()


        if request.POST.get("day") == "today":
            order.order_day = date_format(datetime.date.today(), "l")
            day = date_format(datetime.date.today(), "d/m/Y")
        elif request.POST.get("day") == "tomorrow":
            order.order_day = date_format(datetime.date.today() + datetime.timedelta(days=1), "l")
            day = date_format(datetime.date.today() + datetime.timedelta(days=1), "d/m/Y")
        else:
            day = request.POST.get("day")


        time = request.POST.get("time")
    
        order.delivery_date = f"{ day } | { time }"

        order.order_time = request.POST.get("time")
        if note:
            order.note = note

        order.save()

      

        
        
        
       
        # url = baseURL + "/v2/SendPayment"
        # payload = "{\"CustomerName\": \""+ order.user.username +"\",\"NotificationOption\": \"LNK\"," \
        #             "\"InvoiceValue\": "+ str(order.get_order_total()) +"," \
        #             "\"CallBackUrl\": \"https://www.chocolatecokw.com/checkout-done\",} "
        # headers = {'Content-Type': "application/json", 'Authorization': "Bearer " + token}
        # response = requests.request("POST", url, data=payload, headers=headers)
        # print(response.text)
        # res = json.loads(response.text)
        # last_url = res["Data"]["InvoiceURL"]
        return redirect("order-summary")



def visitor(request):

    user = request.user
    order = Order.objects.get(user=user, ordered=False)

    form = ProfileForm()

    if request.method == "POST":

       


        
       
        area = request.POST.get("area")
        block = request.POST.get("block")
        street = request.POST.get("street")
        house = request.POST.get("house")
        floor = request.POST.get("floor")
        apartment =  request.POST.get("apartment")

        request.session["area"] = area
        request.session["block"] = block
        request.session["street"] = street
        request.session["house"] = house
        request.session["floor"] = floor
        request.session["apartment"] = apartment




        return redirect("checkout")




        

            


            
            
    context = {
        "order": order,
        "profile_form": form
    }




    return render(request, "gifting_options.html", context)






def socket_checkout(request):

    user = request.user
    order = Order.objects.get(user=user, ordered=False)
    
    
    transaction = Transaction.objects.get(order=order, done=False)
    transaction.done = True
    transaction.date = datetime.datetime.now()
    transaction.save()
    for item in order.items.all():
        item.ordered = True
        item.ordered_date = datetime.datetime.now()
        item.save()

    order.ordered = True
    if Discount.objects.filter(start_date__lte=datetime.datetime.now().date(), end_date__gte=datetime.datetime.now().date()).exists():
        order.discount = Discount.objects.get(start_date__lte=datetime.datetime.now().date(), end_date__gte=datetime.datetime.now().date()).value
    else:
        order.discount = 0
    order.ordered_date = datetime.datetime.now()
    order.save()

    items = order.items.all()

    subject = 'Thank you for your order'
    html_message = render_to_string('order_received_email.html', {'items': items})
    plain_message = strip_tags(html_message)
    from_email = settings.EMAIL_HOST_USER
    to = user.email

    send_mail(subject, plain_message, from_email, [to], html_message=html_message, fail_silently=False)

    url = reverse("home")
    return redirect(f"{ url }?checkout=True")


def checkout_done(request):

    track_id = request.GET.get("paymentId")



    user = request.user
    order = Order.objects.get(user=user, ordered=False)
    if Profile.objects.filter(user=user).exists():
        
        order.block = user.profile.block
        order.area = user.profile.area
        order.street = user.profile.street
        order.house = user.profile.house
        order.user_name = user.profile.name
        order.phone_number = user.profile.phone_number
    else:
        print("jiiii")
        print(order)
        order.block = request.session["block"]
        order.area = request.session["area"]
        order.street = request.session["street"]
        order.house = request.session["house"]
        order.user_name = request.session["name"]
        order.phone_number = request.session["number"]


    


    order.ordered_date = datetime.datetime.now()
    order.save()


    context = {
        "ordered_date": order.ordered_date,
        "items": order.items.all(),
        "delivery_charge": DeliveryCharge.objects.first().price,
        "order": order
    }

    
   

    if Profile.objects.filter(user=user).exists():
        if Transaction.objects.filter(order=order, user=user, done=False).exists():
            pass
        else:
            Transaction.objects.create(order=order, amount=order.get_order_total(), user=user, track_id=track_id)
    else:
        if Transaction.objects.filter(order=order, user_name=request.session["name"], done=False):
            pass
        else:
            Transaction.objects.create(order=order, amount=order.get_order_total(), user_name=request.session["name"], track_id=track_id)


    return render(request, "checkout_loading.html", context)

class CheckoutView(LoginRequiredMixin,View):
    

    def get(self, *args, **kwargs):


        if self.request.user.is_authenticated:
            try:
                order = Order.objects.get(user=self.request.user, ordered=False)
                order.order_type = None
                order.coupon = None
                order.save()
            except Order.DoesNotExist:
                order = Order.objects.create(user=self.request.user, ordered=False)
        user = self.request.user
        
        
        lst = OrderItem.objects.filter(user=user, ordered=False).values_list("item__delivery_date__number", flat=True)
        lst = list(lst)
        customized_box_items = OrderItem.objects.filter(item__customized_box=True, user=user, ordered=False)
        if customized_box_items.exists():
            lst.append(1)
            print("one appended")
        print(lst, "LIST")
        max_date = max([int(x) for x in lst])
        print(max_date, "MAX DATE")
        delivery_date = DeliveryDate.objects.get(number=max_date)

        dates_off = DateOff.objects.all()

        print(datetime.datetime.now().time(), "CURRENT TIME")




        if delivery_date.number > 0:
            today_off = True
        else:
            today_off = False

        if delivery_date.number > 1:
            today_off = True
            tomorrow_off = True
        else:
            tomorrow_off = False

        if delivery_date.number > 2:
            day_number = delivery_date.number
        else:
            day_number = 2
        

        for date in dates_off:
            if date.from_date <= datetime.date.today() <= date.to_date:
                today_off = True
                break

        for date in dates_off:
            if date.from_date <= datetime.date.today() + datetime.timedelta(1) <= date.to_date:
                tomorrow_off = True
                break

        
        
        # if self.request.user.is_authenticated:
        #     fake_user = User.objects.filter(username=self.request.user.username, password="lkasjdhflksajdhflkLKJHLkdjhf").exists()
        #     print(fake_user, "HII")
        #     if fake_user:
        #         return redirect("register")
        #     else:
        #         print(fake_user, "HII")
        # else:
        #     print("hody")
        try:
            print("he")
            days = WorkingDay.objects.all()
            day = WorkingDay.objects.get(day=date_format(datetime.date.today(), "l"))
            print(day, "DAY")
            today = datetime.datetime.now().strftime("%A")
            
                
                

            if day.to_time1 < datetime.datetime.now().time():
                today_disabled_time1 = True
            else:
                today_disabled_time1 = False
                
            
            if day.to_time2 < datetime.datetime.now().time():
                today_disabled_time2 = True
            else:
                today_disabled_time2 = False
                


            if day.to_time3 < datetime.datetime.now().time():
                today_disabled_time3 = True
            else:
                today_disabled_time3 = False
                
            
            

            lst = []
            order = Order.objects.get(user=self.request.user, ordered=False)
            order.save()
            for item in order.items.all():
                if item.flower:
                    disabled = "disabled"
                else:
                    print("something wrong", "DISABLED")
                    disabled = "today-container"
            print("helo")
            for item in order.items.all():
                obj = {
                    "ItemName": item.item.title,
                    "Quantity": item.pieces_quantity,
                    "UnitPrice": item.item.price,
                }
                lst.append(obj)
            
            form = CheckoutForm()
            user = self.request.user
            min_price = MinimumOrder.objects.first().price
            pickup =  PickupPlace.objects.first()
           
            
            context = {
                'form': form,
                'order': order,
                "day": day,
                "pickup": pickup,
                "days": days,
                "min_price": min_price,
                "disabled": disabled,
                "today_off": today_off,
                "tomorrow_off": tomorrow_off,
                "day_number": day_number,
                "today_disabled_time1": today_disabled_time1,
                "today_disabled_time2": today_disabled_time2,
                "today_disabled_time3": today_disabled_time3,
                "days_off": json.dumps((DatesSerializer(DateOff.objects.all(), many=True).data))
            }

            
            

            if order.get_order_total() > 0:
                return render(self.request, "checkout.html", context)
            else:
                print('hhh')
                return redirect('home')
        except ObjectDoesNotExist as e:
            print(e)
            print("hi")
            print("------------------------------------------")
            messages.info(self.request, "You do not have an active order")
            return redirect("home")

    




class Account(UserPassesTestMixin ,UpdateView):

    def test_func(self):
        return not self.request.user.is_staff

    template_name = 'account.html'
    model = Profile
    form_class = ProfileForm
    success_url = '/'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['user'] = self.request.user
        context["order"] = Order.objects.get(user=self.request.user, ordered=False)
        return context

    



def order_details(request, id):
    order = Order.objects.get(id=id)


    return render(request, "operation/order_details.html", {"order": order})


def operation(request):
    try:
        user = request.user
        if user.is_staff:
            orders = Order.objects.all()
        
            return render(request, 'operation/index.html', {"orders": orders})
        else:
            return redirect("op-login")
    except:
        return redirect("op-login")

def operation_login(request):
    
    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")
        try:
            user = User.objects.get(username=username)
            print(user.username)
            print(username)
            print(user.password)
            print(password)

            auth = authenticate(username=username, password=password)

            if auth.is_staff:
                login(request, auth)
                return redirect("op-index")
            else:
                print("staff")
                staff = False
                return render(request, "operation/login.html", {"staff": staff})
        except User.DoesNotExist:
            print("user")
            error = True
            return render(request, "operation/login.html", {"error": error})

    else:
        return render(request, 'operation/login.html')




def email_verification(request):
    

    if request.user.is_authenticated:
        fake_user = User.objects.filter(username=request.user.username, password="lkasjdhflksajdhflkLKJHLkdjhf").exists()
        print(fake_user)        
        if fake_user:
            User.objects.filter(username=request.user.username, password="lkasjdhflksajdhflkLKJHLkdjhf").delete()
            print("ahem")
        else:
            print(fake_user, "no user")
    else:
        print("wow")
    email = request.GET["email"]

    user = User.objects.get(email=email)

    user.is_active = True
    user.save()
    login(request, user)
    return redirect("/")



def categories_home(request, id):
    category = Category.objects.get(id=id)
    subs = category.subs.all()
    packages = Package.objects.all()

    if request.user.is_authenticated:
        try:
            order = Order.objects.get(user=request.user, ordered=False)
            order.coupon = None
            order.order_type = None
            order.save()
        except Order.DoesNotExist:
            order = Order.objects.create(user=request.user, ordered=False)
    else:
        order = None
    items = Item.objects.filter(is_deleted=False)
    context = {
        "category": category,
        "subs": subs,
        "items": items,
        "order": order,
        "packages": packages,
    }

    

    return render(request, "categories_home.html", context)







def add_favorite(request, id):
    profile = request.user.profile
    item = Item.objects.get(id=id)

    profile.favorites.add(item)
    profile.save()
    print(profile.favorites.all())

    return JsonResponse({"item": "item"})


def remove_favorite(request, id):
    profile = request.user.profile
    item = Item.objects.get(id=id)

    profile.favorites.remove(item)
    profile.save()
    print(profile.favorites.all())

    return JsonResponse({"item": "item"})



def add_favorite_detail(request, id):
    profile = request.user.profile
    item = Item.objects.get(id=id)

    profile.favorites.add(item)
    profile.save()
    print(profile.favorites.all())

    return redirect("detail", item.slug)


def remove_favorite_detail(request, id):
    profile = request.user.profile
    item = Item.objects.get(id=id)

    profile.favorites.remove(item)
    profile.save()
    print(profile.favorites.all())

    return redirect("detail", item.slug)




def search(request):

    if request.user.is_authenticated:
        order = Order.objects.get(user=request.user, ordered=False)
    else:
        order = None

    if request.method == "GET":
        if request.GET.get("query"):
            query = request.GET.get("query")

            items = list(Item.objects.filter(title__icontains=query))
            for item in Item.objects.filter(description__icontains=query):
                if item in items:
                    pass
                else:
                    items.append(item)

            context = {
                "items": items,
                "results": len(items)
            }

            return render(request, "search.html", context)
        else:
            return redirect("home")

    if request.method == "POST":
        query = request.POST.get("search")


        items = list(Item.objects.filter(title__icontains=query).order_by("sub_category"))
        items.extend(list(Item.objects.filter(description__icontains=query).order_by("sub_category")))


        context = {
            "items": items,
            "results": len(items),
            "query": query,
            "order": order
        }




        return render(request, "search.html", context)        



def favorites(request):
    if request.user.is_authenticated:
        order = Order.objects.get(user=request.user, ordered=False)
        profile = request.user.profile
        favorite_items = profile.favorites.all()

    else:
        return redirect("home")

    context = {
        "items": favorite_items,
        "order": order,

    }

    return render(request, "favorites.html", context)




def forgot_password(request):

    if request.method == "POST":
        email = request.POST.get("email")
        if User.objects.filter(username=email).exists():
            
            query_email = urlencode({"email": email})
            base_url = reverse("reset-password")

            url = f"https://www.chocolatecokw.com{base_url}?{query_email}"
            subject = 'Forgot Password'
            html_message = render_to_string('forgot_password_email.html', {"url": url})
            plain_message = strip_tags(html_message)
            from_email = settings.EMAIL_HOST_USER
            to = email


            send_mail(subject, plain_message, from_email, [to], html_message=html_message, fail_silently=False)
            messages.success(request, 'Reset password link was sent to your email')

            return redirect("home")
        else:
            error = "User with that email doesn't exist"
            return render(request, "forgot_password.html", {"error": error})

    return render(request, "forgot_password.html")



def reset_password(request):
    email = request.GET.get("email")
    user = User.objects.get(username=email)
    form = ResetPasswordForm()
    if request.method == "POST":
        form = ResetPasswordForm(data=request.POST)
        if form.is_valid():
            password = form.cleaned_data["password"]
            password2 = form.cleaned_data["password2"]


            if password == password2:
                try:
                    validate_password(password, user)
                    print(password)
                except ValidationError as e:
                    

                    form.add_error("password", e)
                    return render(request, "reset_password.html", {"form": form})
                
                user.set_password(password)
                user.save()
                login(request, user)



                return redirect("home")
            else:
                error = "Passwords Don't match."
                return render(request, "reset_password.html", {"error":error, "form": form})
        else:
            print(form.errors)

    return render(request, "reset_password.html", {"form": form})



def change_password(request):
    user = request.user
    if Order.objects.filter(user=user, ordered=False).exists():
        order = Order.objects.get(user=user, ordered=False)
    else:
        order = None
    form = ChangePasswordForm()
    if request.method == "POST":
        form = ChangePasswordForm(data=request.POST)
        if form.is_valid():
            user_password = form.cleaned_data["user_password"]
            password = form.cleaned_data["password"]
            password2 = form.cleaned_data["password2"]

            if user.check_password(user_password):
                if password == password2:
                    try:
                        validate_password(password, user)
                        print(password)
                    except ValidationError as e:
                        

                        form.add_error("password", e)
                        return render(request, "reset_password.html", {"form": form, "order": order})
                    
                    user.set_password(password)
                    user.save()
                    login(request, user)
                    


                    return redirect("account", user.profile.id)
                else:
                    error = "Passwords Don't match."
                    return render(request, "reset_password.html", {"error":error, "order": order, "form": form})
            else:
                error = "Your Password is Wrong"
                return render(request, "reset_password.html", {"error":error, "order": order, "form": form})
        else:
            print(form.errors)

    return render(request, "reset_password.html", {"form": form, "order": order})





def orders_history(request):

    user = request.user

    print(user)

    orders = Order.objects.filter(user=user, ordered=True).order_by("-ordered_date")
    order = Order.objects.get(user=user, ordered=False)

    context = {
        "orders": orders,
        "order": order
    }


    return render(request, "orders_history.html", context)



def terms_and_conditions(request):

    if request.user.is_authenticated:
        order = Order.objects.get(user=request.user, ordered=False)
    else:
        order = None

    return render(request, "terms_and_conditions.html", {"order": order})


def privacy_policy(request):

    if request.user.is_authenticated:
        order = Order.objects.get(user=request.user, ordered=False)
    else:
        order = None

    return render(request, "privacy_policy.html", {"order": order})



def order_summary(request):

    user = request.user

    price = DeliveryCharge.objects.first().price

    order = Order.objects.get(user=user, ordered=False)
    print(datetime.datetime.now().date(), "DAte")
    if Discount.objects.filter(start_date__lte=datetime.datetime.now().date(), end_date__gte=datetime.datetime.now().date()).exists():
        discount = Discount.objects.get(start_date__lte=datetime.datetime.now().date(), end_date__gte=datetime.datetime.now().date())

    else:
        discount = None


    url = baseURL + "/v2/SendPayment"
    if Profile.objects.filter(user=user).exists():
        name = order.user.profile.name
        number = order.user.profile.phone_number
        email = order.user.username
    else:
        name = request.session.get("name") or "admin"
        number = request.session.get("number") or "00000000"
        email = "hi"
    

    print(name)
    print(number)


    payload = "{\"CustomerName\": \""+ name +"\",\"CustomerMobile\": \""+ number +"\",\"NotificationOption\": \"LNK\"," \
                "\"InvoiceValue\": "+ str(order.get_order_total()) +"," \
                "\"CallBackUrl\": \"https://www.chocolatecokw.com/checkout-done\",\"ErrorUrl\": \"https://www.chocolatecokw.com/checkout-failed\"} "
    headers = {'Content-Type': "application/json", 'Authorization': "Bearer " + token}
    response = requests.request("POST", url, data=payload.encode('utf-8'), headers=headers)
    print(response.status_code)
    res = json.loads(response.text)
    last_url = res["Data"]["InvoiceURL"]
    


    context = {
        "order": order,
        "delivery_charge": price,
        "url": last_url,
        "discount": discount
    }

    return render(request, "order_summary.html", context)



def send_payment(request):


    user = request.user


    order = Order.objects.get(user=user, ordered=False)

    if Discount.objects.filter(start_date__lte=datetime.datetime.now().date(), end_date__gte=datetime.datetime.now().date()).exists():
        discount = Discount.objects.get(start_date__lte=datetime.datetime.now().date(), end_date__gte=datetime.datetime.now().date())
    else:
        discount = None


    url = baseURL + "/v2/SendPayment"
    if Profile.objects.filter(user=user).exists():
        name = order.user.profile.name
        number = order.user.profile.phone_number
    else:
        name = request.session["name"]
        number = request.session["number"]



    if discount:
        total = order.get_discount_total()
    else:
        total = order.get_order_total()

    payload = "{\"CustomerName\": \""+ name +"\",\"CustomerMobile\": \""+ number +"\",\"NotificationOption\": \"LNK\"," \
                "\"InvoiceValue\": "+ str(total) +"," \
                "\"CallBackUrl\": \"https://www.chocolatecokw.com/checkout-done\",\"ErrorUrl\": \"https://www.chocolatecokw.com/checkout-failed\"} "
    headers = {'Content-Type': "application/json", 'Authorization': "Bearer " + token}
    response = requests.request("POST", url, data=payload.encode('utf-8'), headers=headers)
    print(response.text)
    res = json.loads(response.text)
    last_url = res["Data"]["InvoiceURL"]

    return redirect(last_url)



def ar_send_payment(request):


    user = request.user


    order = Order.objects.get(user=user, ordered=False)

    if Discount.objects.filter(start_date__lte=datetime.datetime.now().date(), end_date__gte=datetime.datetime.now().date()).exists():
        discount = Discount.objects.get(start_date__lte=datetime.datetime.now().date(), end_date__gte=datetime.datetime.now().date())
    else:
        discount = None


    url = baseURL + "/v2/SendPayment"
    if Profile.objects.filter(user=user).exists():
        name = order.user.profile.name
        number = order.user.profile.phone_number
    else:
        name = request.session["name"]
        number = request.session["number"]


    if discount:
        total = order.get_discount_total()
    else:
        total = order.get_order_total()

    payload = "{\"CustomerName\": \""+ name +"\",\"CustomerMobile\": \""+ number +"\",\"NotificationOption\": \"LNK\"," \
                "\"InvoiceValue\": "+ str(total) +"," \
                "\"CallBackUrl\": \"https://www.chocolatecokw.com/ar/checkout-done\",\"ErrorUrl\": \"https://www.chocolatecokw.com/ar/checkout-failed\"} "
    headers = {'Content-Type': "application/json", 'Authorization': "Bearer " + token}
    response = requests.request("POST", url, data=payload.encode('utf-8'), headers=headers)
    print(response.text)
    res = json.loads(response.text)
    last_url = res["Data"]["InvoiceURL"]

    return redirect(last_url)



def subscribe(request):

    email = request.POST.get("subs-email")


    user = SubscribedUser.objects.create(email=email)


    return JsonResponse({"status": "sucess"})



def checkout_failed(request):

    url = reverse("home")

    return redirect(f"{ url }?checkout_failed=True")



def create_visitor(request):


    if request.method == "POST":
        request.session["name"] = request.POST.get("name")
        request.session["number"] = request.POST.get("number")




        print(request.session["name"])
        print(request.session["number"])


        return redirect("visitor")

    
#################### ARABIC VERSION ####################


def ar_index(request):


    print(request.GET.get("customized_ordered"))
    if request.user.is_authenticated:
        print(request.user.username, "USERNAME")
            

    categories = Category.objects.all().order_by("number")
    if request.user.is_authenticated:
        try:
            order = Order.objects.get(user=request.user, ordered=False)
            order.order_type = None
            order.coupon = None
            order.note = None
            order.save()
        except Order.DoesNotExist:
            order = Order.objects.create(user=request.user, ordered=False)
            

        item = Item.objects.get(title="Customize your box")
        order_items = OrderItem.objects.filter(user=request.user, item=item)
        for i in order_items:
            if not i.price:
                i.delete()
                print("deleted Ohhh")
    else:
        
        username = ''.join(random.choice(string.ascii_lowercase) for i in range(10))

        user = User.objects.create(username=username, password='lkasjdhflksajdhflkLKJHLkdjhf')

        print(username)
        login(request, user)

        order = Order.objects.create(user=user)


    pop_up = PopUp.objects.first()    

    context = {
        'order': order,
        'categories': categories,
        'customized_ordered': request.GET.get("customized_ordered"),
        'checkout': request.GET.get("checkout"),
        'checkout_failed': request.GET.get("checkout_failed"),

        "banner_image": BannerImage.objects.first(),
        "pop_up": pop_up
    }


    if not pop_up.disabled:
        if request.user.is_authenticated and request.user.password != "lkasjdhflksajdhflkLKJHLkdjhf":
            profile = request.user.profile
            if not profile.has_seen_popup:
                context['show_popup'] = True
                profile.has_seen_popup = True
                request.session['has_seen_popup'] = True
                profile.save()
        else:
            if not request.session.get('has_seen_popup', True):
                request.session['has_seen_popup'] = True
                context['show_popup'] = True

    return render(request, 'ar/index.html', context)




def ar_categories_home(request, id):
    category = Category.objects.get(id=id)
    subs = category.subs.all()
    if request.user.is_authenticated:
        try:
            order = Order.objects.get(user=request.user, ordered=False)
            order.coupon = None
            order.order_type = None
            order.save()
        except Order.DoesNotExist:
            order = Order.objects.create(user=request.user, ordered=False)
    else:
        order = None
    items = Item.objects.filter(is_deleted=False)
    context = {
        "category": category,
        "subs": subs,
        "items": items,
        "order": order
    }

    

    return render(request, "ar/categories_home.html", context)






def ar_detail_view(request, id):

    if request.user.is_authenticated:
        pass
    else:
        username = ''.join(random.choice(string.ascii_lowercase) for i in range(10))

        user = User.objects.create(username=username, password='lkasjdhflksajdhflkLKJHLkdjhf')

        print(username)
        login(request, user)

        order = Order.objects.create(user=user)

    item = get_object_or_404(Item, id=id)

    packages = Package.objects.all()


    items = Item.objects.exclude(id=id).filter(category=item.category)
    if request.user.is_authenticated:
        order = Order.objects.get(user=request.user, ordered=False)
    else:
        order = None
    context = {
        'item': item,
        'order': order,
        'packages': packages,

    }

    return render(request, 'ar/detail.html', context)




def ar_about_us(request):

    about_us_content = AboutUsContent.objects.all()[0]
    topics = AboutUsTopic.objects.all().order_by("number")

    if request.user.is_authenticated:
        try:
            order = Order.objects.get(user=request.user, ordered=False)
            order.order_type = None
            order.coupon = None
            order.save()
        except Order.DoesNotExist:
            order = Order.objects.create(user=request.user, ordered=False)
    else:
        order = None

    if request.method == "POST":
        name = request.POST.get("name")
        email = request.POST.get("email")
        message = request.POST.get("message")

        ContactUs.objects.create(name=name, email=email, message=message)

        return redirect("about-us")

    
    context = {
        "user": request.user,
        "order": order,
        "content": about_us_content,
        "topics": topics,
    }

    return render(request, 'ar/about.html', context)




def ar_chocolate_type(request):
    item = Item.objects.get(title="Customize your box")
    if request.user.is_authenticated:
        try:
            order = Order.objects.get(user=request.user, ordered=False)
            order.coupon = None
            order.order_type = None
            order.save()
        except Order.DoesNotExist:
            order = Order.objects.create(user=request.user, ordered=False)
    else:
        username = ''.join(random.choice(string.ascii_lowercase) for i in range(10))

        user = User.objects.create(username=username, password='lkasjdhflksajdhflkLKJHLkdjhf')

        print(username)
        login(request, user)

        order = Order.objects.create(user=user)
       
        
    
    if request.method == "POST":
        chocolate_type = request.POST.get("chocolate_type")
        quantity_choice = request.POST.get("quantity_choice")
        box_image = request.POST.get("box_image")
        
        lst = []
        items_list = []
        items = BoxItem.objects.filter(type=chocolate_type)
        


        for i in items:
            quantity = request.POST.get("quantity-"+ i.slug)
            if int(quantity) > 0:
                order_box_item = OrderBoxItem.objects.create(
                    user=request.user,
                    item=i,
                )
                order_box_item.quantity = quantity
                lst.append(int(order_box_item.quantity))
                items_list.append(order_box_item)
        last = sum(lst)

        print("Hiiiii")
        
    
        order_item = OrderItem.objects.create(
            user=request.user,
            item=item,
            ordered=False,
            chocolate_type=chocolate_type,
            quantity_choice=quantity_choice,
            box_choice=box_image,

            slug="".join(random.choice(string.ascii_lowercase) for i in range(10))
        )
        order_items = OrderItem.objects.filter(user=request.user, last=False, item=item)
        if order_item.quantity_choice == "Kilogram":
            if last == 60:

                for item in items_list:
                    if int(item.quantity) > 0:
                        item.save()
                        order_item.box_items.add(item)
                order_item.price = 60
                order_item.save()
                order.items.add(order_item)
            else:
                print(last)
                print("error")
            for i in order_items:
                one = []
                two = []

                print("---------------------------------------------------------------")
                print(i.chocolate_type)
                print(order_item.chocolate_type)
                print(i.quantity_choice)
                print(order_item.quantity_choice)
                print(i.box_choice)
                print(order_item.box_choice)
                print(i.box_items.all())
                print(order_item.box_items.all())

                for el in i.box_items.all():
                    if el.item.quantity > 0:
                        one.append(el.item.title)
                        one.append(el.quantity)

                for ele in order_item.box_items.all():
                    if ele.item.quantity > 0:
                        two.append(ele.item.title)
                        two.append(ele.quantity)

                print(one)
                print(two)

                if i.chocolate_type == order_item.chocolate_type and i.quantity_choice == order_item.quantity_choice and i.box_choice == order_item.box_choice and one == two:
                    print(4)
                    print(i.chocolate_type)
                    print(order_item.chocolate_type)
                    
                    
                    print("yo yo man")
                    i.quantity += 1
                    i.save()
                    order_item.delete()
                else:
                    order_item.last = False
                    order_item.save()
                
            return redirect('home')
        elif order_item.quantity_choice == "Half Kilo":
            if last == 30:

                for item in items_list:
                    item.save()
                    order_item.box_items.add(item)
                order_item.price = 30
                order_item.save()
                order.items.add(order_item)
            else:
                print(last)
                print("error")
            for i in order_items:
                one = []
                two = []

                for el in i.box_items.all():
                    if el.item.quantity > 0:
                        one.append(el.item.title)
                        one.append(el.quantity)

                for ele in order_item.box_items.all():
                    if ele.item.quantity > 0:
                        two.append(ele.item.title)
                        two.append(ele.quantity)

                print(one)
                print(two)

                if i.chocolate_type == order_item.chocolate_type and i.quantity_choice == order_item.quantity_choice and i.box_choice == order_item.box_choice and one == two:
                    
                    print(5)
                    print(i.chocolate_type)
                    print(order_item.chocolate_type)
                    
                    print("yo yo man")
                    i.quantity += 1
                    i.save()
                    order_item.delete()

                    


            order_item.last = False
            order_item.save()
                
            return redirect('home')
        elif order_item.quantity_choice == "Quarter Kilo":
            print("hiiii")
            if last == 15:

                for item in items_list:
                    item.save()
                    order_item.box_items.add(item)
                order_item.price = 15
                order_item.save()
                order.items.add(order_item)
            else:
                print(last)
                print("error")
            print(len(order_items))
            if len(order_items) > 0:
                print("ohhhhh")
                for i in order_items:
                    

                    one = []
                    two = []

                    
                    for el in i.box_items.all():
                        if el.item.quantity > 0:
                            one.append(el.item.title)
                            one.append(el.quantity)

                    for ele in order_item.box_items.all():
                        if ele.item.quantity > 0:
                            two.append(ele.item.title)
                            two.append(ele.quantity)


                    print(one)
                    print(two)
                    
                    if i.chocolate_type == order_item.chocolate_type and i.quantity_choice == order_item.quantity_choice and i.box_choice == order_item.box_choice and one == two:
                        
                        print(6)
                        print(i.chocolate_type)
                        print(order_item.chocolate_type)
                        
                        print("yo yo man")
                        i.quantity += 1
                        i.save()
                        order_item.delete()
                    else:
                        order_item.last = False
                        order_item.save()
                        print("nice")
                        
            else:
                order_item.last = False
                order_item.save()
                print("----------------------------------------------")
            return redirect('home')

        return HttpResponseRedirect(reverse("chocolate_weight"))
    
    else:
        chocolate_type_form = ChocolateTypeForm()
        items = BoxItem.objects.all().order_by("type")
        types = ChocolateType.objects.all()
        boxes = Box.objects.all()
        trays = Tray.objects.all()
        toppers = Topper.objects.all()
        flowers = Flower.objects.all()
        ribbons = Ribbon.objects.all()
        cards = Card.objects.all()


        context = {
            "chocolate_form": chocolate_type_form,
            'order': Order.objects.get(
                user=request.user,
                ordered=False
            ),
            "items": items,
            "types": types,
            "boxes": boxes,
            "trays": trays,
            "toppers": toppers,
            "flowers": flowers,
            "ribbons": ribbons,
            "cards": cards,




        }

        return render(request, 'ar/chocolate_type.html', context)



def ar_register(request):
    registered = False
    context_email =''.join(random.choice(string.ascii_lowercase) for i in range(33))

    if request.user.is_authenticated:
        order = Order.objects.get(
            user=request.user,
            ordered=False
        )
    else:
        order = None

    if request.method == "POST":
        if request.user.is_authenticated:
            not_user = request.user
            not_order = Order.objects.get(user=not_user, ordered=False)
                        
            lst = []
            for i in not_order.items.all():
                lst.append(i.item.title)
            print(lst)

        user_form = ArUserForm(data=request.POST)
        profile_form = ArProfileForm(data=request.POST)

        if user_form.is_valid() and profile_form.is_valid():
            try:
                user = user_form.save(commit=False)
                user.username = user_form.cleaned_data.get('email').lower()
                try:
                    validate_password(user.password, user)
                    print(user.password)
                except ValidationError as e:
                    print("--------------------------")
                    user_form.add_error("password", e)
                    return render(request, 'register.html', {'user_form': user_form, 'profile_form': profile_form})
                user.set_password(user.password)
                user.is_active = False
                print(user.username)
                # user.is_active = False
                user.save()
                
                order = Order.objects.create(user=user)
                if request.user.is_authenticated:
                    if User.objects.filter(username=request.user.username, password="lkasjdhflksajdhflkLKJHLkdjhf"):
                        for order_item in not_order.items.all():
                            print("hi")
                            item = OrderItem.objects.create(item=order_item.item, pieces_quantity=order_item.pieces_quantity, weight_quantity=order_item.weight_quantity, user=user, slug=order_item.item.slug, price=order_item.price, quantity_type=order_item.quantity_type)
                            if order_item.item.title == "Customize your box":
                                item.tray = order_item.tray
                                item.flower = order_item.flower
                                item.topper = order_item.topper
                                item.ribbon = order_item.ribbon
                                item.card = order_item.card
                                item.chocolate_type = order_item.chocolate_type
                                item.quantity_choice = order_item.quantity_choice
                                item.box_choice = order_item.box_choice
                                item.price = int(item.pieces_quantity) * int(order_item.price)
                                item.slug = order_item.slug
                                for i in order_item.box_items.all():
                                    order_box_item = OrderBoxItem.objects.create(user=user, item=i.item, quantity=i.quantity, ordered=i.ordered)
                                    item.box_items.add(order_box_item)
                            item.save()
                            print(item.item.title)
                            order.items.add(item)
                order.save()
                

                profile = profile_form.save(commit=False)

                profile.user = user
                profile.pin = random_digit(4)
                profile.save()
                if request.user.is_authenticated:
                    if User.objects.filter(username=request.user.username, password="lkasjdhflksajdhflkLKJHLkdjhf"):
                        request.user.delete()
                address = Address.objects.create(title="main address", area=profile.area, block=profile.block, street=profile.street, house=profile.house, floor=profile.floor, apartment=profile.apartment)
                order.address = address
                order.block = profile.block
                order.area = profile.area
                order.street = profile.street
                order.house = profile.house
                order.floor = profile.floor
                order.apartment = profile.apartment
                profile.addresses.add(address)
                profile.save()
                order.save()

            except IntegrityError:
                error = "مستخدم بهذا البريد الإلكتروني موجود بالفعل"
                user_form = ArUserForm()
                profile_form = ArProfileForm()
                context = {
                    "error": error,
                    "user_form": user_form,
                    "profile_form": profile_form,
                    "order": order
                }
                print("hello")

                return render(request, "ar/register.html", context)

            if user:

                query_email = urlencode({"email": user.email})
                base_url = reverse("ar-check-pin")

                url = f"https://www.chocolatecokw.com{base_url}?{query_email}"

                subject = 'Verification Link'
                html_message = render_to_string('ar/email_verification.html', {'url': url})
                plain_message = strip_tags(html_message)
                from_email = settings.EMAIL_HOST_USER
                to = user.email

                send_mail(subject, plain_message, from_email, [to], html_message=html_message, fail_silently=False)
                messages.success(request, 'تم ارسال رابط تفعيل الى بريدك الاكتروني')
                
                return redirect("/ar")
            else:
                return HttpResponse("No User")

            registered = True

        else:
            print("Error")
            print(user_form.errors, profile_form.errors)
    else:
        user_form = ArUserForm()
        profile_form = ArProfileForm()

    

    context = {
        "user_form": user_form,
        "profile_form": profile_form,
        "registered": registered,
        'order': order,
        "email": context_email
    }

    return render(request, 'ar/register.html', context)



def ar_user_login(request):


    if request.user.is_authenticated:
        order=Order.objects.get(user=request.user, ordered=False)
    else:
        order=None
    if request.method == "POST":

        if request.user.is_authenticated:
            fake_user = User.objects.filter(username=request.user.username, password="lkasjdhflksajdhflkLKJHLkdjhf")
            if fake_user:
                fake_user.delete()

        username = request.POST.get("username").lower()
        password = request.POST.get("password")

        user = authenticate(username=username, password=password)


        if user:

            if user.is_active:

                login(request, user)

                return redirect("ar-home")
            else:
                return redirect("ar-login")
        else:
            error = True
            return render(request, 'ar/login.html', {'error': error, "order": order})
            
    else:
        
        context = {
            "user": request.user,
            "order": order
        }
        return render(request, 'ar/login.html', context)


def ar_cart(request):

    order = Order.objects.get(user=request.user, ordered=False)

    order_items = order.items.all()
    
    print(request.user)

    context = {
        'items': order_items,
        'order': order
    }
    return render(request, 'ar/cart.html', context)




class ArCheckoutView(LoginRequiredMixin,View):
    

    def get(self, *args, **kwargs):

        if self.request.user.is_authenticated:
            try:
                order = Order.objects.get(user=self.request.user, ordered=False)
                order.order_type = None
                order.coupon = None
                order.save()
            except Order.DoesNotExist:
                order = Order.objects.create(user=self.request.user, ordered=False)
        user = self.request.user
        lst = OrderItem.objects.filter(user=user, ordered=False).values_list("item__delivery_date__number", flat=True)
        lst = list(lst)
        customized_box_items = OrderItem.objects.filter(item__customized_box=True, user=user, ordered=False)
        if customized_box_items.exists():
            lst.append(1)
            print("one appended")
        print(lst, "LIST")
        max_date = max([int(x) for x in lst])
        print(max_date, "MAX DATE")
        delivery_date = DeliveryDate.objects.get(number=max_date)
        dates_off = DateOff.objects.all()



        if delivery_date.number > 0:
            today_off = True
        else:
            today_off = False

        if delivery_date.number > 1:
            today_off = True
            tomorrow_off = True
        else:
            tomorrow_off = False

        if delivery_date.number > 2:
            day_number = delivery_date.number
        else:
            day_number = 2


        for date in dates_off:
            if date.from_date <= datetime.date.today() <= date.to_date:
                today_off = True
                break

        for date in dates_off:
            if date.from_date <= datetime.date.today() + datetime.timedelta(1) <= date.to_date:
                tomorrow_off = True
                break
        
        
        try:
            print("he")


        

        
            days = WorkingDay.objects.all()
            day = WorkingDay.objects.get(day=date_format(datetime.date.today(), "l"))
            today = datetime.datetime.now().strftime("%A")

            if day.to_time1 < datetime.datetime.now().time():
                today_disabled_time1 = True
            else:
                today_disabled_time1 = False
                
            
            if day.to_time2 < datetime.datetime.now().time():
                today_disabled_time2 = True
            else:
                today_disabled_time2 = False
                


            if day.to_time3 < datetime.datetime.now().time():
                today_disabled_time3 = True
            else:
                today_disabled_time3 = False

            print("hee")

            lst = []
            print("helloo")
            order = Order.objects.get(user=self.request.user, ordered=False)
            # for item in order.items.all():
            #     if item.flower:
            #         disabled = "disabled"
            #     else:
            #         print("something wrong", "DISABLED")
            #         disabled = "today-container"
            print("helo")
            for item in order.items.all():
                obj = {
                    "ItemName": item.item.title,
                    "Quantity": item.pieces_quantity,
                    "UnitPrice": item.item.price,
                }
                lst.append(obj)
            
            form = CheckoutForm()
            user = self.request.user
            min_price = MinimumOrder.objects.first().price
            
            pickup =  PickupPlace.objects.first()
            url = baseURL + "/v2/SendPayment"
            payload = "{\"CustomerName\": \""+ order.user.username +"\",\"NotificationOption\": \"LNK\"," \
                        "\"InvoiceValue\": "+ str(order.get_order_total()) +"," \
                        "\"CallBackUrl\": \"https://www.chocolatecokw.com/checkout-done\",} "
            headers = {'Content-Type': "application/json", 'Authorization': "Bearer " + token}
            response = requests.request("POST", url, data=payload.encode('utf-8'), headers=headers)
            print(response.text, "HII")
            res = json.loads(response.text)
            last_url = res["Data"]["InvoiceURL"]
            context = {
                'form': form,
                'order': order,
                "day": day,
                "url": last_url,
                "pickup": pickup,
                "days": days,
                "min_price": min_price,
                "today_off": today_off,
                "tomorrow_off": tomorrow_off,
                "day_number": day_number,
                "today_disabled_time1": today_disabled_time1,
                "today_disabled_time2": today_disabled_time2,
                "today_disabled_time3": today_disabled_time3,
                "days_off": json.dumps((DatesSerializer(DateOff.objects.all(), many=True).data))


            }

            
            

            if order.get_order_total() > 0:
                return render(self.request, "ar/checkout.html", context)
            else:
                print('hhh')
                return redirect('ar-home')
        except ObjectDoesNotExist:
            print("hi")
            print("------------------------------------------")
            messages.info(self.request, "You do not have an active order")
            return redirect("ar-home")

    def post(self, *args, **kwargs):
        form = CheckoutForm(self.request.POST or None)
        order_items = OrderItem.objects.filter(user=self.request.user)
        items = []
    
        order = Order.objects.get(
            user=self.request.user,
            ordered=False
        )
        try:
            if self.request.method == 'POST':
                
                if form.is_valid():
                    print(self.request.POST["day"])
                    print(self.request.POST["time"])

                    order.area = self.request.POST["shipping_address"]
                    order.address_line = self.request.POST["shipping_address2"]
                    form.save()

                    subject = 'We got an Order'
                    message = f'Phone Numberhello : {str(self.request.user.profile.number)} \n\nThe Order : {items} \n\nArea: {str(order.area)} \nAddress Line: {str(order.address_line)}\n'
                    for order_item in order_items:
                        if order_item.item.has_form:
                            message += "\n" + order_item.item.title + " Sauces: " + order_item.first_sauce + ", " + order_item.second_sauce + ", " + order_item.third_sauce + "\n"
                        if order_item.item.create_box:
                            message += "\nBox Churros: " + order_item.first_choice + ", " + order_item.second_choice + ", " + order_item.third_choice + ", " + order_item.fourth_choice + "\n"
                        if order_item.item.has_extra:
                            if order_item.chocolate_sauce:
                                message += "\nExtra chocolate sauce\n"
                            if order_item.white_sauce:
                                message += "\nExtra white sauce\n"
                            if order_item.pistachio_sauce:
                                message += "\nExtra pistachio sauce\n"
                    message += "\nTotal Price: " + str(order.get_total())
                    html_message = render_to_string('order_email.html', context={'order': order,'items': items})
                    
                            
                            
                    email_from = settings.EMAIL_HOST_USER
                    recipient_list = ['yazedraed20051@gmail.com']
                    recipient_list2 = [self.request.user.username]

                    send_mail(subject, message, email_from, recipient_list, fail_silently=False)
                    mail.send_mail("Thank you for ordering from Chocolate Co", message, email_from, recipient_list2, html_message=html_message)
                
                order_qs = Order.objects.filter(
                    user=self.request.user.profile,
                    ordered=False
                )
                if order_qs.exists():
                    order = order_qs[0]
                    # check if the order item is in the order

                    order_item = OrderItem.objects.filter(
                        user=self.request.user.profile,
                        ordered=False
                    )[0]
                    order.items.remove(order_item)
                    order_item.delete()
                Item.objects.all().update(quantity=1)

            return redirect('ar-home')
        except ObjectDoesNotExist:
            messages.warning(self.request, "You do not have an active order")
            return redirect("ar-cart")



def ar_add_to_cart_detail(request, id):

    if request.user.is_authenticated:
        pass
    else:
        username = ''.join(random.choice(string.ascii_lowercase) for i in range(10))

        user = User.objects.create(username=username, password='lkasjdhflksajdhflkLKJHLkdjhf')

        print(username)
        login(request, user)

        order = Order.objects.create(user=user)
        
    item = get_object_or_404(Item, id=id)
    # order_item, created = OrderItem.objects.get_or_create(
    #     user=request.user,
    #     item=item,
    #     ordered=False
    # )
    category = item.category
    order_qs = Order.objects.filter(user=request.user, ordered=False)
    choice = request.POST.get("choice")
    if order_qs.exists():
        order = order_qs[0]
        print(request.POST.get("quantity"))
        if order.items.filter(item__slug=item.slug, choice=choice).exists():
            order_item = OrderItem.objects.get(user=request.user, item=item, ordered=False, choice=choice)
            print("hoooooooooooooooooooooooo")
            if item.quantity_type == "Pieces":
                order_item.pieces_quantity += int(request.POST.get("quantity").split()[0])
            elif item.quantity_type == "Weight":
                order_item.weight_quantity += decimal.Decimal(request.POST.get("quantity").split()[0])
                order_item.pieces_quantity += int(request.POST.get("quantity").split()[0]) / 0.25
            order_item.save()
            return redirect("ar-detail", item.id)
        else:
            
            order_item = OrderItem.objects.create(user=request.user, item=item, choice=choice, quantity_type=item.quantity_type)
            if choice:
                order_item.has_choice = True
            print("hiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii")
            if item.quantity_type == "Pieces":
                order_item.pieces_quantity = int(request.POST.get('quantity').split()[0])
            else:
                order_item.weight_quantity = decimal.Decimal(request.POST.get('quantity').split()[0])
                order_item.pieces_quantity = int(float(request.POST.get('quantity').split()[0]) / 0.25)
            order_item.price = item.price
            order_item.save()   
            order.items.add(order_item)
            order.save()

            # Item.objects.all().update(pieces_quantity=1)

            return redirect("ar-detail", item.id)
    else:
        
        order = Order.objects.create(
            user=request.user
        )
        order_item = order_item = OrderItem.objects.create(user=request.user, item=item, choice=choice, quantity_type=item.quantity_type)
        order_item.quantity = int(request.POST.get('quantity'))
        order_item.save()
        order.items.add(order_item)
        order.save()
        return redirect("ar-detail", item.id)





class ArAccount(UserPassesTestMixin ,UpdateView):


    def test_func(self):
        return not self.request.user.is_staff

    template_name = 'ar/account.html'
    model = Profile
    form_class = ProfileForm
    success_url = '/ar'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['user'] = self.request.user
        context["order"] = Order.objects.get(user=self.request.user, ordered=False)
        return context




def ar_change_password(request):
    user = request.user
    form = ArChangePasswordForm()
    if Order.objects.filter(user=user, ordered=False).exists():
        order = Order.objects.get(user=user, ordered=False)
    else:
        order = None
    
    if request.method == "POST":
        form = ArChangePasswordForm(data=request.POST)
        if form.is_valid():
            user_password = form.cleaned_data["user_password"]
            password = form.cleaned_data["password"]
            password2 = form.cleaned_data["password2"]

            if user.check_password(user_password):
                if password == password2:
                    try:
                        validate_password(password, user)
                        print(password)
                    except ValidationError as e:
                        

                        form.add_error("password", e)
                        return render(request, "ar/reset_password.html", {"form": form, "order": order})
                    
                    user.set_password(password)
                    user.save()
                    login(request, user)
                    


                    return redirect("account", user.profile.id)
                else:
                    error = "لا يوجد تتطابق بين كلمتي المرور"
                    return render(request, "ar/reset_password.html", {"error":error, "form": form, "order": order})
            else:
                error = "كلمة المرور الخاصة بك خاطئة"
                return render(request, "ar/reset_password.html", {"error":error, "form": form, "order": order})
            
        else:
            print(form.errors)

    return render(request, "ar/reset_password.html", {"form": form})



def ar_reset_password(request):
    email = request.GET.get("email")
    user = User.objects.get(username=email)
    form = ArResetPasswordForm()
    if request.method == "POST":
        form = ArResetPasswordForm(data=request.POST)
        if form.is_valid():
            password = form.cleaned_data["password"]
            password2 = form.cleaned_data["password2"]


            if password == password2:
                try:
                    validate_password(password, user)
                    print(password)
                except ValidationError as e:
                    

                    form.add_error("password", e)
                    return render(request, "ar/reset_password.html", {"form": form})
                
                user.set_password(password)
                user.save()
                login(request, user)



                return redirect("home")
            else:
                error = "Passwords Don't match."
                return render(request, "ar/reset_password.html", {"error":error})
        else:
            print(form.errors)

    return render(request, "ar/reset_password.html", {"form": form})




def ar_forgot_password(request):

    if request.method == "POST":
        email = request.POST.get("email")
        if User.objects.filter(username=email).exists():
            
            query_email = urlencode({"email": email})
            base_url = reverse("ar-reset-password")

            url = f"https://www.chocolatecokw.com{base_url}?{query_email}"
            subject = 'Forgot Password'
            html_message = render_to_string('ar/forgot_password_email.html', {"url": url})
            plain_message = strip_tags(html_message)
            from_email = settings.EMAIL_HOST_USER
            to = email


            send_mail(subject, plain_message, from_email, [to], html_message=html_message, fail_silently=False)
            messages.success(request, 'لقد تم ارسال رابط لإعادة تعيين كلمة المرور')

            return redirect("ar-home")
        else:
            error = "لا يوجد مستخدم بهذا البريد الإلكتروني"
            return render(request, "ar/forgot_password.html", {"error": error})

    return render(request, "ar/forgot_password.html")

def ar_orders_history(request):

    user = request.user

    print(user)

    orders = Order.objects.filter(user=user, ordered=True).order_by("-ordered_date")
    order = Order.objects.get(user=user, ordered=False)

    context = {
        "orders": orders,
        "order": order
    }


    return render(request, "ar/orders_history.html", context)



def ar_user_logout(request):


    fake_user = User.objects.filter(username=request.user.username, password="lkasjdhflksajdhflkLKJHLkdjhf")
    if fake_user:
        fake_user.delete()
    logout(request)
    return redirect('ar-home')



def ar_favorites(request):
    if request.user.is_authenticated:
        order = Order.objects.get(user=request.user, ordered=False)
        profile = request.user.profile
        favorite_items = profile.favorites.all()

    else:
        return redirect("ar-home")

    context = {
        "items": favorite_items,
        "order": order,

    }

    return render(request, "ar/favorites.html", context)





def ar_search(request):


    if request.user.is_authenticated:
        order = Order.objects.get(user=request.user, ordered=False)
    else:
        order = None

    if request.method == "GET":
        return redirect("ar-home")

    if request.method == "POST":
        query = request.POST.get("search")


        items = list(Item.objects.filter(ar_title__icontains=query).order_by("sub_category"))
        items.extend(list(Item.objects.filter(ar_description__icontains=query).order_by("sub_category")))


        context = {
            "items": items,
            "results": len(items),
            "query": query,
            "order": order
        }




        return render(request, "ar/search.html", context)     



def ar_checkout(request):
    
    if request.method == "POST":
        
        user = request.user
        order = Order.objects.get(ordered=False, user=user)
        # for item in order.items.all():
            
        #     item.quantity = request.POST.get(f"{ item.item.title }-quantity")
        #     item.save()


        note = request.POST.get("note")


        order.order_type = request.POST.get("type")
        order.save()


        if request.POST.get("day") == "today":
            order.order_day = date_format(datetime.date.today(), "l")
            day = date_format(datetime.date.today(), "d/m/Y")
        elif request.POST.get("day") == "tomorrow":
            order.order_day = date_format(datetime.date.today() + datetime.timedelta(days=1), "l")
            day = date_format(datetime.date.today() + datetime.timedelta(days=1), "d/m/Y")
        else:
            day = request.POST.get("day")


        time = request.POST.get("time")
    
        order.delivery_date = f"{ day } | { time }"

        order.order_time = request.POST.get("time")

        if note:
            order.note = note

        order.save()

      

        
        
       
        # url = baseURL + "/v2/SendPayment"
        # payload = "{\"CustomerName\": \""+ order.user.username +"\",\"NotificationOption\": \"LNK\"," \
        #             "\"InvoiceValue\": "+ str(order.get_order_total()) +"," \
        #             "\"CallBackUrl\": \"https://www.chocolatecokw.com/ar/checkout-done\",} "
        # headers = {'Content-Type': "application/json", 'Authorization': "Bearer " + token}
        # response = requests.request("POST", url, data=payload, headers=headers)
        # print(response.text)
        # res = json.loads(response.text)
        # last_url = res["Data"]["InvoiceURL"]
        return redirect("ar-order-summary")




def ar_visitor(request):

    user = request.user
    order = Order.objects.get(user=user, ordered=False)

    form = ArProfileForm()

    if request.method == "POST":

       


        
       
        area = request.POST.get("area")
        block = request.POST.get("block")
        street = request.POST.get("street")
        house = request.POST.get("house")
        floor = request.POST.get("floor")
        apartment =  request.POST.get("apartment")

        request.session["area"] = area
        request.session["block"] = block
        request.session["street"] = street
        request.session["house"] = house
        request.session["floor"] = floor
        request.session["apartment"] = apartment          


        return redirect("ar-checkout")  


            
            
    context = {
        "order": order,
        "profile_form": form
    }




    return render(request, "ar/gifting_options.html", context)




def ar_checkout_done(request):

    track_id = request.GET.get("paymentId")



    
    user = request.user
    order = Order.objects.get(user=user, ordered=False)
    if Profile.objects.filter(user=user).exists():
        
        order.block = user.profile.block
        order.area = user.profile.area
        order.street = user.profile.street
        order.house = user.profile.house
        order.user_name = user.profile.name
        order.phone_number = user.profile.phone_number
    else:
        print("jiiii")
        print(order)
        order.block = request.session["block"]
        order.area = request.session["area"]
        order.street = request.session["street"]
        order.house = request.session["house"]
        order.user_name = request.session["name"]
        order.phone_number = request.session["number"]



    order.ordered_date = datetime.datetime.now()
    order.save()



    context = {
        "ordered_date": order.ordered_date,
        "items": order.items.all(),
        "delivery_charge": DeliveryCharge.objects.first().price,
        "order": order
    }

    



    if Profile.objects.filter(user=user).exists():
        if Transaction.objects.filter(order=order, user=user, done=False).exists():
            pass
        else:
            Transaction.objects.create(order=order, amount=order.get_order_total(), user=user, track_id=track_id)
    else:
        if Transaction.objects.filter(order=order, user_name=request.session["name"], done=False):
            pass
        else:
            Transaction.objects.create(order=order, amount=order.get_order_total(), user_name=request.session["name"], track_id=track_id)

    return render(request, "ar/checkout_loading.html", context)



def ar_socket_checkout(request):

    user = request.user
    order = Order.objects.get(user=user, ordered=False)
    print(user)
    
    transaction = Transaction.objects.get(order=order, done=False)
    transaction.done = True
    transaction.date = datetime.datetime.now()
    transaction.save()
    
    for item in order.items.all():
        item.ordered = True
        item.ordered_date = datetime.datetime.now()
        item.save()

    order.ordered = True 
    if Discount.objects.filter(start_date__lte=datetime.datetime.now().date(), end_date__gte=datetime.datetime.now().date()).exists():
        order.discount = Discount.objects.get(start_date__lte=datetime.datetime.now().date(), end_date__gte=datetime.datetime.now().date()).value
    else:
        order.discount = 0
    order.ordered_date = datetime.datetime.now()
    order.save()

    url = reverse("ar-home")
    return redirect(f"{ url }?checkout=True")


def ar_checkout_failed(request):

    url = reverse("ar-home")

    return redirect(f"{ url }?checkout_failed=True")



def ar_terms_and_conditions(request):

    if request.user.is_authenticated:
        order = Order.objects.get(user=request.user, ordered=False)
    else:
        order = None

    return render(request, "ar/terms_and_conditions.html", {"order": order})


def ar_privacy_policy(request):

    if request.user.is_authenticated:
        order = Order.objects.get(user=request.user, ordered=False)
    else:
        order = None

    return render(request, "ar/privacy_policy.html", {"order": order})


def ar_order_summary(request):

    user = request.user

    price = DeliveryCharge.objects.first().price

    order = Order.objects.get(user=user, ordered=False)

    if Discount.objects.filter(start_date__lte=datetime.datetime.now().date(), end_date__gte=datetime.datetime.now().date()).exists():
        discount = Discount.objects.get(start_date__lte=datetime.datetime.now().date(), end_date__gte=datetime.datetime.now().date())
    else:
        discount = None

    if Profile.objects.filter(user=user).exists():
        name = order.user.profile.name
        number = order.user.profile.phone_number
        email = order.user.username
    else:
        name = request.session["name"]
        number = request.session["number"]


    url = baseURL + "/v2/SendPayment"
    payload = "{\"CustomerName\": \""+ name +"\",\"CustomerMobile\": \""+ number +"\",\"NotificationOption\": \"LNK\"," \
                "\"InvoiceValue\": "+ str(order.get_order_total()) +"," \
                "\"CallBackUrl\": \"https://www.chocolatecokw.com/ar/checkout-done\",\"ErrorUrl\": \"https://www.chocolatecokw.com/ar/checkout-failed\"} "
    headers = {'Content-Type': "application/json", 'Authorization': "Bearer " + token}
    response = requests.request("POST", url, data=payload.encode('utf-8'), headers=headers)
    print(response.text)
    res = json.loads(response.text)
    last_url = res["Data"]["InvoiceURL"]


    context = {
        "order": order,
        "delivery_charge": price,
        "url": last_url,
        "discount": discount
    }




    return render(request, "ar/order_summary.html", context)



def ar_remove_box_from_cart(request, id):
    try:
        order = Order.objects.get(user=request.user, ordered=False)
        order_item = OrderItem.objects.get(id=id, user=request.user)
        order.items.remove(order_item)
        order_item.delete()
        return redirect("ar-cart")
    except ObjectDoesNotExist:

        return redirect("ar-cart")


def ar_email_verification(request):
    

    if request.user.is_authenticated:
        fake_user = User.objects.filter(username=request.user.username, password="lkasjdhflksajdhflkLKJHLkdjhf").exists()
        print(fake_user)        
        if fake_user:
            User.objects.filter(username=request.user.username, password="lkasjdhflksajdhflkLKJHLkdjhf").delete()
            print("ahem")
        else:
            print(fake_user, "no user")
    else:
        print("wow")
    email = request.GET["email"]

    user = User.objects.get(email=email)

    user.is_active = True
    user.save()
    login(request, user)
    return redirect("/ar")



def ar_create_visitor(request):


    if request.method == "POST":
        request.session["name"] = request.POST.get("name")
        request.session["number"] = request.POST.get("number")




        print(request.session["name"])
        print(request.session["number"])


        return redirect("ar-visitor")





########### PAYMENT DATA ###########
baseURL = "https://api.myfatoorah.com"
token = 'gnq6OHnnqdn3YUmZJ5-Ari6zP5XvsX4k677DeTYK65YZrQIa0M-7b3ksOY63glMKS7H2u_95tbkKRD4D8kAKxMCtsndAEePz4RKDrxf0EtG_py4NxsEjvxJc-_Yh8byyBfQAfv7BJvRVNNHfrAwCMGxT-FLeWbDsiP40KkQ3sTKW2Y6LruYSuUCf65npXDe2PPoyw3n22ZB6GeiPGhAFWOKGV1aRel1JNEmVMNOVskdjmESFGsQ2cSwH9ZUnD3DOvmBvHTsrhPGnH4TwjX2yjPFxDjbh5jg0dGTPLCGZHkv-bdGvrmBzs9lGsP-nxSbWZs81ywKkTBwanMTFZscv-jjOjUFahqEObMDj0hPeezxOE7Opv6EroPTWrdUbVnQDGNEXBWBMmDuaKp0twLBObQHRHu4jL1lUW2ybZNEv0dg84rHem_ekvRSzxTHp7jGej7yu-3TTNhWl-OrE-VRlQpht-icf3OlVX4vWRR19eOAxe8CtXg0f1hVtbckL-MH6seS2G5ZGNZuV0IBu836I0Mi4LblKrPb7zvyn452KR0-ee0ZWADmAOvxX65DnanCg3sOn4LTzQrJED7j3X75qYrGk91oglD09h4ENyLJKLbRkb63Qszu3B0GANdk_JduKwhArRnqJrh92Ad5pZ4s4d1vI0usFW6tNPzrP46_W1B6wUG2DKhRD7vyDdraTN5-9chLqfg' #token value to be placed here

########### TEST DATA ###########
# baseURL = "https://apitest.myfatoorah.com"
# token = 'rLtt6JWvbUHDDhsZnfpAhpYk4dxYDQkbcPTyGaKp2TYqQgG7FGZ5Th_WD53Oq8Ebz6A53njUoo1w3pjU1D4vs_ZMqFiz_j0urb_BH9Oq9VZoKFoJEDAbRZepGcQanImyYrry7Kt6MnMdgfG5jn4HngWoRdKduNNyP4kzcp3mRv7x00ahkm9LAK7ZRieg7k1PDAnBIOG3EyVSJ5kK4WLMvYr7sCwHbHcu4A5WwelxYK0GMJy37bNAarSJDFQsJ2ZvJjvMDmfWwDVFEVe_5tOomfVNt6bOg9mexbGjMrnHBnKnZR1vQbBtQieDlQepzTZMuQrSuKn-t5XZM7V6fCW7oP-uXGX-sMOajeX65JOf6XVpk29DP6ro8WTAflCDANC193yof8-f5_EYY-3hXhJj7RBXmizDpneEQDSaSz5sFk0sV5qPcARJ9zGG73vuGFyenjPPmtDtXtpx35A-BVcOSBYVIWe9kndG3nclfefjKEuZ3m4jL9Gg1h2JBvmXSMYiZtp9MR5I6pvbvylU_PP5xJFSjVTIz7IQSjcVGO41npnwIxRXNRxFOdIUHn0tjQ-7LwvEcTXyPsHXcMD8WtgBh-wxR8aKX7WPSsT1O8d8reb2aR7K3rkV3K82K_0OgawImEpwSvp9MNKynEAJQS6ZHe_J_l77652xwPNxMRTMASk1ZsJL' #token value to be placed here









def send_confirmatiion_email(request):

    user = User.objects.get(username="yazedraed2005@gmail.com")
    order = Order.objects.get(user=user)
    print(order.items.all())

    context = {
        "ordered_date": order.ordered_date,
        "items": order.items.all(),
        "delivery_charge": DeliveryCharge.objects.first().price,
        "order": order
    }

    

    subject = 'Thank you for your order'
    html_message = render_to_string('emails/order_email.html', context)
    plain_message = strip_tags(html_message)
    from_email = settings.EMAIL_HOST_USER
    to = "yazedraed2005@gmail.com"

    

    # send_mail(subject, plain_message, from_email, [to], html_message=html_message, fail_silently=False)



    return render(request, "emails/ar/order_email.html", context)





def images(request):


    objects = Item.objects.all().exclude(title="Customize your box")

    for obj in objects:
        if obj.image:
            obj.picture = cloudinary.uploader.upload_image(obj.image.url.replace("dt3kozoud", "do2xnv4uq"))
        if obj.image2:
            obj.picture2 = cloudinary.uploader.upload_image(obj.image2.url.replace("dt3kozoud", "do2xnv4uq"))
        if obj.image3:
            obj.picture3 = cloudinary.uploader.upload_image(obj.image3.url.replace("dt3kozoud", "do2xnv4uq"))
        obj.save()
        print("Updated.")

    
    print("Done!")
        


@api_view(["POST", ])
def submit_coupon(request):

    coupon_code = request.POST.get("coupon")
    user = request.user
    order = Order.objects.get(user=user, ordered=False)
    print(order.id)
    if Coupon.objects.filter(code=coupon_code).exists():
        coupon = Coupon.objects.get(code=coupon_code)
        if coupon.end_date <= datetime.datetime.now().date():
            resp = {
                "status": {
                    "code": 400,
                    "message": "failed",
                },
                "results": "Invalid Coupon Code."
            }

            return Response(resp)
        else:

            if order.coupon:
                if coupon_code != order.coupon.code:
                    resp = {
                        "status": {
                            "code": 100,
                            "message": "OK"
                        },
                        "results": order.coupon.value
                    }

                    return Response(resp)
                else:
                    resp = {
                        "status": {
                            "code": 400,
                            "message": "failed"
                        },
                        "results": "This coupon is already applied on you current order."
                    }

                    return Response(resp)
            else:
                order.coupon = coupon
                order.save()


                if order.order_type == "Delivery":
                    resp = {
                        "status": {
                            "code": 200,
                            "message": "OK"
                        },
                        "results": {
                            "before": order.get_order_sub(),
                            "after": order.get_sub_coupon(),
                            "grandTotal": order.get_order_total()
                        }
                    }

                else:
                    resp = {
                        "status": {
                            "code": 200,
                            "message": "OK"
                        },
                        "results": {
                            "before": order.get_total_without_coupon(),
                            "after": order.get_order_total(),
                        }
                    }


                return Response(resp)
    else:

        resp = {
            "status": {
                "code": 400,
                "message": "failed",
            },
            "results": "Invalid Coupon Code."
        }

        return Response(resp)




@api_view(["POST", ])
def replace_coupon(request):

    coupon_code = request.POST.get("coupon")
    user = request.user

    if Coupon.objects.filter(code=coupon_code).exists():
        order = Order.objects.get(user=user, ordered=False)
        coupon = Coupon.objects.get(code=coupon_code)

        order.coupon = coupon
        order.save()

        if order.order_type == "Delivery":

            resp = {
                "status": {
                    "code": 200,
                    "message": "OK"
                },
                "results": {
                    "total": order.get_sub_coupon(),
                    "grandTotal": order.get_order_total() 
                }
            }
        else:
            resp = {
                "status": {
                    "code": 200,
                    "message": "OK"
                },
                "results": {
                    "total": order.get_order_total(),
                    "grandTotal": order.get_order_total() 
                }
            }


        return Response(resp)




        




@api_view(["POST", ])
def ar_submit_coupon(request):

    coupon_code = request.POST.get("coupon")
    user = request.user
    order = Order.objects.get(user=user, ordered=False)
    print(order.id)
    if Coupon.objects.filter(code=coupon_code).exists():
        coupon = Coupon.objects.get(code=coupon_code)

        if coupon.end_date <= datetime.datetime.now().date():
            resp = {
                "status": {
                    "code": 400,
                    "message": "failed",
                },
                "results": "رمز الكوبون غير صحيح."
            }
            return Response(resp)
        else:
            if order.coupon:
                if coupon_code != order.coupon.code:
                    resp = {
                        "status": {
                            "code": 100,
                            "message": "OK"
                        },
                        "results": order.coupon.value
                    }

                    return Response(resp)
                else:
                    resp = {
                        "status": {
                            "code": 400,
                            "message": "failed"
                        },
                        "results": "هذا الكوبون فعال على طلبك الحالي."
                    }

                    return Response(resp)
            else:
                order.coupon = coupon
                order.save()

                resp = {
                    "status": {
                        "code": 200,
                        "message": "OK"
                    },
                    "results": {
                        "before": order.get_total_without_coupon(),
                        "after": order.get_order_total()
                    }
                }


                return Response(resp)
    else:

        resp = {
            "status": {
                "code": 400,
                "message": "failed",
            },
            "results": "رمز الكوبون غير صحيح."
        }

        return Response(resp)




@api_view(["POST", ])
def ar_replace_coupon(request):

    coupon_code = request.POST.get("coupon")
    user = request.user

    if Coupon.objects.filter(code=coupon_code).exists():
        order = Order.objects.get(user=user, ordered=False)
        coupon = Coupon.objects.get(code=coupon_code)

        order.coupon = coupon
        order.save()

        resp = {
            "status": {
                "code": 200,
                "message": "OK"
            },
            "results": order.get_order_total()
        }


        return Response(resp)
