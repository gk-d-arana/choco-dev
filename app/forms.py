from django import forms
from django.forms import fields, widgets
from .models import BoxImage, Order, User, Profile, Item, OrderItem, BoxImage, CHOCOLATE_TYPE, QUANTITY_CHOICE, BannerImage





class UserForm(forms.ModelForm):
    
    class Meta:
        model = User
        fields = ('email', 'password')
        widgets = {
            "email": forms.EmailInput(attrs={"placeholder": "Email", "class": "input w-input"}),
            "password": forms.PasswordInput(attrs={"placeholder": "Password", "class": "input w-input"})
        }

        def __init__(self, *args, **kwargs):
            super(UserForm, self).__init__(*args, **kwargs)
            self.fields['email'].required = True
            self.fields['password'].required = True




class ArUserForm(forms.ModelForm):
    
    class Meta:
        model = User
        fields = ('email', 'password')
        widgets = {
            "email": forms.EmailInput(attrs={"placeholder": "البريد الإلكتروني", "class": "input w-input", "style": "text-align: right"}),
            "password": forms.PasswordInput(attrs={"placeholder": "كلمة المرور", "class": "input w-input"})
        }

        def __init__(self, *args, **kwargs):
            super(UserForm, self).__init__(*args, **kwargs)
            self.fields['email'].required = True
            self.fields['password'].required = True





class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ("name", 'phone_number', 'area', 'block', 'street', 'house', 'floor', 'apartment')
        widgets = {
            "name": forms.TextInput(attrs={"placeholder": "Name", "class": "input w-input"}),
            "phone_number": forms.TextInput(attrs={"placeholder": "Phone Number", "class": "input w-input", "minlength": "8"}),
            "area": forms.TextInput(attrs={"placeholder": "Area", "class": "input w-input"}),
            "block": forms.TextInput(attrs={"placeholder": "Block", "class": "input w-input"}),
            "street": forms.TextInput(attrs={"placeholder": "Street", "class": "input w-input"}),
            "house": forms.TextInput(attrs={"placeholder": "House/Building", "class": "input w-input"}),
            "floor": forms.TextInput(attrs={"placeholder": "Floor No", "class": "input w-input"}),
            "apartment": forms.TextInput(attrs={"placeholder": "Apartment No", "class": "input w-input"}),



        }


        def __init__(self, *args, **kwargs):
            super(ProfileForm, self).__init__(*args, **kwargs)
            self.fields['address2'].required = True
            self.fields['phone_number'].required = True
            self.fields['area'].required = True
            self.fields['block'].required = True
            self.fields['street'].required = True
            self.fields['house'].required = True



class ArProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ("name", 'phone_number', 'area', 'block', 'street', 'house', 'floor', 'apartment')
        widgets = {
            "name": forms.TextInput(attrs={"placeholder": "الإسم", "class": "input w-input"}),
            "phone_number": forms.TextInput(attrs={"placeholder": "رقم الهاتف", "class": "input w-input", "minlength": "8"}),
            "area": forms.TextInput(attrs={"placeholder": "المنطقة", "class": "input w-input"}),
            "block": forms.TextInput(attrs={"placeholder": "القطعة", "class": "input w-input"}),
            "street": forms.TextInput(attrs={"placeholder": "الشارع", "class": "input w-input"}),
            "house": forms.TextInput(attrs={"placeholder": "المنزل", "class": "input w-input"}),
            "floor": forms.TextInput(attrs={"placeholder": "الطابق", "class": "input w-input"}),
            "apartment": forms.TextInput(attrs={"placeholder": "الشقة", "class": "input w-input"}),



        }


        def __init__(self, *args, **kwargs):
            super(ProfileForm, self).__init__(*args, **kwargs)
            self.fields['address2'].required = True
            self.fields['phone_number'].required = True
            self.fields['area'].required = True
            self.fields['block'].required = True
            self.fields['street'].required = True
            self.fields['house'].required = True




class ChocolateTypeForm(forms.ModelForm):
        class Meta:
            model = OrderItem
            fields = ("chocolate_type",)
            widgets = {
                'chocolate_type': forms.RadioSelect(attrs={"class": "form-check-input"})
            }



class QuantityChoiceForm(forms.ModelForm):
    class Meta:
        model = OrderItem
        fields = ("quantity_choice", )
        widgets = {
            'quantity_choice': forms.RadioSelect(attrs={"class": "form-check-input"})
        }



class CheckoutForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ('area', 'address_line')
        widgets = {
            'address_line': forms.TextInput(attrs={'class': 'form-control', 'readonly': 'true'}),
            'area': forms.TextInput(attrs={'class': 'form-control', 'readonly': 'true'})
        }




class BoxImageForm(forms.Form):

    images = forms.ModelChoiceField(queryset=BoxImage.objects.all())



class AddCategoryItem(forms.ModelForm):

    class Meta:
        model = Item
        fields = ("title", "description", "price", "picture", "slug")

   



class ResetPasswordForm(forms.Form):

    password = forms.CharField(widget=forms.PasswordInput(attrs={"class":"input w-input", "placeholder": "New Password"}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={"class":"input w-input", "placeholder": "Confirm New Password"}))

    widgets = {
        "password": forms.PasswordInput(attrs={"class":"input w-input"}),
        "password2": forms.PasswordInput(attrs={"class":"input w-input"}),
    }


    def __init__(self, *args, **kwargs):
        super(ResetPasswordForm, self).__init__(*args, **kwargs)
        self.fields["password"].label = ""
        self.fields["password2"].label = ""



class ChangePasswordForm(forms.Form):

    user_password = forms.CharField(widget=forms.PasswordInput(attrs={"class":"input w-input", "placeholder": "Enter Your Password"}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={"class":"input w-input", "placeholder": "New Password"}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={"class":"input w-input", "placeholder": "Confirm New Password"}))

    widgets = {
        "password": forms.PasswordInput(attrs={"class":"input w-input"}),
        "password2": forms.PasswordInput(attrs={"class":"input w-input"}),
        "user_password": forms.PasswordInput(attrs={"class":"input w-input"}),

    }


    def __init__(self, *args, **kwargs):
        super(ChangePasswordForm, self).__init__(*args, **kwargs)
        self.fields["user_password"].label = ""
        self.fields["password"].label = ""
        self.fields["password2"].label = ""


class ArChangePasswordForm(forms.Form):

    user_password = forms.CharField(widget=forms.PasswordInput(attrs={"class":"input w-input", "placeholder": "كلمة المرور الخاصة بك"}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={"class":"input w-input", "placeholder": "كلمة المرور الجديدة"}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={"class":"input w-input", "placeholder": "تأكيد كلمة المرور"}))

    widgets = {
        "password": forms.PasswordInput(attrs={"class":"input w-input"}),
        "password2": forms.PasswordInput(attrs={"class":"input w-input"}),
        "user_password": forms.PasswordInput(attrs={"class":"input w-input"}),

    }


    def __init__(self, *args, **kwargs):
        super(ArChangePasswordForm, self).__init__(*args, **kwargs)
        self.fields["user_password"].label = ""
        self.fields["password"].label = ""
        self.fields["password2"].label = ""




class ArResetPasswordForm(forms.Form):

    password = forms.CharField(widget=forms.PasswordInput(attrs={"class":"input w-input", "placeholder": "كلمة المرور الجديدة"}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={"class":"input w-input", "placeholder": "تأكيد كلمة المرور"}))

    widgets = {
        "password": forms.PasswordInput(attrs={"class":"input w-input"}),
        "password2": forms.PasswordInput(attrs={"class":"input w-input"}),
    }


    def __init__(self, *args, **kwargs):
        super(ArResetPasswordForm, self).__init__(*args, **kwargs)
        self.fields["password"].label = ""
        self.fields["password2"].label = ""