from django.contrib import admin
from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('', views.index, name='home'),
    

    path('register', views.register, name="register"),
    path('login', views.user_login, name="login"),
    path('logout', views.user_logout, name="logout"),
    path('add-to-cart/<id>', views.add_to_cart, name="add"),
    path('add-to-cart-detail/<id>', views.add_to_cart_detail, name="add-detail"),

    path('remove-from-cart/<id>', views.remove_from_cart, name="remove"),
    path('product/<id>', views.detail_view, name="detail"),
    path('item-occasions', views.occasions_list, name="occasions"),
    path("customize-your-box", views.chocolate_type, name="chocolate_type"),
    # path("customize-your-box/chocolate-weight", views.chocolate_weight, name="chocolate_weight"),
    # path("customize-your-box/chocolate-box", views.chocolate_box, name="chocolate_box"),
    # path("customize-your-box/box-items", views.box_items_choice, name="box-items-choice"),
    path("customize-your-box/box-items-post", views.box_items_choice_post, name="box-items-choice-post"),
    # path('customize-your-box/done', views.custom_box_done, name="box-done"),
    path('cart', views.cart, name="cart"),
    path('add-quantity/<slug>', views.add_quantity, name="add-quantity"),
    path('remove-quantity/<id>', views.remove_quantity, name="remove-quantity"),
    path('checkout', views.CheckoutView.as_view(), name='checkout'),
    path('checkout-post', views.checkout, name='checkout-post'),
    path('clear-cart', views.clear_cart, name="clear-cart"),
    path('account/<pk>', views.Account.as_view(), name="account"),
    path('about-us', views.about_us, name="about-us"),
    path("ajax-add/<id>", views.ajax_add, name="ajax-add"),
    path("add-box-quantity/<slug>", views.add_box_quantity, name="add-box-quantity"),
    path("remove-box-quantity/<id>", views.remove_box_quantity, name="remove-box-quantity"),
    path("remove-box-from-cart/<id>", views.remove_box_from_cart, name="remove-box-from-cart"),
    path("add-checkout/<id>", views.add_check, name="add-check"),
    path("remove-checkout/<slug>", views.remove_check, name="remove-check"),
    path("add-box-checkout/<slug>", views.add_box_quantity_check, name="add-box-check"),
    path("remove-box-checkout/<slug>", views.remove_box_check, name="remove-box-check"),
    path("operation", views.operation, name="op-index"),
    path("operation/login", views.operation_login, name="op-login"),
    path("operation/order_details/<id>", views.order_details, name="op-order-details"),
    path("socket-checkout", views.socket_checkout, name="socket-checkout"),
    path("checkout-done", views.checkout_done, name="checkout-done"),
    path("verify-email", views.email_verification, name="check-pin"),
    path("send-payment", views.send_payment, name="send-payment"),
    path("ar-send-payment", views.ar_send_payment, name="ar-send-payment"),

    path("add-favorite/<id>", views.add_favorite, name="add-favorite"),
    path("remove-favorite/<id>", views.remove_favorite, name="remove-favorite"),
    path("add-favorite-detail/<id>", views.add_favorite_detail, name="add-favorite-detail"),
    path("remove-favorite-detail/<id>", views.remove_favorite_detail, name="remove-favorite-detail"),
    path("search-results", views.search, name="search"),
    path("favorites", views.favorites, name="favorites"),
    path("category-items/<id>", views.categories_home, name="category-items"),
    path("forgot-password", views.forgot_password, name="forgot-password"),
    path("reset-password", views.reset_password, name="reset-password"),
    path("change-password", views.change_password, name="change-password"),
    path("orders-history", views.orders_history, name="orders-history"),
    path("visitor", views.visitor, name="visitor"),
    path("terms-conditions", views.terms_and_conditions, name="terms-conditions"),
    path("privacy-policy", views.privacy_policy, name="privacy-policy"),
    path("add-visits", views.add_visits, name="add-visits"),
    path("checkout-failed", views.checkout_failed, name="checkout-failed"),


    path("submit-coupon", views.submit_coupon, name="submit-coupon"),
    path("replace-coupon", views.replace_coupon, name="replace-coupon"),


    path("ar-submit-coupon", views.ar_submit_coupon, name="ar-submit-coupon"),
    path("ar-replace-coupon", views.ar_replace_coupon, name="ar-replace-coupon"),






    path("create-visitor", views.create_visitor, name="create-visitor"),

    path("order-summary", views.order_summary, name="order-summary"),



    path("subscribe", views.subscribe, name="subscribe"),
    path("contact-us", views.contact_us_post, name="contact-us"),


    



    path("send-conf-email", views.send_confirmatiion_email),

    




    







    path('ar/', views.ar_index, name='ar-home'),
    path('ar/category-items/<id>', views.ar_categories_home, name='ar-category-items'),
    path('ar/product/<id>', views.ar_detail_view, name='ar-detail'),
    path("ar/customize-your-box", views.ar_chocolate_type, name="ar_chocolate_type"),
    path("ar/about-us", views.ar_about_us, name="ar-about-us"),
    path("ar/register", views.ar_register, name="ar-register"),
    path("ar/login", views.ar_user_login, name="ar-login"),
    path("ar/logout", views.ar_user_logout, name="ar-logout"),
    path("ar/cart", views.ar_cart, name="ar-cart"),
    path('ar/checkout', views.ArCheckoutView.as_view(), name='ar-checkout'),
    path('ar/account/<pk>', views.ArAccount.as_view(), name='ar-account'),
    path('ar/change-password', views.ar_change_password, name='ar-change-password'),
    path('ar/reset-password', views.ar_reset_password, name='ar-reset-password'),
    path('ar/forgot-password', views.ar_forgot_password, name='ar-forgot-password'),


    path('ar/orders-history', views.ar_orders_history, name='ar-orders-history'),
    path('ar/favorites', views.ar_favorites, name='ar-favorites'),
    path("ar/search-results", views.ar_search, name="ar-search"),
    path('ar/checkout-post', views.ar_checkout, name='ar-checkout-post'),
    path("ar/visitor", views.ar_visitor, name="ar-visitor"),
    path("ar/checkout-done", views.ar_checkout_done, name="ar-checkout-done"),
    path("ar/socket-checkout", views.ar_socket_checkout, name="ar-socket-checkout"),
    path("ar/terms-conditions", views.ar_terms_and_conditions, name="ar-terms-conditions"),
    path("ar/order-summary", views.ar_order_summary, name="ar-order-summary"),
    path("ar/privacy-policy", views.ar_privacy_policy, name="ar-privacy-policy"),
    path("ar-remove-box-from-cart/<id>", views.ar_remove_box_from_cart, name="ar-remove-box-from-cart"),
    path("ar/verify-email", views.ar_email_verification, name="ar-check-pin"),
    path("ar/checkout-failed", views.ar_checkout_failed, name="ar-checkout-failed"),
    path("ar/create-visitor", views.ar_create_visitor, name="ar-create-visitor"),




    path("images", views.images)














    
    










]


# urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)