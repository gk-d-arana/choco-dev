# Generated by Django 2.2.24 on 2022-02-07 10:24

import app.validators
import cloudinary.models
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0206_auto_20220207_1208'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aboutuscontent',
            name='image',
            field=cloudinary.models.CloudinaryField(max_length=255, null=True, validators=[app.validators.cloudinary_size], verbose_name='image'),
        ),
        migrations.AlterField(
            model_name='bannerimage',
            name='image1',
            field=cloudinary.models.CloudinaryField(blank=True, max_length=255, null=True, validators=[app.validators.cloudinary_size], verbose_name='image1'),
        ),
        migrations.AlterField(
            model_name='bannerimage',
            name='image2',
            field=cloudinary.models.CloudinaryField(blank=True, max_length=255, null=True, validators=[app.validators.cloudinary_size], verbose_name='image2'),
        ),
        migrations.AlterField(
            model_name='bannerimage',
            name='image3',
            field=cloudinary.models.CloudinaryField(blank=True, max_length=255, null=True, validators=[app.validators.cloudinary_size], verbose_name='image3'),
        ),
        migrations.AlterField(
            model_name='bannerimage',
            name='image4',
            field=cloudinary.models.CloudinaryField(blank=True, max_length=255, null=True, validators=[app.validators.cloudinary_size], verbose_name='image4'),
        ),
        migrations.AlterField(
            model_name='bannerimage',
            name='image5',
            field=cloudinary.models.CloudinaryField(blank=True, max_length=255, null=True, validators=[app.validators.cloudinary_size], verbose_name='image5'),
        ),
        migrations.AlterField(
            model_name='box',
            name='image',
            field=cloudinary.models.CloudinaryField(max_length=255, validators=[app.validators.cloudinary_size], verbose_name='image'),
        ),
        migrations.AlterField(
            model_name='boxitem',
            name='image',
            field=cloudinary.models.CloudinaryField(blank=True, max_length=255, null=True, validators=[app.validators.cloudinary_size], verbose_name='images'),
        ),
        migrations.AlterField(
            model_name='card',
            name='image',
            field=cloudinary.models.CloudinaryField(blank=True, max_length=255, null=True, validators=[app.validators.cloudinary_size], verbose_name='image'),
        ),
        migrations.AlterField(
            model_name='category',
            name='banner_image',
            field=cloudinary.models.CloudinaryField(blank=True, max_length=255, null=True, validators=[app.validators.cloudinary_size], verbose_name='image'),
        ),
        migrations.AlterField(
            model_name='category',
            name='image',
            field=cloudinary.models.CloudinaryField(blank=True, max_length=255, null=True, validators=[app.validators.cloudinary_size], verbose_name='image'),
        ),
        migrations.AlterField(
            model_name='chocolatetype',
            name='image',
            field=cloudinary.models.CloudinaryField(max_length=255, validators=[app.validators.cloudinary_size], verbose_name='image'),
        ),
        migrations.AlterField(
            model_name='flower',
            name='image',
            field=cloudinary.models.CloudinaryField(blank=True, max_length=255, null=True, validators=[app.validators.cloudinary_size], verbose_name='image'),
        ),
        migrations.AlterField(
            model_name='item',
            name='picture',
            field=cloudinary.models.CloudinaryField(blank=True, max_length=255, null=True, validators=[app.validators.cloudinary_size], verbose_name='picture'),
        ),
        migrations.AlterField(
            model_name='item',
            name='picture2',
            field=cloudinary.models.CloudinaryField(blank=True, max_length=255, null=True, validators=[app.validators.cloudinary_size], verbose_name='picture2'),
        ),
        migrations.AlterField(
            model_name='item',
            name='picture3',
            field=cloudinary.models.CloudinaryField(blank=True, max_length=255, null=True, validators=[app.validators.cloudinary_size], verbose_name='picture3'),
        ),
        migrations.AlterField(
            model_name='package',
            name='image',
            field=cloudinary.models.CloudinaryField(blank=True, max_length=255, null=True, validators=[app.validators.cloudinary_size], verbose_name='image'),
        ),
        migrations.AlterField(
            model_name='ribbon',
            name='image',
            field=cloudinary.models.CloudinaryField(blank=True, max_length=255, null=True, validators=[app.validators.cloudinary_size], verbose_name='image'),
        ),
        migrations.AlterField(
            model_name='topper',
            name='image',
            field=cloudinary.models.CloudinaryField(blank=True, max_length=255, null=True, validators=[app.validators.cloudinary_size], verbose_name='image'),
        ),
        migrations.AlterField(
            model_name='tray',
            name='image',
            field=cloudinary.models.CloudinaryField(blank=True, max_length=255, null=True, validators=[app.validators.cloudinary_size], verbose_name='image'),
        ),
    ]
