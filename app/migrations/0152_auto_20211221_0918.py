# Generated by Django 2.2.24 on 2021-12-21 06:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0151_auto_20211221_0857'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='hidden',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='item',
            name='not_available',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='item',
            name='sold_out',
            field=models.BooleanField(default=False),
        ),
    ]
