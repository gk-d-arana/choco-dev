# Generated by Django 2.2.24 on 2022-02-21 09:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0210_dateoff'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dateoff',
            name='from_date',
            field=models.DateField(),
        ),
        migrations.AlterField(
            model_name='dateoff',
            name='to_date',
            field=models.DateField(),
        ),
    ]
