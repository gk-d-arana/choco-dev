# Generated by Django 2.2.24 on 2021-12-23 06:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0161_auto_20211223_0902'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='choices',
            field=models.ManyToManyField(blank=True, related_name='item', to='app.Choice'),
        ),
    ]
