# Generated by Django 2.2.24 on 2022-11-07 11:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0263_auto_20221101_1726'),
    ]

    operations = [
        migrations.AlterField(
            model_name='boxitem',
            name='type',
            field=models.CharField(choices=[('Bars', 'Bars'), ('Pralines', 'Pralines'), ('Dragers', 'Dragers'), ('Belgium Truffles', 'Belgium Truffles')], default='Bars', max_length=100),
        ),
        migrations.AlterField(
            model_name='item',
            name='chocolate_type',
            field=models.CharField(blank=True, choices=[('Bars', 'Bars'), ('Pralines', 'Pralines'), ('Dragers', 'Dragers'), ('Belgium Truffles', 'Belgium Truffles')], max_length=100),
        ),
        migrations.AlterField(
            model_name='orderitem',
            name='chocolate_type',
            field=models.CharField(blank=True, choices=[('Bars', 'Bars'), ('Pralines', 'Pralines'), ('Dragers', 'Dragers'), ('Belgium Truffles', 'Belgium Truffles')], default='bars', max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='tray',
            name='type',
            field=models.CharField(choices=[('Bars', 'Bars'), ('Pralines', 'Pralines'), ('Dragers', 'Dragers'), ('Bars and Pralines', 'Bars and Pralines'), ('Bars and Dragers', 'Bars and Dragers'), ('Belgium Truffles', 'Belgium Truffles')], default='Bars', max_length=100),
        ),
    ]
