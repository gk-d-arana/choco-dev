# Generated by Django 3.2.3 on 2021-06-19 17:15

import cloudinary.models
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0025_auto_20210619_1849'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='picture',
            field=cloudinary.models.CloudinaryField(blank=True, max_length=255, null=True, verbose_name='picture'),
        ),
    ]
