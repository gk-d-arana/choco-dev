# Generated by Django 2.2.24 on 2021-12-23 22:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0164_auto_20211224_0122'),
    ]

    operations = [
        migrations.AlterField(
            model_name='boxitem',
            name='type',
            field=models.CharField(choices=[('Bars', 'Bars'), ('Pralines', 'Pralines'), ('Dragers', 'Dragers')], default='Bars', max_length=100),
        ),
        migrations.AlterField(
            model_name='item',
            name='chocolate_type',
            field=models.CharField(blank=True, choices=[('Bars', 'Bars'), ('Pralines', 'Pralines'), ('Dragers', 'Dragers')], max_length=100),
        ),
        migrations.AlterField(
            model_name='orderitem',
            name='chocolate_type',
            field=models.CharField(blank=True, choices=[('Bars', 'Bars'), ('Pralines', 'Pralines'), ('Dragers', 'Dragers')], default='bars', max_length=100, null=True),
        ),
    ]
