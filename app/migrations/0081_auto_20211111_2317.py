# Generated by Django 2.2.24 on 2021-11-11 20:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0080_auto_20211111_2311'),
    ]

    operations = [
        migrations.RenameField(
            model_name='workingday',
            old_name='from_time',
            new_name='from_time1',
        ),
        migrations.RenameField(
            model_name='workingday',
            old_name='to_time',
            new_name='to_time1',
        ),
        migrations.AddField(
            model_name='workingday',
            name='disabled_time1',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='workingday',
            name='disabled_time2',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='workingday',
            name='disabled_time3',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='workingday',
            name='from_time2',
            field=models.TimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='workingday',
            name='from_time3',
            field=models.TimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='workingday',
            name='to_time2',
            field=models.TimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='workingday',
            name='to_time3',
            field=models.TimeField(blank=True, null=True),
        ),
    ]
