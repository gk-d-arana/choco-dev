# Generated by Django 2.2.24 on 2021-12-12 17:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0122_auto_20211211_1709'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='ar_title',
            field=models.CharField(blank=True, max_length=100, null=True, unique=True),
        ),
        migrations.AlterField(
            model_name='item',
            name='title',
            field=models.CharField(max_length=100, unique=True),
        ),
    ]
