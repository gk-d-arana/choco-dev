# Generated by Django 2.2.24 on 2021-12-21 07:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0152_auto_20211221_0918'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='category',
            name='coming_soon',
        ),
        migrations.AddField(
            model_name='subcategory',
            name='coming_soon',
            field=models.BooleanField(default=False),
        ),
    ]
