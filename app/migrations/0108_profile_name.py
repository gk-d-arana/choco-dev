# Generated by Django 2.2.24 on 2021-11-27 15:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0107_auto_20211123_1743'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='name',
            field=models.CharField(default='yazed', max_length=100),
            preserve_default=False,
        ),
    ]
