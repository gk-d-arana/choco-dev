# Generated by Django 2.2.24 on 2022-04-30 09:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0245_auto_20220430_0357'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='track_id',
            field=models.CharField(max_length=30),
        ),
    ]
