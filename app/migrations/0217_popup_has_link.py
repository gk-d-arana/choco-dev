# Generated by Django 2.2.24 on 2022-02-24 07:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0216_popup_ar_text'),
    ]

    operations = [
        migrations.AddField(
            model_name='popup',
            name='has_link',
            field=models.BooleanField(default=True),
        ),
    ]
