# Generated by Django 4.0.6 on 2022-07-27 06:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0256_alter_aboutuscontent_id_alter_aboutusmainimage_id_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='note',
            field=models.TextField(blank=True, null=True),
        ),
    ]
