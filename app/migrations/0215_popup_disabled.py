# Generated by Django 2.2.24 on 2022-02-21 17:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0214_auto_20220221_1831'),
    ]

    operations = [
        migrations.AddField(
            model_name='popup',
            name='disabled',
            field=models.BooleanField(default=False),
        ),
    ]
