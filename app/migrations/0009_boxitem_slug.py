# Generated by Django 3.2.3 on 2021-05-24 11:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0008_auto_20210524_0102'),
    ]

    operations = [
        migrations.AddField(
            model_name='boxitem',
            name='slug',
            field=models.SlugField(blank=True),
        ),
    ]
