# Generated by Django 2.2.24 on 2021-12-20 19:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0147_auto_20211220_1728'),
    ]

    operations = [
        migrations.AddField(
            model_name='tray',
            name='ar_unit',
            field=models.CharField(choices=[('كجم', 'كجم'), ('جرام', 'جرام')], default='كجم', max_length=50),
        ),
    ]
