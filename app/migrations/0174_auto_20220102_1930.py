# Generated by Django 2.2.24 on 2022-01-02 16:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0173_auto_20220102_1828'),
    ]

    operations = [
        migrations.AlterField(
            model_name='package',
            name='ar_unit',
            field=models.CharField(blank=True, choices=[('كجم', 'كجم'), ('جرام', 'جرام')], max_length=60, null=True),
        ),
        migrations.AlterField(
            model_name='package',
            name='en_unit',
            field=models.CharField(blank=True, choices=[('Kilogram', 'Kilogram'), ('Gram', 'Gram')], max_length=60, null=True),
        ),
    ]
