# Generated by Django 2.2.24 on 2021-11-22 19:42

import cloudinary.models
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0104_auto_20211122_1156'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tray',
            name='image',
            field=cloudinary.models.CloudinaryField(max_length=255, verbose_name='image'),
        ),
    ]
