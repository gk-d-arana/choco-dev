from django.contrib import admin
from .models import (Card, Choice, DeliveryDate, Discount, Flower, MinimumOrder, PickupPlace, Profile, Item, Order, OrderItem, BoxItem, OrderBoxItem, BoxImage, Category, Ribbon, Topper, Tray,
                    WorkingDay, AboutUsContent, AboutUsMainImage, AboutUsTopic,
                    Box, ChocolateType, SubCategory, Transaction)
from django.urls import reverse
from django.utils.http import urlencode
from django.utils.html import format_html


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ("en_title", "ar_title", "items")

    def items(self, obj):
       
        return format_html(f'<a class="addlink" href="/admin/add-category-item/{str(obj.id)}">Add Item</a>')

    items.short_description = ""

class ItemAdmin(admin.ModelAdmin):
    readonly_fields = ('id', )



class ChoiceAdmin(admin.ModelAdmin):
    list_display = ("en_choice", "ar_choice")





admin.site.register(Item, ItemAdmin)
admin.site.register(OrderItem)
admin.site.register(Order)
admin.site.register(Profile)
admin.site.register(BoxItem)
admin.site.register(OrderBoxItem)
admin.site.register(BoxImage)
admin.site.register(WorkingDay)
admin.site.register(AboutUsTopic)
admin.site.register(AboutUsContent)
admin.site.register(AboutUsMainImage)
admin.site.register(Box)
admin.site.register(ChocolateType)
admin.site.register(SubCategory)
admin.site.register(Transaction)
admin.site.register(PickupPlace)
admin.site.register(MinimumOrder)
admin.site.register(Tray)
admin.site.register(Topper)
admin.site.register(Flower)
# admin.site.register(Coupon)
admin.site.register(Discount)

admin.site.register(Card)
admin.site.register(Choice, ChoiceAdmin)
admin.site.register(DeliveryDate)














