from email.policy import default
from django.core.mail import mail_admins
from django.db import models
from django.contrib.auth.models import User
from django.db.models.deletion import SET_NULL
from django.db.models.expressions import Case
from django.db.models.fields import BLANK_CHOICE_DASH, IntegerField
from django.db.models.fields import related
from django.db.models.fields.related import ForeignKey
from django.utils import timezone
from cloudinary.models import CloudinaryField
from django.urls import reverse
from django.utils.text import slugify
import datetime
from django.dispatch import receiver
import cloudinary
import cloudinary.uploader
import decimal


from . import validators




CHOCOLATE_TYPE = (
    ("Bars", "Bars"),
    ("Pralines", "Pralines"),
    ("Dragers", "Dragers"),
    ("Belgium Truffles", "Belgium Truffles"),

)


TRAY_CHOCOLATE_TYPE = (
    ("Bars", "Bars"),
    ("Pralines", "Pralines"),
    ("Dragers", "Dragers"),
    ("Bars and Pralines", "Bars and Pralines"),
    ("Bars and Dragers", "Bars and Dragers"),
    ("Belgium Truffles", "Belgium Truffles")
)

QUANTITY_CHOICE = (
    ("Kilogram", "Kilogram"),
    ("Half Kilo", "Half Kilo"),
    ("Quarter Kilo", "Quarter Kilo")
)

UNIT_CHOICE = (
    ("Kilogram", "Kilogram"),
    ("Gram", "Gram"),
)


AR_UNIT_CHOICE = (
    ("كجم", "كجم"),
    ("جرام", "جرام"),
)


BOX_IMAGE_CHOICE = (
    ("box_1.png", "box_1.png"),
    ("box_2.png", "box_2.png"),
    ("box_3.png", "box_3.png"),
)



ORDER_STATUS = (
    ("Received", "Received"),
    ("Processing", "Processing"),
    ("On Delivery", "On Delivery"),
    ("Delivered", "Delivered"),

)



EN_DELIVERY_DATES = (
    ("Same Day", "0"),
    ("1 Day", "1 Day"),
    ("2 Days", "2 Days"),
    ("3 Day", "3 Day"),

)



AR_DELIVERY_DATES = (
    ("نفس اليوم", "نفس اليوم"),
    ("بعد يوم", "بعد يوم"),
    ("بعد يومين", "بعد يومين"),
    ("بعد ٣ ايام", "بعد ٣ ايام"),

)

QUANTITY_TYPES = (
    ("Weight", "Weight"),
    ("Pieces", "Pieces"),

)

User._meta.get_field("email").blank = False
User._meta.get_field("email").null = False






class Flavor(models.Model):
    en_name = models.CharField(max_length=100)
    ar_name = models.CharField(max_length=100)
    index = models.IntegerField(default=1)



    def __str__(self):
        return self.en_name


class Color(models.Model):
    en_name = models.CharField(max_length=100)
    ar_name = models.CharField(max_length=100)
    index = models.IntegerField(default=1)

    def __str__(self):
        return self.en_name




class Discount(models.Model):
    
    value = models.IntegerField()
    start_date = models.DateField()
    end_date = models.DateField()




class DateOff(models.Model):
    from_date = models.DateField()
    to_date = models.DateField()



    def __str__(self):
        return f'{self.from_date.strftime("%d-%m-%Y")} - {self.to_date.strftime("%d-%m-%Y")}'

    def get_absolute_url(self):
        return reverse("dashboard:set-date-off")

class Visit(models.Model):
    date = models.DateTimeField(auto_now_add=True)

class SubscribedUser(models.Model):
    email = models.EmailField()

    def __str__(self):
        return self.email

class DeliveryDate(models.Model):
    en_date = models.CharField(max_length=50)
    ar_date = models.CharField(max_length=50)
    number = models.IntegerField(default=0)

    def __str__(self):
        return self.en_date


class Choice(models.Model):
    en_choice = models.CharField(max_length=200, unique=True)
    ar_choice = models.CharField(max_length=200, unique=True)

    
    def __str__(self):
        return self.en_choice

    def get_absolute_url(self):
        return reverse("dashboard:items")



class DeliveryCharge(models.Model):
    price = models.IntegerField()


class ContactUs(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField()
    message = models.TextField()
    date = models.DateTimeField(auto_now_add=True)


class BannerImage(models.Model):
    image1 = CloudinaryField("image1", blank=True, null=True)
    image2 = CloudinaryField("image2", blank=True, null=True)
    image3 = CloudinaryField("image3", blank=True, null=True)
    image4 = CloudinaryField("image4", blank=True, null=True)
    image5 = CloudinaryField("image5", blank=True, null=True)
    picture1 = CloudinaryField("picture1", blank=True, null=True)
    picture2 = CloudinaryField("picture2", blank=True, null=True)
    picture3 = CloudinaryField("picture3", blank=True, null=True)
    picture4 = CloudinaryField("picture4", blank=True, null=True)
    picture5 = CloudinaryField("picture5", blank=True, null=True)

class ItemChoice(models.Model):
    choice = models.CharField(max_length=500)



class Card(models.Model):
    en_name = models.CharField(max_length=100)
    ar_name = models.CharField(max_length=100, blank=True, null=True)
    image = CloudinaryField("image", blank=True, null=True)
    picture = CloudinaryField("picture", blank=True, null=True)
    
    price = models.DecimalField(max_digits=6, decimal_places=3)
    slug = models.SlugField(blank=True, null=True)


    def __str__(self):
        return self.en_name


    def save(self, *args, **kwargs):
        self.slug = slugify(self.en_name)
        super(Card, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("dashboard:cards")

class Ribbon(models.Model):
    en_name = models.CharField(max_length=100)
    ar_name = models.CharField(max_length=100, blank=True, null=True)
    image = CloudinaryField("image", blank=True, null=True)
    picture = CloudinaryField("picture", blank=True, null=True)

    price = models.DecimalField(max_digits=6, decimal_places=3)
    slug = models.SlugField(blank=True, null=True)


    def __str__(self):
        return self.en_name


    def save(self, *args, **kwargs):
        self.slug = slugify(self.en_name)
        super(Ribbon, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("dashboard:ribbons")


class Flower(models.Model):
    en_name = models.CharField(max_length=100)
    ar_name = models.CharField(max_length=100, blank=True, null=True)
    image = CloudinaryField("image", blank=True, null=True)
    picture = CloudinaryField("picture", blank=True, null=True)

    price = models.DecimalField(max_digits=6, decimal_places=3)
    slug = models.SlugField(blank=True, null=True)

    def __str__(self):
        return self.en_name


    def save(self, *args, **kwargs):
        self.slug = slugify(self.en_name)
        super(Flower, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("dashboard:flowers")


class Topper(models.Model):
    en_name = models.CharField(max_length=100)
    ar_name = models.CharField(max_length=100, blank=True, null=True)
    image = CloudinaryField("image", blank=True, null=True)
    picture = CloudinaryField("picture", blank=True, null=True)

    price = models.DecimalField(max_digits=6, decimal_places=3)
    slug = models.SlugField(blank=True, null=True)


    def save(self, *args, **kwargs):
        self.slug = slugify(self.en_name)
        super(Topper, self).save(*args, **kwargs)

    def __str__(self):
        return self.en_name

    def get_absolute_url(self):
        return reverse("dashboard:toppers")



class Tray(models.Model):
    en_name = models.CharField(max_length=100)
    ar_name = models.CharField(max_length=100, blank=True, null=True)
    slug = models.SlugField(blank=True, null=True)
    image = CloudinaryField("image", blank=True, null=True)
    picture = CloudinaryField("picture", blank=True, null=True)

    height = models.DecimalField(max_digits=4, decimal_places=1, null=True, blank=True)
    width = models.DecimalField(max_digits=4, decimal_places=1, null=True, blank=True)
    length = models.DecimalField(max_digits=4, decimal_places=1, null=True, blank=True)
    diameter = models.DecimalField(max_digits=4, decimal_places=1, null=True, blank=True)
    price = models.DecimalField(max_digits=6, decimal_places=3)
    weight = models.DecimalField(max_digits=6, decimal_places=3)
    unit = models.CharField(choices=UNIT_CHOICE, default="Kilogram", max_length=50)
    ar_unit = models.CharField(choices=AR_UNIT_CHOICE, default="كجم", max_length=50)
    items = models.IntegerField(blank=True, null=True)
    type = models.CharField(choices=TRAY_CHOCOLATE_TYPE, max_length=100, default="Bars")
    delivery_date = models.ForeignKey(DeliveryDate, on_delete=models.SET_NULL, null=True, blank=True, default=1)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.en_name)
        super(Tray, self).save(*args, **kwargs)

    def __str__(self):
        return self.en_name

    def get_absolute_url(self):
        return reverse("dashboard:trays")





class MinimumOrder(models.Model):
    price = models.DecimalField(max_digits=6, decimal_places=3)

class Box(models.Model):
    name = models.CharField(max_length=100, default="box")
    image = CloudinaryField("image")
    picture = CloudinaryField("picture", blank=True, null=True)


    def __str__(self):
        return self.name

class ChocolateType(models.Model):
    title = models.CharField(max_length=100)
    image = CloudinaryField("image")
    picture = CloudinaryField("picture", blank=True, null=True)

    checked = models.BooleanField(default=False)

    def __str__(self):
        return self.title







class AboutUsMainImage(models.Model):
    image = models.ImageField(upload_to="images/about-us", validators=[validators.image_size])




class AboutUsContent(models.Model):
    main_text = models.TextField()
    ar_main_text = models.TextField(null=True, blank=True)

    ar_secondary_text = models.TextField(null=True, blank=True)
    secondary_text = models.TextField()


    image = CloudinaryField("image", null=True)


class AboutUsTopic(models.Model):
    title = models.CharField(max_length=100)
    ar_title = models.CharField(max_length=100, null=True, blank=True)

    paragraph = models.TextField()
    ar_paragraph = models.TextField(null=True, blank=True)

    

    pattern_above = models.BooleanField(default=False)


    number = models.IntegerField(default=2)

    def __str__(self):
        return self.title



class Occasion(models.Model):
    title = models.CharField(max_length=100)


    def __str__(self):
        return self.title




class WorkingDay(models.Model):
    day = models.CharField(default="Sunday", max_length=60)
    from_time1 = models.TimeField(blank=True, null=True)
    to_time1 = models.TimeField(blank=True, null=True)
    disabled_time1 = models.BooleanField(default=False)

    from_time2 = models.TimeField(blank=True, null=True)
    to_time2 = models.TimeField(blank=True, null=True)
    disabled_time2 = models.BooleanField(default=False)

    from_time3 = models.TimeField(blank=True, null=True)
    to_time3 = models.TimeField(blank=True, null=True)
    disabled_time3 = models.BooleanField(default=False)

    day_off = models.BooleanField(default=False)

    def __str__(self):
        return self.day


    def get_absolute_url(self):
        return reverse("dashboard:days-list")



class SubCategory(models.Model):
    en_title = models.CharField(max_length=100)
    ar_title = models.CharField(max_length=100, blank=True, null=True)
    number = models.IntegerField()
    coming_soon = models.BooleanField(default=False)
    singular = models.CharField(max_length=50, null=True, blank=True)
    ar_singular = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return self.en_title

    def get_absolute_url(self):
        return reverse("dashboard:items")


    def get_items(self):
        return self.items.filter(is_deleted=False)

    class Meta:
        verbose_name_plural = "Sub Categories"



class Category(models.Model):
    en_title = models.CharField(max_length=100)
    image = CloudinaryField("image", blank=True, null=True)
    picture = CloudinaryField("picture", blank=True, null=True)

    banner_image = CloudinaryField("image", blank=True, null=True)
    banner_picture = CloudinaryField("banner_picture", blank=True, null=True)


    ar_title = models.CharField(max_length=100, blank=True, null=True)
    number = models.IntegerField(default=1)
    subs = models.ManyToManyField(SubCategory, related_name="categories", blank=True)
    has_dimensions = models.BooleanField(default=False)

    singular = models.CharField(max_length=50, null=True, blank=True)
    ar_singular = models.CharField(max_length=50, null=True, blank=True)


    hidden = models.BooleanField(default=False)

    def __str__(self):
        return self.en_title

    def get_absolute_url(self):
        return reverse("dashboard:items")


    def get_items(self):
        return self.items.filter(is_deleted=False)
    

    class Meta:
        verbose_name_plural = "Categories"




    


class BoxItem(models.Model):
    title = models.CharField(max_length=100)
    ar_title = models.CharField(max_length=100, null=True, blank=True)
    image = CloudinaryField("images", null=True, blank=True)
    picture = CloudinaryField("picture", blank=True, null=True)

    quantity = models.DecimalField(max_digits=6, decimal_places=3, default=1)
    slug = models.SlugField(blank=True, max_length=200)
    price = models.DecimalField(max_digits=6, decimal_places=3, default=10)
    type = models.CharField(choices=CHOCOLATE_TYPE, max_length=100, default="Bars")
    

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(BoxItem, self).save(*args, **kwargs)

    def __str__(self):
        return self.title


    def get_absolute_url(self):
        return reverse("dashboard:box-items")

class OrderBoxItem(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    item = ForeignKey(BoxItem, on_delete=models.CASCADE)
    quantity = models.DecimalField(max_digits=6, decimal_places=3, default=1)  
    ordered = models.BooleanField(default=False)
    price = models.DecimalField(max_digits=6, decimal_places=3, default=10)


    def __str__(self):
        return str(self.quantity) + " of " + self.item.title


class Package(models.Model):
    en_name = models.CharField(max_length=200)
    ar_name = models.CharField(max_length=200)

    subcategory = models.ForeignKey(SubCategory, on_delete=models.SET_NULL, null=True)

    price = models.DecimalField(max_digits=6, decimal_places=3)
    weight = models.DecimalField(max_digits=6, decimal_places=3, null=True, blank=True)
    pieces = models.IntegerField(null=True, blank=True)


    en_unit = models.CharField(choices=UNIT_CHOICE, max_length=60, null=True, blank=True)
    ar_unit = models.CharField(choices=AR_UNIT_CHOICE, max_length=60, null=True, blank=True)

    image = CloudinaryField("image", null=True, blank=True)
    picture = CloudinaryField("picture", null=True, blank=True)



    def get_absolute_url(self):
        return reverse("dashboard:packages")




class Item(models.Model):
    title = models.CharField(max_length=100)
    ar_title = models.CharField(max_length=100, null=True, blank=True)


    description = models.TextField(blank=True)
    ar_description = models.TextField(blank=True)
    price = models.DecimalField(max_digits=6, decimal_places=3)
    
    picture = CloudinaryField("picture", null=True, blank=True)
    picture2 = CloudinaryField("picture2", null=True, blank=True)
    picture3 = CloudinaryField("picture3", null=True, blank=True)

    category = models.ForeignKey(Category, on_delete=models.SET_NULL, blank=True, null=True, related_name="items")
    sub_category = models.ForeignKey(SubCategory, on_delete=models.SET_NULL, blank=True, null=True, related_name="items")

    slug = models.SlugField(max_length=100)
    quantity_type = models.CharField(choices=QUANTITY_TYPES, max_length=30, default="Pieces")
    weight_quantity = models.DecimalField(decimal_places=3, max_digits=6, default=0.25)
    pieces_quantity = models.IntegerField(default=1)

    customized_box = models.BooleanField(default=False)
    chocolate_type = models.CharField(choices=CHOCOLATE_TYPE, max_length=100, blank=True)
    quantity_choice = models.CharField(choices=QUANTITY_CHOICE, max_length=100, blank=True)
    

    occasion = models.ForeignKey(Occasion, on_delete=models.SET_NULL, null=True, blank=True)

    has_choices = models.BooleanField(default=False)
    
    choice1 = models.CharField(max_length=300, null=True, blank=True)
    ar_choice1 = models.CharField(max_length=300, null=True, blank=True)

    choice2 = models.CharField(max_length=300, null=True, blank=True)
    ar_choice2 = models.CharField(max_length=300, null=True, blank=True)

    choice3 = models.CharField(max_length=300, null=True, blank=True)
    ar_choice3 = models.CharField(max_length=300, null=True, blank=True)

    choice4 = models.CharField(max_length=300, null=True, blank=True)
    ar_choice4 = models.CharField(max_length=300, null=True, blank=True)

    choice5 = models.CharField(max_length=300, null=True, blank=True)
    ar_choice5 = models.CharField(max_length=300, null=True, blank=True)

    choice6 = models.CharField(max_length=300, null=True, blank=True)
    ar_choice6 = models.CharField(max_length=300, null=True, blank=True)


    choices = models.ManyToManyField(Choice, related_name="item", blank=True)

    has_flavors  = models.BooleanField(default=False)
    flavors = models.ManyToManyField(Flavor, related_name="item", blank=True)
    has_colors = models.BooleanField(default=False)
    colors = models.ManyToManyField(Color, related_name="item", blank=True)


    height = models.IntegerField(blank=True, null=True)
    width = models.IntegerField(blank=True, null=True)
    length = models.IntegerField(blank=True, null=True)
    diameter = models.IntegerField(blank=True, null=True)



    index = models.IntegerField(default=1, null=True, blank=True)




    hidden = models.BooleanField(default=False)
    sold_out = models.BooleanField(default=False)
    not_available = models.BooleanField(default=False)
    coming_soon = models.BooleanField(default=False)


    weight_maximum = models.DecimalField(decimal_places=3, max_digits=6, null=True, blank=True)
    pieces_maximum = models.IntegerField(null=True, blank=True)


    weight_minimum = models.DecimalField(decimal_places=3, max_digits=6, default=0.250, null=True, blank=True)
    pieces_minimum = models.IntegerField(default=1, null=True, blank=True)

    delivery_date = models.ForeignKey(DeliveryDate, on_delete=models.SET_NULL, null=True, blank=True, default=1)
    


    is_deleted = models.BooleanField(default=False)


    def get_min_price(self):

        if self.quantity_type == "Weight":
            min = self.weight_minimum
            price = min / decimal.Decimal(0.250) * self.price
        else:
            min = self.pieces_minimum
            price = min * self.price
        
        return round(price, 3)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Item, self).save(*args, **kwargs)
    

    def __str__(self):
        return self.title


    def get_absolute_url(self):
        return reverse("dashboard:items")


class Address(models.Model):
    title = models.CharField(max_length=100)
    area = models.CharField(max_length=100)
    block = models.CharField(max_length=4)
    street = models.CharField(max_length=60)
    house = models.CharField(max_length=10)
    floor = models.CharField(max_length=10, null=True, blank=True)
    apartment = models.CharField(max_length=10, null=True, blank=True)




class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=8)
    area = models.CharField(max_length=100)
    block = models.CharField(max_length=50)
    street = models.CharField(max_length=60)
    house = models.CharField(max_length=10)
    floor = models.CharField(max_length=10, null=True, blank=True)
    apartment = models.CharField(max_length=10, null=True, blank=True)
    address2 = models.CharField(max_length=100)
    pin = models.CharField(max_length=10, blank=True, null=True)
    addresses = models.ManyToManyField(Address, related_name="profile")
    favorites = models.ManyToManyField(Item, related_name="profile")
    has_seen_popup = models.BooleanField(default=False)



class OrderItem(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    quantity_type = models.CharField(choices=QUANTITY_TYPES, max_length=30, default="Pieces")
    weight_quantity = models.DecimalField(decimal_places=3, max_digits=6, default=0.25)
    pieces_quantity = models.IntegerField(default=1)
    ordered = models.BooleanField(default=False)
    ordered_date = models.DateTimeField(null=True, blank=True)
    chocolate_type = models.CharField(choices=CHOCOLATE_TYPE, max_length=100, default="bars", blank=True, null=True)
    quantity_choice = models.CharField(choices=QUANTITY_CHOICE, max_length=100, default="kilogram", blank=True, null=True)
    box_items = models.ManyToManyField(OrderBoxItem, blank=True)
    price = models.DecimalField(blank=True, null=True, max_digits=6, decimal_places=3)
    box_choice = models.CharField(max_length=100, blank=True, null=True)
    last = models.BooleanField(default=False)
    slug = models.SlugField(null=True, blank=True)


    package = models.ForeignKey(Package, on_delete=SET_NULL, null=True)


    tray = models.ForeignKey(Tray, on_delete=models.SET_NULL, blank=True, null=True)
    topper = models.ForeignKey(Topper, on_delete=models.SET_NULL, blank=True, null=True)
    flower = models.ForeignKey(Flower, on_delete=models.SET_NULL, blank=True, null=True)
    ribbon = models.ForeignKey(Ribbon, on_delete=models.SET_NULL, blank=True, null=True)
    card = models.ForeignKey(Card, on_delete=models.SET_NULL, blank=True, null=True)
    has_choice = models.BooleanField(default=False)
    choice = models.CharField(max_length=300, null=True, blank=True)


    flavor = models.ForeignKey(Flavor, null=True, blank=True, on_delete=models.SET_NULL)
    color = models.ForeignKey(Color, null=True, blank=True, on_delete=models.SET_NULL)

    def get_item_total(self):
        return self.price * self.pieces_quantity


class Coupon(models.Model):
    value = models.IntegerField()
    code = models.CharField(max_length=50)
    end_date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.code

    def get_absolute_url(self):
        return reverse("dashboard:coupons-list")



class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    items = models.ManyToManyField(OrderItem)
    ordered_date = models.DateTimeField(blank=True, null=True)
    order_day = models.CharField(max_length=100, blank=True, null=True)
    order_time = models.CharField(max_length=100, blank=True, null=True)
    order_type = models.CharField(max_length=100, blank=True, null=True)
    email = models.EmailField()
    phone_number = models.CharField(max_length=8)
    address_line = models.CharField(max_length=200, blank=True)
    ordered = models.BooleanField(default=False)
    status = models.CharField(choices=ORDER_STATUS, max_length=30, default="Received")
    area = models.CharField(max_length=100, blank=True, null=True)
    block = models.CharField(max_length=4, blank=True, null=True)
    street = models.CharField(max_length=60, blank=True, null=True)
    house = models.CharField(max_length=10, blank=True, null=True)
    canceled = models.BooleanField(default=False)
    gift = models.BooleanField(default=False, null=True)
    empty_card = models.BooleanField(default=False, null=True)
    address = models.ForeignKey(Address, on_delete=models.SET_NULL, null=True)
    phone_number = models.CharField(max_length=20, null=True, blank=True)
    user_name = models.CharField(max_length=100, null=True, blank=True)

    delivery_date = models.CharField(max_length=250, null=True)


    coupon = models.ForeignKey(Coupon, on_delete=models.SET_NULL, null=True)
    discount = models.IntegerField(default=0)


    note = models.TextField(null=True, blank=True, max_length=250)
    

    def __str__(self):
        return str(self.get_order_total())


    def get_order_sub(self):
        total = 0
        for item in self.items.all():
            total += item.get_item_total()
        return round(total, 3)


    def get_discount_sub(self):
        total = 0
        for item in self.items.all():
            total += item.get_item_total()
        if Discount.objects.filter(start_date__lte=datetime.datetime.now().date(), end_date__gte=datetime.datetime.now().date()).exists():
            discount = Discount.objects.get(start_date__lte=datetime.datetime.now().date(), end_date__gte=datetime.datetime.now().date())
            discount_value = discount.value / 100
            total -= total * decimal.Decimal(discount_value)
        elif self.discount != 0:
            discount_value = self.discount / 100
            total -= total * decimal.Decimal(discount_value)

        
        return round(total, 3)

    

    def get_order_total(self):
        total = 0
        for item in self.items.all():
            total += item.get_item_total()

        if self.order_type == "Delivery":
            total += DeliveryCharge.objects.first().price
        
        return round(total, 3)


    def get_discount_total(self):

        total = 0
        for item in self.items.all():
            total += item.get_item_total()

        if Discount.objects.filter(start_date__lte=datetime.datetime.now().date(), end_date__gte=datetime.datetime.now().date()).exists():
            discount = Discount.objects.get(start_date__lte=datetime.datetime.now().date(), end_date__gte=datetime.datetime.now().date())
            discount_value = discount.value / 100
            total -= total * decimal.Decimal(discount_value)
        elif self.discount != 0:
            discount_value = self.discount / 100
            total -= total * decimal.Decimal(discount_value)


        if self.order_type == "Delivery":
            total += DeliveryCharge.objects.first().price
        
        
        
        return round(total, 3)


    def get_discount(self):
        discount_value = 0
        if Discount.objects.filter(start_date__lte=datetime.datetime.now().date(), end_date__gte=datetime.datetime.now().date()).exists():
            discount = Discount.objects.get(start_date__lte=datetime.datetime.now().date(), end_date__gte=datetime.datetime.now().date())
            discount_value = discount.value / 100

        elif self.discount != 0:
            discount_value = self.discount / 100
        
        return discount_value

    def get_invoice_total(self):

        total = 0
        for item in self.items.all():
            total += item.get_item_total()

        
        if self.discount != 0:
            discount_value = self.discount / 100
            total -= total * decimal.Decimal(discount_value)


        if self.order_type == "Delivery":
            total += DeliveryCharge.objects.first().price
        
        
        
        return round(total, 3)


    def get_items_number(self):
        number = 0
        for item in self.items.all():
            number += 1

        return number
    def get_products(self):
        products = 0
        for order_item in self.items.all():
            products += 1
        return products




class BoxImage(models.Model):
    image = models.ImageField(upload_to="boxes_images", validators=[validators.image_size])





class Transaction(models.Model):
    user_name = models.CharField(max_length=200, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    track_id = models.CharField(max_length=30)
    amount = models.FloatField(default=7.00)
    done = models.BooleanField(default=False)
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name="transaction", null=True, blank=True)
    date = models.DateTimeField(blank=True, null=True)


class PickupPlace(models.Model):
    name = models.CharField(max_length=100)
    ar_name = models.CharField(max_length=100, default="السالمية")

    def __str__(self):
        return self.name




class PopUp(models.Model):
    image = CloudinaryField("pop_up_image", blank=True, null=True)
    picture = CloudinaryField("picture", null=True, blank=True)

    link = models.URLField(blank=True, null=True)
    ar_link = models.URLField(blank=True, null=True)

    text = models.CharField(max_length=100, default="View Offer", null=True, blank=True)
    ar_text = models.CharField(max_length=100, default="قراءة المزيد", null=True, blank=True)
    has_link = models.BooleanField(default=True)
    disabled = models.BooleanField(default=False)



    def save(self, *args, **kwargs):
        Profile.objects.all().update(has_seen_popup=False)
        super(PopUp, self).save(*args, **kwargs)










@receiver(models.signals.post_delete, sender=Item.picture)
def remove_file_from_s3(sender, instance, using, **kwargs):
    instance.img.delete(save=False)






@receiver(models.signals.post_delete, sender=Tray)
def remove_tray_image(sender, instance, using, **kwargs):
    cloudinary.uploader.destroy( instance.image.public_id,invalidate=True)


@receiver(models.signals.post_delete, sender=Ribbon)
def remove_ribbon_image(sender, instance, using, **kwargs):
    cloudinary.uploader.destroy( instance.image.public_id,invalidate=True)

@receiver(models.signals.post_delete, sender=Flower)
def remove_flower_image(sender, instance, using, **kwargs):
    cloudinary.uploader.destroy( instance.image.public_id,invalidate=True)


@receiver(models.signals.post_delete, sender=Topper)
def remove_flower_image(sender, instance, using, **kwargs):
    cloudinary.uploader.destroy( instance.image.public_id,invalidate=True)

@receiver(models.signals.post_delete, sender=Card)
def remove_flower_image(sender, instance, using, **kwargs):
    cloudinary.uploader.destroy( instance.image.public_id,invalidate=True)




