from pathlib import Path
import os
from urllib.parse import urlparse
import cloudinary
import cloudinary.api
import cloudinary.uploader
import dj_database_url
import redis
from django.contrib.messages import constants as messages


    

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent
TEMPLATE_DIR = os.path.join(BASE_DIR, 'templates')
SECURE_REDIRECTS_SSL = False

MESSAGE_TAGS = {
    messages.DEBUG: 'alert-secondary',
    messages.INFO: 'alert-info',
    messages.SUCCESS: 'alert-success',
    messages.WARNING: 'alert-warning',
    messages.ERROR: 'alert-danger',
 }



# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'django-insecure-b7-!bzye44(j5un33l$&++_4as0ryww^@q%c#b2w_6ql6g@vhg'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['chocolateco.herokuapp.com', '127.0.0.1', "www.chocolatecokw.com", "choco-dev.herokuapp.com"]

## Staging
# DATABASE_URL = 'postgres://cnnhgsbewqqibb:b253a5aef05698a7821bd105b949e486f7f509a9bf9db5b23b17f10849ab6d5f@ec2-52-86-115-245.compute-1.amazonaws.com:5432/dfr5ce6utdag0s'
## Live
DATABASE_URL = "postgres://musdpvlhxglign:8a7024a370c47a1d68e0ea02b8e96d69d16a6de7b9f86db1cc688c02618b02d8@ec2-3-216-167-65.compute-1.amazonaws.com:5432/d5gcc9oavvqgjc"

# "postgres://zcqthtxubsikbg:b2819401d7b796452ff42101a871898c463fe75402d4220386766448d0e76cc9@ec2-52-70-186-184.compute-1.amazonaws.com:5432/damocvhf5edsmf"
# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'app',
    'dashboard',
    'rest_framework',

    'bootstrap4',
    'cloudinary',
    "channels",
    "django_cleanup.apps.CleanupConfig",
    "facebook_pixel_code",
    
    "dbbackup",
]


FACEBOOK_PIXEL_CODE_ID = "3179941832325793"


DBBACKUP_STORAGE = 'django.core.files.storage.FileSystemStorage'
DBBACKUP_STORAGE_OPTIONS = {'location': BASE_DIR / 'backups'}



cloudinary.config( 
  cloud_name = "dt3kozoud", 
  api_key = "256416339455514", 
  api_secret = "USKcRkhgqyuByupunqbYfeQLTPs" 
)

# cloudinary.config( 
#   cloud_name = "do2xnv4uq", 
#   api_key = "639169213199528", 
#   api_secret = "e_2PwhpIYPPeYrpU9EfHlZvThUY" 
# )

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.middleware.gzip.GZipMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware'
]

STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'


ROOT_URLCONF = 'chocolate.urls'
ASGI_APPLICATION = "chocolate.routing.application"
TEMPLATES = [

    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [TEMPLATE_DIR, ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'chocolate.wsgi.application'


# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

# DATABASES = {
#     'default': dj_database_url.config(default=DATABASE_URL)
# }

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'choco-dev',
        'USER': 'postgres',
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}



# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/

LANGUAGE_CODE = 'ar-KW'

TIME_ZONE = 'Asia/Kuwait'

USE_I18N = True

USE_L10N = True



# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/


LOGIN_URL = 'login'
LOGIN_REDIRECT_URL = '/'


STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static')
]
STATIC_ROOT = os.path.join(BASE_DIR, 'static_root')




MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media/images')


# CHANNEL_LAYERS = {
#     "default": {
#         "BACKEND": "channels.layers.InMemoryChannelLayer"
#    }
# }

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {
            "hosts": [("127.0.0.1", 6379)],
        },
    },
}

# CHANNEL_LAYERS = {
#     "default": {
#         "BACKEND": "channels_redis.core.RedisChannelLayer",
#         "CONFIG": {
#             "hosts": [os.environ.get("REDIS_URL", 'redis://localhost:6379')],
#         },
#     },
# }


# Default primary key field types
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'



EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'dominate.chocolateco@gmail.com'
EMAIL_HOST_PASSWORD = 'ehocwswkkahtktvt' # pass the key or password app here
EMAIL_PORT = 587
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'dominate.chocolateco@gmail.com'