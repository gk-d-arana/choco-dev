from django.urls import path, re_path 
# from django.urls import url
from channels.routing import ChannelNameRouter, ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack
from channels.security.websocket import AllowedHostsOriginValidator, OriginValidator

from dashboard import consumers

application = ProtocolTypeRouter({
    "websocket": AllowedHostsOriginValidator(
        AuthMiddlewareStack(
            URLRouter(
                [
                    re_path(r"^dashboard/orders/$", consumers.OrderConsumer.as_asgi())
                ]
            )
        )
        
        
    )
})


