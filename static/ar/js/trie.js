export default class Trie {
    constructor() {
      this.end = false;
      this.prefixes = 0;
      this.children = {
    };
    }
  
    insert(str, pos = 0) {

        if(str.length === pos){
            this.end = true;
            return;
        }

        let key = str[pos]
        if(!this.children[key]){
            this.children[key] = new Trie
        }

        this.children[key].insert(str, pos+1)
    
        //TODO: Insert function should be able to insert a word like "cat" and add it to the Trie
      }
    
  
    getAllWords(word = '', words = []) {
      // TODO: getAllWords() should get every single word in the Trie and return all those words in an array
        
      if (this.end){
          words.push(word)
      }
      for(let key in this.children){
          this.children[key].getAllWords(word+key, words)
      }
      return words;
    }
  
    remove(str, pos = 0) {
      // TODO: Remove words from the Trie
      let key = str[pos]
      if(str.length == pos){
        this.end = false;
        return;
      }
      let child = this.children[key]

      if(child){
        child.remove(str, pos+1)
      }

    
    }


    autoComplete(str, pos = 0){
      // Edge case: If str == empty then return empty
      if(str.length == 0){
        return;
      }
      // Key stores letter of str based on pos. Default is pos == 0

      let key = str[pos]

      // Child stores the children of the current character utilizing key

      let child = this.children[key]

      // If child has no children then return empty

      if(child == undefined){
        return {}
      }

      // When pos has reached the end of the string return object with prev string entered and all the words to show autoComplete (Basecase)
      
      if(pos == str.length - 1){
        return {prev: str, found: child.getAllWords(str)}
      }

      // Call function recursively when we haven't checked for all the char in the string yet.

      return child.autoComplete(str, pos+1)
    }
  }
  
  // Create new instance of Trie
    let trie = new Trie()
    trie.insert("java")
    trie.insert("javascript")
    trie.insert("python")

    console.log(trie)

    console.log(trie.getAllWords())

  

    console.log(trie.autoComplete("ja"))


  // Test Trie Methods
  






