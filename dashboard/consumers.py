from asgiref.sync import sync_to_async
import json
from django.contrib.auth import get_user_model
from channels.consumer import AsyncConsumer
from channels.generic.websocket import AsyncJsonWebsocketConsumer, WebsocketConsumer
from channels.db import database_sync_to_async
from django.core import serializers
from channels.layers import get_channel_layer
from app.models import Order, User
from asgiref.sync import async_to_sync

class OrderConsumer(AsyncConsumer):
    async def websocket_connect(self, event):
        print("connected", event)

        await self.channel_layer.group_add("name", self.channel_name)
        await self.send({
            "type": "websocket.accept"
        })

       
    
    async def websocket_receive(self, event):
        print("received", event)
        user = self.scope["user"]
        print(user)
        order = await self.get_order(user)
        obj = {
            "order_id": order.id,
            "name": user.username,
            "location": f"{ order.area }, Block: { order.block }, { order.street } St, House No. { order.house }",
            "amount": await self.get_order_total(order),
            "status": order.status,
            "block": order.block,
            "area": order.area,
            "street": order.street,
            "house": order.house,
            "new": True,
            "qty": await self.get_items_number(order),
            "number": order.phone_number,
            "ordered_date": await self.get_ordered_date(order)

            
        }

        new_event = {
            "type": "new_order",
            "text": json.dumps(obj)
        }
        await self.channel_layer.group_send(
            "name",
            new_event
        )
        print("DONE")

    async def websocket_disconnect(self, event):
        print("disonnected", event)
        self.channel_layer.group_discard(
            "name",
            self.channel_name
        )

    async def new_order(self, event):
        
        await self.send({
            "type": "websocket.send",
            "text": event["text"]
        })
    @database_sync_to_async
    def get_order(self, user):
        return  Order.objects.get(user=user, ordered=False)

    @database_sync_to_async
    def get_order_total(self, order):
        total = 0
        for item in order.items.all():
            total += item.get_item_total()
        return str(total)

    @database_sync_to_async
    def get_items_number(self, order):
        number = 0
        for item in order.items.all():
            number += 1

        return str(number)

    @database_sync_to_async
    def get_ordered_date(self, order):
        return order.ordered_date.strftime("%Y-%m-%d %H:%M %p")