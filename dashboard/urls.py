from django.urls import path
from . import views

app_name = "dashboard"

urlpatterns = [
    path("", views.index, name="index"),
    path("items/", views.items_view, name="items"),
    path("create-item/", views.CreateItem.as_view(), name="create-item"),
    path("update/<pk>/", views.UpdateItem.as_view(), name="update-item"),
    path("delete/<id>/", views.delete_item, name="delete-item"),
    path("categories/", views.categories_view, name="categories"),
    path("create-category/", views.create_category, name="create-category"),
    path("update-category/<id>", views.update_category, name="update-category"),
    path("delete-category/<id>", views.delete_category, name="delete-category"),

    path('create-subcategory-item/<id>', views.create_subcategory_item, name="create-subcategory-item"),
    path('create-category-item/<id>', views.create_category_item, name="create-category-item"),

    path("orders/", views.orders, name="orders"),
    path("change-status/<id>/<str:status>", views.change_status, name="change-status"),
    path("change-new-status/<id>/<str:status>", views.change_new_status, name="change-new-status"),
    path("cancel-order/<id>", views.cancel_order, name="cancel-order"),


    path("working-hours/<pk>", views.WorkingHours.as_view(), name="working-hours"),
    path("days-list", views.days_list, name="days-list"),
    path("order/<id>", views.order_details, name="order-details"),



    path("about-us", views.about_us, name="about-us"),
    path("about-us/topics", views.topics_list, name="about-topics"),
    path("about-us/content", views.UpdateAboutContent.as_view(), name="about-us-content"),
    path("about-us/main-image", views.UpdateMainImage.as_view(), name="about-main-image"),
    path("about-us/update-topic/<pk>", views.UpdateTopic.as_view(), name="update-topic"),

   



    path("customized-box", views.customized_box, name="customized-box"),
    path("customized-box/create-type", views.AddType.as_view(), name="add-type"),
    path("customized-box/update-type/<pk>", views.UpdateType.as_view(), name="update-type"),
    path("customized-box/types", views.types, name="types"),
    path("customized-box/create-box", views.AddBox.as_view(), name="add-box"),
    path("customized-box/update-box/<pk>", views.UpdateBox.as_view(), name="update-box"),
    path("customized-box/boxes", views.boxes, name="boxes"),


    path("login", views.dash_login, name="dash-login"),
    path("logout", views.dash_logout, name="dash-logout"),




    path("customers", views.customers, name="customers"),
    path("transactions", views.transactions, name="transactions"),
    path("invoice/<order_id>", views.invoice, name="invoice"),

    path("create-subcategory/<category_id>", views.create_subcategory, name="create-sub"),
    path("update-subcategory/<pk>", views.UpdateSubCategory.as_view(), name="update-sub"),


    path("delete-sub/<id>", views.delete_sub, name="delete-sub"),


    path("trays/", views.trays_view, name="trays"),
    path("add-tray/", views.AddTray.as_view(), name="add-tray"),
    path("delete-tray/<id>", views.delete_tray, name="delete-tray"),
    path("update-tray/<slug>", views.UpdateTray.as_view(), name="update-tray"),



    path("flowers/", views.flowers_view, name="flowers"),
    path("add-flower/", views.AddFlower.as_view(), name="add-flower"),
    path("delete-flower/<id>", views.delete_flower, name="delete-flower"),
    path("update-flower/<slug>", views.UpdateFlower.as_view(), name="update-flower"),



    path("ribbons/", views.ribbons_view, name="ribbons"),
    path("add-ribbon/", views.AddRibbon.as_view(), name="add-ribbon"),
    path("delete-ribbon/<id>", views.delete_ribbon, name="delete-ribbon"),
    path("update-ribbon/<slug>", views.UpdateRibbon.as_view(), name="update-ribbon"),


    path("toppers/", views.toppers_view, name="toppers"),
    path("add-topper/", views.AddTopper.as_view(), name="add-topper"),
    path("delete-topper/<id>", views.delete_topper, name="delete-topper"),
    path("update-topper/<slug>", views.UpdateTopper.as_view(), name="update-topper"),


    path("cards/", views.cards_view, name="cards"),
    path("add-card/", views.AddCard.as_view(), name="add-card"),
    path("delete-card/<id>", views.delete_card, name="delete-card"),
    path("update-card/<slug>", views.UpdateCard.as_view(), name="update-card"),



    path("box-items/", views.box_items_view, name="box-items"),
    path("add-box-item/", views.CreateBoxItem.as_view(), name="add-box-item"),
    path("update-box-item/<pk>", views.UpdateBoxItem.as_view(), name="update-box-item"),
    path("delete-box-item/<id>", views.delete_box_item, name="delete-box-item"),


    path("update-banner/", views.UpdateBannerImage.as_view(), name="update-banner"),








    path("update-minimum-order", views.UpdateMinimuOrder.as_view(), name="update-minimum-order"),
    path("update-delivery-charge", views.UpdateDeliveryCharge.as_view(), name="update-delivery-charge"),


    path("contact-us", views.contact_us_messages, name="contact-messages"),
    path("contact-us/<id>", views.contact_us_message, name="contact-message"),

    path("create-choice", views.CreateChoice.as_view(), name="create-choice"),


    path("add-package", views.AddPackage.as_view(), name="add-package"),
    path("update-package/<pk>", views.UpdatePackage.as_view(), name="update-package"),
    path("delete-package/<id>", views.delete_package, name="delete-package"),


    path("packages", views.packages_view, name="packages"),



    path("reports", views.reports, name="reports"),




    
    path("general-income-report", views.general_income_report, name="general-income"),
    path("advanced-income-report", views.advanced_income_report, name="advanced-income"),
    path("visits-report", views.visits_report, name="visits-report"),
    path("users-report", views.users_report, name="users-report"),


    path("set-date-off", views.set_date_off, name="set-date-off"),
    path("update-date-off/<id>", views.update_date_off, name="update-date-off"),
    path("delete-date-off/<id>", views.delete_date_off, name="delete-date-off"),


    path("off-dates", views.off_dates, name="off-dates"),
    path("update-pop-up", views.UpdatePopUp.as_view(), name="update-pop-up"),




    path("create-coupon", views.CreateCoupon.as_view(), name="create-coupon"),
    path("update-coupon/<pk>", views.UpdateCoupon.as_view(), name="update-coupon"),
    path("update-discount", views.UpdateDiscount.as_view(), name="update-discount"),


    path("coupons-list", views.coupons_list, name="coupons-list"),
    path("delete-coupon/<id>", views.delete_coupon, name="delete-coupon"),

    path("colors", views.colors_list, name="colors-list"),
    path("create-color", views.CreateColor.as_view(), name="create-color"),
    path("update-color/<pk>", views.UpdateColor.as_view(), name="update-color"),
    path("delete-color/<pk>", views.delete_color, name="delete-color"),


    path("flavors", views.flavors_list, name="flavors-list"),
    path("create-flavor", views.CreateFlavor.as_view(), name="create-flavor"),
    path("update-flavor/<pk>", views.UpdateFlavor.as_view(), name="update-flavor"),
    path("delete-flavor/<pk>", views.delete_flavor, name="delete-flavor"),













]