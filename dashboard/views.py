from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect, render
from django.urls.base import reverse
from app.models import Color, Discount, Coupon, BannerImage, BoxItem, ContactUs, Flavor, Item, Category, Order, User, WorkingDay, AboutUsContent, AboutUsMainImage, AboutUsTopic, ChocolateType, Box, Transaction, SubCategory, Tray, Flower, Ribbon, Topper, Card, MinimumOrder, DeliveryCharge, Package, Visit, OrderItem, DateOff, PopUp
from django.views.generic import CreateView, UpdateView



import decimal



from django.db.models import Q




from .forms import ColorForm, CreateCouponForm, AboutMainImageForm, AboutTopicForm, AboutUsContentForm, BannerImageForm, ChoiceForm, DiscountForm, FlavorForm, ItemForm, CategoryForm, CategoryItem, PackageForm, PopUpForm, SubCategoryForm, UserLogin, WorkingHoursForm, ChocolateTypeForm, BoxForm, TrayForm, FlowerForm, RibbonForm, TopperForm, CardForm, MinimumOrderForm, BoxItemForm, DeliveryChargeForm, SetDateOffForm
from django.http import JsonResponse
from django.http.response import FileResponse, HttpResponse

from django.utils import timezone
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
import datetime
from datetime import timedelta
from django.utils import timezone

from io import BytesIO
import xlsxwriter


def check_admin(user):
    return user.is_superuser





def check_staff(user):
    return user.is_staff or user.is_superuser



def dash_logout(request):

    logout(request)

    return redirect("dashboard:dash-login")



@user_passes_test(check_admin, login_url="/dashboard/login")
def index(request):
    items = Item.objects.filter(customized_box=False)
    all_orders = Order.objects.filter(ordered=True)
    done_orders = Order.objects.filter(ordered=True).exclude(status="Canceled")
    pending_orders = Order.objects.all().exclude(Q(status="Canceled"))
    users = User.objects.exclude(password="lkasjdhflksajdhflkLKJHLkdjhf")
    last_users = users.exclude(is_staff=True).exclude(is_superuser=True)
    all_revenue = 0
    done_revenue = 0
    ordered_items = OrderItem.objects.filter(ordered=True).order_by("-ordered_date")
    print(datetime.datetime.today())
    today_ordered = OrderItem.objects.filter(ordered=True, ordered_date=datetime.datetime.today()).order_by("-ordered_date")
    if today_ordered.exists():
        if today_ordered.count() >= 3:
            trending_ids = today_ordered.values_list("item__id", flat=True)
        else:
            trending_ids = ordered_items.values_list("item__id", flat=True)
    else:
        trending_ids = ordered_items.values_list("item__id", flat=True)
    trending = Item.objects.filter(id__in=trending_ids)
    featured_items = trending[3:6]
    for order in done_orders:
        total = order.get_order_total()
        done_revenue += total

    for order in all_orders:
        total = order.get_order_total()
        all_revenue += total

    




    





    this_week = datetime.date.today()-timedelta(days=7)

    week_orders = Order.objects.filter(ordered_date__gte=this_week).exclude(status="Canceled")


    
    week_sales = 0
    for order in week_orders:
        week_sales += order.get_order_total()
    if week_orders:
        week_order_average = round(week_sales / week_orders.count(), 2)
    else:
        week_order_average = 0

    

    current_month = datetime.datetime.now().month

    current_year = datetime.datetime.now().year

    year_orders = Order.objects.filter(ordered_date__year=current_year).exclude(status="Canceled")
    year_sales = 0
    for order in year_orders:
        year_sales += order.get_order_total()

    if year_orders:
        
        year_order_average = round(year_sales / year_orders.count(), 2)
    else:
        year_order_average = 0



    print(current_month)

    month_orders = Order.objects.filter(ordered_date__month=current_month).exclude(status="Canceled")
    month_sales = 0
    for order in month_orders:
        month_sales += order.get_order_total()

    if month_orders:
        month_order_average = round(month_sales / month_orders.count(), 2)
    else:
        month_order_average = 0


    week_visits = Visit.objects.filter(date__gte=this_week).count()
    month_visits = Visit.objects.filter(date__month=current_month).count()
    year_visits = Visit.objects.filter(date__year=current_year).count()



    week_users = User.objects.filter(date_joined__gte=this_week, is_superuser=False).exclude(password="lkasjdhflksajdhflkLKJHLkdjhf").order_by("date_joined")
    month_users = User.objects.filter(date_joined__month=current_month, is_superuser=False).exclude(password="lkasjdhflksajdhflkLKJHLkdjhf").order_by("date_joined")
    year_users = User.objects.filter(date_joined__year=current_year, is_superuser=False).exclude(password="lkasjdhflksajdhflkLKJHLkdjhf").order_by("date_joined")


    print(month_users, "MONTH USERS")
    print(current_month, "CURRENT MONTH")


    year_ordered_items = OrderItem.objects.filter(ordered=True, ordered_date__year=current_year).values_list("item__id", flat=True)
    my_dict = {i:list(year_ordered_items).count(i) for i in list(year_ordered_items)}
    print(my_dict)
    year_items = []
    for i in my_dict:
        if not i == 210:
            item = Item.objects.get(id=i)
            obj = {
                "title": item.title,
                "sales": item.price * my_dict[i],
                "count": my_dict[i]
            }
            year_items.append(obj)
    year_items = sorted(year_items, key = lambda i: i['count'],reverse=True)


    


    month_ordered_items = OrderItem.objects.filter(ordered=True, ordered_date__month=current_month).values_list("item__id", flat=True)
    my_dict = {i:list(month_ordered_items).count(i) for i in list(month_ordered_items)}
    month_items = []
    for i in my_dict:
        if not i == 210:
            item = Item.objects.get(id=i)
            obj = {
                "title": item.title,
                "sales": item.price * my_dict[i],
                "count": my_dict[i]
            }
            month_items.append(obj)

    month_items = sorted(month_items, key = lambda i: i['count'],reverse=True)


    week_ordered_items = OrderItem.objects.filter(ordered=True, ordered_date__gte=this_week).values_list("item__id", flat=True)
    my_dict = {i:list(week_ordered_items).count(i) for i in list(week_ordered_items)}
    week_items = []
    for i in my_dict:
        if not i == 210:
            item = Item.objects.get(id=i)
            obj = {
                "title": item.title,
                "sales": item.price * my_dict[i],
                "count": my_dict[i]
            }
            week_items.append(obj)
    
    week_items = sorted(week_items, key = lambda i: i['count'],reverse=True)


    canceled = Order.objects.filter(status="Canceled").count()
    delivered = Order.objects.filter(status="Delivered").count()
    pending = Order.objects.filter(ordered=True).exclude(status="Canceled").exclude(status="Delivered").count()



    context = {
        "items": items,
        "all_orders": all_orders,
        "orders": done_orders,
        "users": last_users,
        "revenue": done_revenue,
        "featured_items": featured_items,
        "canceled": canceled,
        "delivered": delivered,
        "pending": pending,

        "all_revenue": all_revenue,
        "week_sales": week_sales,
        "month_sales": month_sales,
        "year_sales": year_sales,
        "week_visits": week_visits,
        "month_visits": month_visits,
        "year_visits": year_visits,
        "week_users": week_users,
        "month_users": month_users,
        "year_users": year_users,


        "week_items": week_items,
        "month_items": month_items,
        "year_items": year_items,

        "week_order_average": week_order_average,
        "month_order_average": month_order_average,
        "year_order_average": year_order_average,
    }

    return render(request, "dashboard/index.html", context)


@user_passes_test(check_staff, login_url="/dashboard/login")
def items_view(request):
    items = Item.objects.filter(is_deleted=False)
    categories = Category.objects.all().order_by("number")


    context = {
        "items": items,
        "categories": categories
    }

    return render(request, "dashboard/items.html", context)



class UpdateMinimuOrder(UserPassesTestMixin, UpdateView):
    
    login_url = "/dashboard/login"


    success_url = "/dashboard"

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_superuser
    

    def get_object(self):
        return MinimumOrder.objects.first()

    model = MinimumOrder
    form_class = MinimumOrderForm
    template_name = "dashboard/minimum_order.html"


class UpdateDeliveryCharge(UserPassesTestMixin, UpdateView):
    
    login_url = "/dashboard/login"


    success_url = "/dashboard"

    def test_func(self):
        return self.request.user.is_superuser
    

    def get_object(self):
        return DeliveryCharge.objects.first()

    model = DeliveryCharge
    form_class = DeliveryChargeForm
    template_name = "dashboard/delivery_charge.html"




class UpdateBannerImage(UpdateView):
    
    login_url = "/dashboard/login"


    success_url = "/dashboard"


    

    def get_object(self):
        return BannerImage.objects.first()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        banner_image = self.get_object()
        context["banner_image"] = banner_image

        return context
    model = BannerImage
    form_class = BannerImageForm
    template_name = "dashboard/update_banner.html"




class CreateItem(UserPassesTestMixin, CreateView):

    login_url="/dashboard/login"

        

    def test_func(self):
        return self.request.user.is_superuser
    form_class = ItemForm
    template_name = "dashboard/create_item.html"




class CreateChoice(UserPassesTestMixin, CreateView):

    login_url="/dashboard/login"

        

    def test_func(self):
        return self.request.user.is_superuser
    form_class = ChoiceForm
    template_name = "dashboard/create_choice.html"




def box_items_view(request):
    items = BoxItem.objects.all()


    context = {
        "items": items
    }

    return render(request, "dashboard/box_items.html", context)



class CreateBoxItem(UserPassesTestMixin, CreateView):

    login_url="/dashboard/login"

        

    def test_func(self):
        return self.request.user.is_superuser
    form_class = BoxItemForm
    template_name = "dashboard/add_box_item.html"


class UpdateBoxItem(UserPassesTestMixin, UpdateView):

    login_url="/dashboard/login"

    def test_func(self):
        return self.request.user.is_superuser

    def post(self, *args, **kwargs):
        box_item = self.get_object()
        form = self.get_form()
        if form.is_valid():
            image = form.cleaned_data.get("image")
            title = form.cleaned_data.get("title")
            ar_title = form.cleaned_data.get("ar_title")
            price = form.cleaned_data.get("price")
            price = form.cleaned_data.get("price")
            type = form.cleaned_data.get("type")



            print(image)
            
            if image:
                box_item.title = title
                box_item.ar_title = ar_title
                box_item.price = price
                box_item.image = image
                box_item.type = type
                box_item.save()
            else:
                
                box_item.title = title
                box_item.ar_title = ar_title
                box_item.price = price
                box_item.type = type

                
                box_item.save()
            return redirect("dashboard:box-items")
        else:
            print(form.errors)
            context = {
                "form": form,
                "box_item": self.get_object()

            }
            return render(self.request, "dashboard/update_box_item.html", context)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["box_item"] = self.get_object()
        return context


    model = BoxItem
    form_class = BoxItemForm
    template_name = "dashboard/update_box_item.html"



def delete_box_item(request, id):
    item = BoxItem.objects.get(id=id)

    item.delete()

    return redirect("dashboard:box-items")


class UpdateItem(UserPassesTestMixin, UpdateView):

    login_url="/dashboard/login"

    def test_func(self):
        return self.request.user.is_superuser


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["item"] = self.get_object()
        return context

    model = Item
    form_class = ItemForm
    template_name = "dashboard/update_item.html"

    


def delete_item(request, id):
    item = Item.objects.get(id=id)

    item.is_deleted = True
    item.save()

    return redirect("dashboard:items")






def create_category(request):

    form = CategoryForm()
    if request.method == "POST":
        form = CategoryForm(request.POST, request.FILES)
        if form.is_valid():

            
            
            

            

            if Category.objects.filter(en_title=form.cleaned_data.get("en_title")).exists():
                error = "Category with that title already exists"
                print("hiii")
                context = {
                    "error": error
                }
                return render(request, "dashboard/create_category.html", context)

            if Category.objects.filter(ar_title=form.cleaned_data.get("ar_title")).exists():
                error = "Category with that AR title already exists"
                print("hiii")
                context = {
                    "error": error,
                    "form": form
                }
                return render(request, "dashboard/create_category.html", context)
            
            category = form.save(commit=False)
            
            

            

            
            print("hiiiii")

            if Category.objects.filter(number=form.cleaned_data.get("number")).exists():
                Category.objects.filter(number=form.cleaned_data.get("number")).update(number=form.cleaned_data.get("number"))
            category.number = form.cleaned_data.get("number")
            category.save()
            return redirect("dashboard:items")

    

    return render(request, "dashboard/create_category.html", {"form": form})




@user_passes_test(check_staff, login_url="/dashboard/login")
def orders(request):


    today_min = datetime.datetime.combine(datetime.date.today(), datetime.time.min)
    today_max = datetime.datetime.combine(datetime.date.today(), datetime.time.max)

    today_orders = Order.objects.filter(ordered_date__range=(today_min, today_max), ordered=True, canceled=False).order_by("-status")


    this_week = datetime.date.today()-timedelta(days=7)

    week_orders = Order.objects.filter(ordered_date__gte=this_week, ordered=True, canceled=False).order_by("-status")
    

 

    current_month = datetime.datetime.now().month

    print(current_month)

    month_orders = Order.objects.filter(ordered_date__month=current_month, ordered=True, canceled=False).order_by("-status")


    orders = Order.objects.filter(ordered=True, canceled=False).order_by("-status")

    context = {
        "today_orders": today_orders,
        "month_orders": month_orders,
        "week_orders": week_orders,
        "orders": orders
    }

    return render(request, "dashboard/orders.html", context)


def categories_view(request):
    categories = Category.objects.all()

    context = {
        "categories": categories
    }

    return render(request, "dashboard/categories.html", context)

@user_passes_test(check_admin, login_url="/dashboard/login")
def create_subcategory_item(request, id):
    sub_category = SubCategory.objects.get(id=id)
    category = sub_category.categories.first()
    if request.method == "POST":
        form = ItemForm(request.POST, request.FILES)
        
        if form.is_valid():
            flavors = form.cleaned_data.get("flavors")
            colors = form.cleaned_data.get("colors")
            
            item = form.save(commit=False)
            
            item.category = category
            item.sub_category = sub_category
            

            item.save()
            item.flavors.add(*flavors)
            item.colors.add(*colors)

            return redirect("dashboard:items")
        else:
            print(form.errors)
            return render(request, 'dashboard/add_subcategory_item.html', {"form": form})
    else:
        form = ItemForm()
        return render(request, 'dashboard/add_subcategory_item.html', {"form": form})



@user_passes_test(check_admin, login_url="/dashboard/login")
def create_category_item(request, id):
    category = Category.objects.get(id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, request.FILES)

        if form.is_valid():
            flavors = form.cleaned_data.get("flavors")
            colors = form.cleaned_data.get("colors")
            item = form.save(commit=False)
            item.category = category
            item.save()

            item.flavors.add(*flavors)
            item.colors.add(*colors)

            

            return redirect("dashboard:items")
        else:
            print(form.errors)
    else:
        form = ItemForm()
        return render(request, 'dashboard/add_category_item.html', {"form": form})


@user_passes_test(check_staff, login_url="/dashboard/login")
def change_status(request, id, status):
    order = Order.objects.get(id=id)
    order.status = status
    order.save()
    return JsonResponse({"status": order.status}, status=200)

@user_passes_test(check_staff, login_url="/dashboard/login")
def change_new_status(request, id, status):
    order = Order.objects.get(id=id)
    order.status = status
    order.save()
    return redirect("dashboard:orders")



@user_passes_test(check_admin, login_url="/dashboard/login")
def days_list(request):

    days = WorkingDay.objects.all()
    context = {
        "days": days
    }

    return render(request, "dashboard/days_list.html", context)





class WorkingHours(UserPassesTestMixin, UpdateView):

    login_url="/dashboard/login"

    def test_func(self):
        return self.request.user.is_superuser
    model = WorkingDay
    form_class = WorkingHoursForm
    template_name = "dashboard/working_hours.html"







def order_details(request, id):

    order = Order.objects.get(id=id)
    context = {
        "order": order
    }

    return render(request, "dashboard/order_details.html", context)




def delete_category(request, id):

    category = Category.objects.get(id=id)
    category.delete()
    return redirect("dashboard:items")



def update_category(request, id):

    category = Category.objects.get(id=id)

    if request.method == "POST":
        newNumber = request.POST.get("number")
        title = request.POST.get("title")
        ar_title = request.POST.get("ar_title")
        old_number = category.number

        image = request.FILES.get("picture")
        banner_image = request.FILES.get("banner_picture")

        hidden_input = request.POST.get("hidden")
        has_dimensions_input = request.POST.get("has_dimensions")

        if has_dimensions_input == "on":
            has_dimensions = True
        else:
            has_dimensions = False


        if hidden_input == "on":
            hidden = True
        else:
            hidden = False



        if category.en_title != title:

            if Category.objects.filter(en_title=title).exists():
                error = "Category with that title already exists"
                print("hiii")
                context = {
                    "category": category,
                    "error": error
                }
                return render(request, "dashboard/update_category.html", context)
            
        
        category.en_title = title
        category.ar_title = ar_title
        category.hidden = hidden
        category.has_dimensions = has_dimensions
        
        category.save()

        if image:
            category.image = image

        if banner_image:
            category.banner_image = banner_image
        category.save()
        print("hiiiii")
        if old_number != newNumber:
            print("inside")

            if Category.objects.filter(number=newNumber).exists():
                print(Category.objects.filter(number=newNumber))
                print("in inside")
                for inCategory in Category.objects.filter(number__gte=newNumber):
                    inCategory.number += 1
                    inCategory.save()

        category.number = newNumber

        print(category)
        print(newNumber)
        category.save()
        print(category.number)
            
        return redirect("dashboard:items")
            
                

       

    context = {
        "category": category
    }
    

    return render(request, "dashboard/update_category.html", context)




def cancel_order(request, id):
    order = Order.objects.get(id=id)
    order.canceled = True
    order.save()

    return JsonResponse({"success": True}, status=200)




    
class UpdateAboutContent(UserPassesTestMixin, UpdateView):

    login_url="/dashboard/login"


    def test_func(self):
        return self.request.user.is_superuser

    model = AboutUsContent

    form_class = AboutUsContentForm
    template_name = "dashboard/about_us_content.html"
    success_url = "/dashboard/about-us"


    def get_object(self):
        return AboutUsContent.objects.first()



class UpdateMainImage(User, UpdateView):

    def test_func(self):
        return self.request.user.is_superuser

    model = AboutUsMainImage

    form_class = AboutMainImageForm
    template_name = "dashboard/about_main_image.html"
    success_url = "/dashboard/about-us"

    def get_object(self):
        return AboutUsMainImage.objects.first()



def topics_list(request):

    topics = AboutUsTopic.objects.all().order_by("number")

    context = {
        "topics": topics
    }

    return render(request, "dashboard/topics_list.html", context)



class UpdateTopic(UserPassesTestMixin, UpdateView):

    login_url="/dashboard/login"


    def test_func(self):
        return self.request.user.is_superuser

    model = AboutUsTopic
    form_class = AboutTopicForm
    template_name = "dashboard/topic_form.html"
    success_url = "/dashboard/about-us"


def types(request):

    types = ChocolateType.objects.all()

    context = {
        "types": types
    }

    return render(request, "dashboard/types.html", context)



class AddType(UserPassesTestMixin, CreateView):

    login_url="/dashboard/login"
    
    def test_func(self):
        return self.request.user.is_superuser

    model = ChocolateType
    form_class = ChocolateTypeForm
    template_name = "dashboard/type_form.html"



class UpdateType(UserPassesTestMixin, UpdateView):

    login_url="/dashboard/login"

    def test_func(self):
        return self.request.user.is_superuser

    model = ChocolateType
    form_class = ChocolateTypeForm
    template_name = "dashboard/type_form.html"




def boxes(request):

    boxes = Box.objects.all()

    context = {
        "boxes": boxes
    }

    return render(request, "dashboard/boxes.html", context)




class AddBox(UserPassesTestMixin ,CreateView):

    login_url="/dashboard/login"

    def test_func(self):
        return self.request.user.is_superuser


    model = Box
    form_class = BoxForm
    template_name = "dashboard/box_form.html"



class UpdateBox(UserPassesTestMixin, UpdateView):

    login_url="/dashboard/login"

    def test_func(self):
        return self.request.user.is_superuser


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["box"] = self.get_object()
        return context

    model = Box
    form_class = BoxForm
    template_name = "dashboard/box_form.html"
    success_url = "/dashboard/"




def about_us(request):

    topics = AboutUsTopic.objects.all().order_by("number")

    context = {
        "topics": topics
    }

    return render(request, "dashboard/about_us.html", context)



def customized_box(request):


    return render(request, "dashboard/customized_box.html")






def dash_login(request):
    form = UserLogin()

    if request.method == "POST":
        form = UserLogin(data=request.POST)
        if form.is_valid():
            print('in')
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(username=username, password=password)
            print(user)

            if user:

                if user.is_active:

                    login(request, user)
                    if user.is_superuser:
                        return redirect("dashboard:index")
                    elif user.is_staff:

                        return redirect("dashboard:orders")
                    else:
                        return redirect("apis:home")
                else:
                    print("not active user")
            else:
                print("user does not exist")
        else:
            print(form.errors)

    context = {
        "form": form
    }

    return render(request, "dashboard/login.html", context)




@user_passes_test(check_admin, login_url="/dashboard/login")
def customers(request):

    today_min = datetime.datetime.combine(datetime.date.today(), datetime.time.min)
    today_max = datetime.datetime.combine(datetime.date.today(), datetime.time.max)

    today_users = User.objects.filter(date_joined__range=(today_min, today_max), is_staff=False).exclude(password="lkasjdhflksajdhflkLKJHLkdjhf")


    this_week = datetime.date.today()-timedelta(days=7   )

    week_users = User.objects.filter(date_joined__gte=this_week, is_staff=False).exclude(password="lkasjdhflksajdhflkLKJHLkdjhf")
    

 

    current_month = datetime.datetime.now().month

    print(current_month)

    month_users = User.objects.filter(date_joined__month=current_month, is_staff=False).exclude(password="lkasjdhflksajdhflkLKJHLkdjhf")

    print(month_users)
    

    context = {
        "today_users": today_users, 
        "week_users": week_users,
        "month_users": month_users
    }

    return render(request, "dashboard/customers.html", context)




@user_passes_test(check_admin, login_url="/dashboard/login")
def transactions(request):
    
    today_min = datetime.datetime.combine(datetime.date.today(), datetime.time.min)
    today_max = datetime.datetime.combine(datetime.date.today(), datetime.time.max)

    today_transactions = Transaction.objects.filter(date__range=(today_min, today_max), done=True)


    some_day_last_week = timezone.now().date() - timedelta(days=7)
    monday_of_last_week = some_day_last_week - timedelta(days=(some_day_last_week.isocalendar()[2] - 1))
    monday_of_this_week = monday_of_last_week + timedelta(days=7)
    week_transactions = Transaction.objects.filter(date__gte=monday_of_last_week, date__lt=monday_of_this_week, done=True)


    current_month = datetime.datetime.now().month
    print(current_month)

    month_transactions = Transaction.objects.filter(date__month=current_month, done=True)

    transactions = Transaction.objects.filter(done=True)

    context = {
        "transactions": transactions,
        "today_transactions": today_transactions,
        "week_transactions": week_transactions,
        "month_transactions": month_transactions
    }

    return render(request, "dashboard/transactions.html", context)





def invoice(request, order_id):
    order = Order.objects.get(id=order_id)
    delivery_charge = DeliveryCharge.objects.first().price
    if Discount.objects.filter(start_date__lte=datetime.datetime.now().date(), end_date__gte=datetime.datetime.now().date()).exists():
        discount = Discount.objects.get(start_date__lte=datetime.datetime.now().date(), end_date__gte=datetime.datetime.now().date())

    else:
        discount = None


    context = {
        "order": order,
        "delivery_charge": delivery_charge,
        "discount": discount
    }

    return render(request, "dashboard/invoice.html", context)




def create_subcategory(request, category_id):
    category = Category.objects.get(id=category_id)

    form = SubCategoryForm()
    
    if request.method == "POST":
        form = SubCategoryForm(data=request.POST)
        en_title = request.POST.get("en_title")
        ar_title = request.POST.get("ar_title")

        number = request.POST.get("number")


        if SubCategory.objects.filter(en_title=en_title).exists():
            error = "Sub Category with that title already exists"
            context = {
                "error": error
            }
            return render(request, "dashboard/create_sub.html", context)

        if SubCategory.objects.filter(number=number).exists():
            
            subs = SubCategory.objects.filter(number__gte=number)
            print(subs)
            for sub in subs:
                sub.number += 1
                sub.save()

        sub_category = SubCategory.objects.create(en_title=en_title, ar_title=ar_title, number=number)
        category.subs.add(sub_category)
        return redirect("dashboard:items")

    context = {
        "form": form
    }

    return render(request, "dashboard/create_sub.html", context)




class UpdateSubCategory(UserPassesTestMixin, UpdateView):


    login_url="/dashboard/login"

    def test_func(self):
        return self.request.user.is_superuser


    
    
        


    model = SubCategory
    form_class = SubCategoryForm
    template_name = "dashboard/update_sub.html"



def delete_sub(request, id):
    sub = SubCategory.objects.get(id=id)

    sub.delete()

    return redirect("dashboard:items")




def trays_view(request):
    trays = Tray.objects.all()


    context = {
        "trays": trays
    }

    return render(request, "dashboard/trays.html", context)




class UpdateTray(UserPassesTestMixin, UpdateView):

    login_url="/dashboard/login"

    def test_func(self):
        return self.request.user.is_superuser

    
    def post(self, *args, **kwargs):
        tray = self.get_object()
        form = self.get_form()
        if form.is_valid():
            image = form.cleaned_data.get("picture")
            en_name = form.cleaned_data.get("en_name")
            ar_name = form.cleaned_data.get("ar_name")
            price = form.cleaned_data.get("price")
            weight = form.cleaned_data.get("weight")
            unit = form.cleaned_data.get("unit")
            height = form.cleaned_data.get("height")
            width = form.cleaned_data.get("width")
            length = form.cleaned_data.get("length")
            type = form.cleaned_data.get("type")
            delivery_date = form.cleaned_data.get("delivery_date")




            if image:
                tray.en_name = en_name
                tray.ar_name = ar_name
                tray.price = price
                tray.weight = weight
                tray.unit = unit
                tray.height = height
                tray.width = width
                tray.length = length
                tray.image = image
                tray.type = type
                tray.delivery_date = delivery_date

                tray.save()
            else:
                
                tray.en_name = en_name
                tray.ar_name = ar_name
                tray.price = price
                tray.weight = weight
                tray.unit = unit
                tray.height = height
                tray.width = width
                tray.length = length
                tray.type = type
                tray.delivery_date = delivery_date
                tray.save()
            return redirect("dashboard:trays")
        else:
            context = {
                "tray": self.get_object(),
                "form": form
            }
            return render(self.request, "dashboard/update_tray.html", context)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["tray"] = self.get_object()
        return context

    model = Tray
    form_class = TrayForm
    template_name = "dashboard/update_tray.html"


class AddTray(UserPassesTestMixin, CreateView):

    login_url="/dashboard/login"

    def test_func(self):
        return self.request.user.is_superuser
    

    model = Tray
    form_class = TrayForm
    template_name = "dashboard/update_tray.html"




def delete_tray(request, id):
    tray = Tray.objects.get(id=id)
    tray.delete()


    return redirect("dashboard:trays")




def packages_view(request):
    packages = Package.objects.all()


    context = {
        "packages": packages
    }

    return render(request, "dashboard/packages.html", context)



class AddPackage(UserPassesTestMixin, CreateView):

    login_url="/dashboard/login"

    def test_func(self):
        return self.request.user.is_superuser
    

    model = Package
    form_class = PackageForm
    template_name = "dashboard/package_form.html"



class UpdatePackage(UserPassesTestMixin, UpdateView):

    login_url="/dashboard/login"

    def test_func(self):
        return self.request.user.is_superuser


    def post(self, *args, **kwargs):
        package = self.get_object()
        form = self.get_form()
        if form.is_valid():
            image = form.cleaned_data.get("picture")
            en_name = form.cleaned_data.get("en_name")
            ar_name = form.cleaned_data.get("ar_name")
            price = form.cleaned_data.get("price")
            weight = form.cleaned_data.get("weight")
            pieces = form.cleaned_data.get("pieces")

            en_unit = form.cleaned_data.get("en_unit")
            ar_unit = form.cleaned_data.get("ar_unit")

            subcategory = form.cleaned_data.get("subcategory")
           



            if image:
                package.en_name = en_name
                package.ar_name = ar_name
                package.price = price
                package.weight = weight
                package.pieces = pieces

                package.en_unit = en_unit
                package.ar_unit = ar_unit
                package.subcategory = subcategory
                package.image = image

                package.save()
            else:
                
                package.en_name = en_name
                package.ar_name = ar_name
                package.price = price
                package.weight = weight
                package.pieces = pieces

                package.en_unit = en_unit
                package.ar_unit = ar_unit
                package.subcategory = subcategory
                package.save()
            return redirect("dashboard:packages")
        else:
            context = {
                "form": form,
                "package": self.get_object()
            }
            return render(self.request, "dashboard/package_form.html", context)


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["package"] = self.get_object()
        return context
    

    model = Package
    form_class = PackageForm
    template_name = "dashboard/package_form.html"



def delete_package(request, id):
    package = Package.objects.get(id=id)
    package.delete()


    return redirect("dashboard:packages")




def flowers_view(request):
    flowers = Flower.objects.all()


    context = {
        "flowers": flowers
    }

    return render(request, "dashboard/flowers.html", context)



class UpdateFlower(UserPassesTestMixin, UpdateView):

    login_url="/dashboard/login"

    def test_func(self):
        return self.request.user.is_superuser

    def post(self, *args, **kwargs):
        tray = self.get_object()
        form = self.get_form()
        if form.is_valid():
            image = form.cleaned_data.get("picture")
            en_name = form.cleaned_data.get("en_name")
            ar_name = form.cleaned_data.get("ar_name")
            price = form.cleaned_data.get("price")


            if image:
                tray.en_name = en_name
                tray.ar_name = ar_name
                tray.price = price
                tray.image = image
                tray.save()
            else:
                
                tray.en_name = en_name
                tray.ar_name = ar_name
                tray.price = price
                
                tray.save()
            return redirect("dashboard:flowers")
        else:
            context = {
                "form": form,
                "flower": self.get_object()
            }
            return render(self.request, "dashboard/update_flower.html", context)


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["flower"] = self.get_object()
        return context

    model = Flower
    form_class = FlowerForm
    template_name = "dashboard/update_flower.html"

class AddFlower(UserPassesTestMixin, CreateView):

    login_url="/dashboard/login"

    def test_func(self):
        return self.request.user.is_superuser
    

    model = Flower
    form_class = FlowerForm
    template_name = "dashboard/update_flower.html"


def delete_flower(request, id):
    flower = Flower.objects.get(id=id)
    flower.delete()


    return redirect("dashboard:flowers")






@user_passes_test(check_admin, login_url="/dashboard/login")
def ribbons_view(request):
    ribbons = Ribbon.objects.all()


    context = {
        "ribbons": ribbons
    }

    return render(request, "dashboard/ribbons.html", context)

class UpdateRibbon(UserPassesTestMixin, UpdateView):

    login_url="/dashboard/login"

    def test_func(self):
        return self.request.user.is_superuser

    model = Ribbon
    form_class = RibbonForm
    template_name = "dashboard/update_ribbon.html"

class AddRibbon(UserPassesTestMixin, CreateView):

    login_url="/dashboard/login"

    def test_func(self):
        return self.request.user.is_superuser
    

    model = Ribbon
    form_class = RibbonForm
    template_name = "dashboard/update_ribbon.html"

def delete_ribbon(request, id):
    ribbon = Ribbon.objects.get(id=id)
    ribbon.delete()


    return redirect("dashboard:ribbons")






def toppers_view(request):
    toppers = Topper.objects.all()


    context = {
        "toppers": toppers
    }

    return render(request, "dashboard/toppers.html", context)

class UpdateTopper(UserPassesTestMixin, UpdateView):

    login_url="/dashboard/login"

    def test_func(self):
        return self.request.user.is_superuser

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["topper"] = self.get_object()
        return context

    model = Topper
    form_class = TopperForm
    template_name = "dashboard/update_topper.html"

class AddTopper(UserPassesTestMixin, CreateView):

    login_url="/dashboard/login"

    def test_func(self):
        return self.request.user.is_superuser
    

    model = Topper
    form_class = TopperForm
    template_name = "dashboard/update_topper.html"


def delete_topper(request, id):
    topper = Topper.objects.get(id=id)
    topper.delete()


    return redirect("dashboard:toppers")





def cards_view(request):
    cards = Card.objects.all()


    context = {
        "cards": cards
    }

    return render(request, "dashboard/cards.html", context)

class UpdateCard(UserPassesTestMixin, UpdateView):

    login_url="/dashboard/login"

    def test_func(self):
        return self.request.user.is_superuser

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["card"] = self.get_object()
        return context

    model = Card
    form_class = CardForm
    template_name = "dashboard/update_card.html"

class AddCard(UserPassesTestMixin, CreateView):

    login_url="/dashboard/login"

    def test_func(self):
        return self.request.user.is_superuser
    

    model = Card
    form_class = CardForm
    template_name = "dashboard/update_card.html"


def delete_card(request, id):
    card = Card.objects.get(id=id)
    card.delete()


    return redirect("dashboard:cards")




def contact_us_messages(request):
    messages = ContactUs.objects.all().order_by("-date")

    context = {
        "messages": messages
    }

    return render(request, "dashboard/contact_us_messages.html", context)



def contact_us_message(request, id):


    message = ContactUs.objects.get(id=id)

    context = {
        "message": message
    }

    return render(request, "dashboard/contact_us_message.html", context)



@user_passes_test(check_admin, login_url="/dashboard/login")
def general_income_report(request):



    request_from_date = request.POST.get("general_income_from")
    from_year = request_from_date.split("/")[2]
    from_month = request_from_date.split("/")[1]
    from_day = request_from_date.split("/")[0]


    from_date = datetime.datetime(int(from_year), int(from_month), int(from_day))
    print(from_date, "FROM DATE")


    request_to_date = request.POST.get("general_income_to")
    to_year = request_to_date.split("/")[2]
    to_month = request_to_date.split("/")[1]
    to_day = request_to_date.split("/")[0]


    to_date = datetime.datetime(int(to_year), int(to_month), int(to_day), 23)
    print(to_date, "TO DATE")


    all_orders = Order.objects.filter(ordered_date__range=(from_date, to_date), ordered=True)
    new_users = User.objects.filter(date_joined__range=(from_date, to_date)).exclude(password="lkasjdhflksajdhflkLKJHLkdjhf")

    completed_orders = all_orders.filter(ordered=True, status="Delivered")
    canceled_orders = all_orders.filter(status="Canceled", ordered=True)
    not_canceled_orders = all_orders.filter(ordered=True).exclude(status="Canceled")



    all_revenue = 0
    for order in all_orders:
        all_revenue += order.get_order_total()


    completed_revenue = 0
    for order in not_canceled_orders:
        completed_revenue += order.get_order_total()


    

    

    buffer = BytesIO()
    workbook = xlsxwriter.Workbook(buffer)
    bold = workbook.add_format({'bold': True, "align": "left"})
    left = workbook.add_format({"align": "left"})

    worksheet = workbook.add_worksheet()
    letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"]

    merge_format = workbook.add_format({
    'bold': 1,
    'border': 1,
    'align': 'left',
    'valign': 'vcenter'})

    worksheet.set_column(0, 5, 17)
    
    worksheet.merge_range("A1:F1", f"General report from { from_date.strftime('%d-%m-%Y') } to { to_date.strftime('%d-%m-%Y') }", merge_format)

    worksheet.write("A2", "All Orders", bold)
    worksheet.write("B2", "Completed Orders", bold)
    worksheet.write("C2", "Canceled Orders", bold)
    worksheet.write("D2", "Done Revenue", bold)
    worksheet.write("E2", "Orders Total", bold)
    worksheet.write("F2", "New Users", bold)





    worksheet.write("A3",  all_orders.count(), left)
    worksheet.write("B3",  completed_orders.count(), left)
    worksheet.write("C3",  canceled_orders.count(), left)
    worksheet.write("D3",  str(completed_revenue) + " KD", left)
    worksheet.write("E3",  str(all_revenue) + " KD", left)
    worksheet.write("F3",  new_users.count(), left)




    workbook.close()
    buffer.seek(0)

    response = HttpResponse(buffer.read(),
                            content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename=general_income.xlsx'
    return response




@user_passes_test(check_admin, login_url="/dashboard/login")
def advanced_income_report(request):

    request_from_date = request.POST.get("advanced_income_from")
    from_year = request_from_date.split("/")[2]
    from_month = request_from_date.split("/")[1]
    from_day = request_from_date.split("/")[0]


    from_date = datetime.datetime(int(from_year), int(from_month), int(from_day))
    print(from_date, "FROM DATE")


    request_to_date = request.POST.get("advanced_income_to")
    to_year = request_to_date.split("/")[2]
    to_month = request_to_date.split("/")[1]
    to_day = request_to_date.split("/")[0]


    to_date = datetime.datetime(int(to_year), int(to_month), int(to_day), 23)
    print(to_date, "TO DATE")


    all_orders = Order.objects.filter(ordered_date__range=(from_date, to_date), ordered=True).order_by("status")
    

    buffer = BytesIO()
    workbook = xlsxwriter.Workbook(buffer)
    bold = workbook.add_format({'bold': True, "align": "left"})
    left = workbook.add_format({"align": "left"})
    worksheet = workbook.add_worksheet()
    letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"]
    num = 3
    
    worksheet.set_column(0, 2, 15)
    worksheet.set_column(3, 3, 30)
    worksheet.set_column(4, 6, 15)


    
    worksheet.write("A2", "Name", bold)
    worksheet.write("B2", "Date", bold)
    worksheet.write("C2", "Number", bold)
    worksheet.write("D2", "Location", bold)
    worksheet.write("E2", "Amount", bold)
    worksheet.write("F2", "Discount", bold)
    worksheet.write("G2", "Total after Discount", bold)
    worksheet.write("H2", "Delivery Fees", bold)
    worksheet.write("I2", "Grand Total", bold)
    worksheet.write("J2", "Qty", bold)
    worksheet.write("K2", "Status", bold)

    merge_format = workbook.add_format({
    'bold': 1,
    'border': 1,
    'align': 'left',
    'valign': 'vcenter'})

    
    worksheet.merge_range("A1:K1", f"Sales Report from { from_date.strftime('%d-%m-%Y') } to { to_date.strftime('%d-%m-%Y') }", merge_format)


    total_discount = 0
    total_amount = 0
    total_after_disc = 0
    total_delivery = 0
    total_gt = 0

    for order in all_orders:
        letters_num = 0
        worksheet.write(letters[letters_num] + str(num),  order.user_name, left)
        letters_num += 1
        worksheet.write(letters[letters_num] + str(num),  order.ordered_date.strftime("%d-%m-%Y"), left)
        letters_num += 1
        worksheet.write(letters[letters_num] + str(num),  order.phone_number, left)
        letters_num += 1
        worksheet.write(letters[letters_num] + str(num),  f"{ order.area }, Block: { order.block }, { order.street } St, House No. { order.house }", left)
        letters_num += 1
        worksheet.write(letters[letters_num] + str(num),  str(order.get_order_total()), left)
        total_amount += order.get_order_total()
        letters_num += 1

        # discuont
        discount = order.get_order_total() * decimal.Decimal(order.get_discount())
        discount = round(discount, 3)
        total_discount += discount
        worksheet.write(letters[letters_num] + str(num), str(discount), left)
        letters_num += 1

        # total after discuont
        total_after_discount = order.get_order_total() - discount
        total_after_discount = round(total_after_discount, 3)
        total_after_disc += total_after_discount
        worksheet.write(letters[letters_num] + str(num),  str(total_after_discount), left)
        letters_num += 1

        # delivery
        delivery = DeliveryCharge.objects.first().price if order.order_type == "Delivery" else 0
        total_delivery += delivery
        worksheet.write(letters[letters_num] + str(num),  str(delivery), left)
        letters_num += 1

        # grand total
        gtotal = total_after_discount + delivery
        total_gt += gtotal
        worksheet.write(letters[letters_num] + str(num),  str(gtotal), left)
        letters_num += 1

        worksheet.write(letters[letters_num] + str(num),  order.get_items_number(), left)
        letters_num += 1
        worksheet.write(letters[letters_num] + str(num),  order.status, left)
        letters_num += 1
        num += 1
        

    worksheet.write("A{}".format(num+1), "Total", bold)
    worksheet.write("E{}".format(num+1), str(total_amount), bold)
    worksheet.write("F{}".format(num+1), str(total_discount), bold)
    worksheet.write("G{}".format(num+1), str(total_after_disc), bold)
    worksheet.write("H{}".format(num+1), str(total_delivery), bold)
    worksheet.write("I{}".format(num+1), str(total_gt), bold)


    workbook.close()
    buffer.seek(0)

    response = HttpResponse(buffer.read(),
                            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=advanced_income.xlsx'
    print(response["Content-Disposition"])
    return response


@user_passes_test(check_admin, login_url="/dashboard/login")
def visits_report(request):

    request_from_date = request.POST.get("visits_from")
    from_year = request_from_date.split("/")[2]
    from_month = request_from_date.split("/")[1]
    from_day = request_from_date.split("/")[0]


    from_date = datetime.datetime(int(from_year), int(from_month), int(from_day))
    print(from_date, "FROM DATE")


    request_to_date = request.POST.get("visits_to")
    to_year = request_to_date.split("/")[2]
    to_month = request_to_date.split("/")[1]
    to_day = request_to_date.split("/")[0]


    to_date = datetime.datetime(int(to_year), int(to_month), int(to_day), 23)
    print(to_date, "TO DATE")



    
    visits = Visit.objects.filter(date__range=(from_date, to_date))
    

    buffer = BytesIO()
    workbook = xlsxwriter.Workbook(buffer)
    bold = workbook.add_format({'bold': True, "align": "left"})
    left = workbook.add_format({"align": "left"})

    worksheet = workbook.add_worksheet()
    letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"]

    worksheet.set_column(0, 0, 10)

    
    worksheet.write("A2", "Visits", bold)

    merge_format = workbook.add_format({
    'bold': 1,
    'border': 1,
    'align': 'left',
    'valign': 'vcenter'})

    
    worksheet.merge_range("A1:F1", f"Visits report from { from_date.strftime('%d-%m-%Y') } to { to_date.strftime('%d-%m-%Y') }", merge_format)
    


    worksheet.write("A3",  visits.count(), left)





    workbook.close()
    buffer.seek(0)

    response = HttpResponse(buffer.read(),
                            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=visits.xlsx'
    print(response["Content-Disposition"])
    return response






@user_passes_test(check_admin, login_url="/dashboard/login")
def users_report(request):

    request_from_date = request.POST.get("users_from")
    from_year = request_from_date.split("/")[2]
    from_month = request_from_date.split("/")[1]
    from_day = request_from_date.split("/")[0]


    from_date = datetime.datetime(int(from_year), int(from_month), int(from_day))
    print(from_date, "FROM DATE")


    request_to_date = request.POST.get("users_to")
    to_year = request_to_date.split("/")[2]
    to_month = request_to_date.split("/")[1]
    to_day = request_to_date.split("/")[0]


    to_date = datetime.datetime(int(to_year), int(to_month), int(to_day), 23)
    print(to_date, "TO DATE")



    
    users = User.objects.filter(date_joined__range=(from_date, to_date)).exclude(password="lkasjdhflksajdhflkLKJHLkdjhf").exclude(profile=None).exclude(is_superuser=True).exclude(is_staff=True)
    

    buffer = BytesIO()
    workbook = xlsxwriter.Workbook(buffer)
    bold = workbook.add_format({'bold': True, "align": "left"})
    left = workbook.add_format({"align": "left"})

    worksheet = workbook.add_worksheet()
    letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"]


    worksheet.set_column(0, 1, 15)
    worksheet.set_column(2, 3, 30)
    worksheet.set_column(4, 4, 15)

    
    worksheet.write("A2", "Name", bold)
    worksheet.write("B2", "Phone Number", bold)
    worksheet.write("C2", "Email", bold)
    worksheet.write("D2", "location", bold)
    worksheet.write("E2", "Joined Date", bold)


    merge_format = workbook.add_format({
    'bold': 1,
    'border': 1,
    'align': 'left',
    'valign': 'vcenter'})

    
    worksheet.merge_range("A1:E1", f"Users report from { from_date.strftime('%d-%m-%Y') } to { to_date.strftime('%d-%m-%Y') }", merge_format)


    num = 3

    for user in users:
        print(user)
        letters_num = 0

        worksheet.write(letters[letters_num] + str(num),  user.profile.name, left)
        letters_num += 1
        worksheet.write(letters[letters_num] + str(num),  user.profile.phone_number, left)
        letters_num += 1
        worksheet.write(letters[letters_num] + str(num),  user.email, left)
        letters_num += 1
        worksheet.write(letters[letters_num] + str(num),  f"{ user.profile.area }, Block: { user.profile.block }, { user.profile.street } St, House No. { user.profile.house }", left)
        letters_num += 1
        worksheet.write(letters[letters_num] + str(num),  user.date_joined.strftime("%d-%m-%Y"), left)
        letters_num += 1

        num += 1





    workbook.close()
    buffer.seek(0)

    response = HttpResponse(buffer.read(),
                            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=users.xlsx'
    print(response["Content-Disposition"])
    return response


@user_passes_test(check_admin, login_url="/dashboard/login")
def reports(request):


    return render(request, "dashboard/reports.html")


@user_passes_test(check_admin, login_url="/dashboard/login")
def off_dates(request):
    
    dates = DateOff.objects.all()

    context = {
        "dates": dates
    }

    return render(request, "dashboard/off_dates.html", context)


@user_passes_test(check_admin, login_url="/dashboard/login")
def set_date_off(request):

   

    form = SetDateOffForm

    

    if request.method == "POST":
        print(request.POST.get("date"))
        from_dates = request.POST.get("date").split(" - ")[0].split("/")
        to_dates = request.POST.get("date").split(" - ")[1].split("/")

        print(from_dates)
        print(to_dates)

        print(from_dates[0])
        print(from_dates[1])
        print(from_dates[2])


        from_date = datetime.datetime(int(from_dates[2]), int(from_dates[0]), int(from_dates[1]))
        to_date = datetime.datetime(int(to_dates[2]), int(to_dates[0]), int(to_dates[1]))

        print(from_date)


        DateOff.objects.create(from_date=from_date, to_date=to_date)

        return redirect("dashboard:off-dates")




    context = {
        "form": form
    }

    return render(request, "dashboard/set_date_off.html", context)

@user_passes_test(check_admin, login_url="/dashboard/login")
def update_date_off(request, id):

    form = SetDateOffForm

    date_off = DateOff.objects.get(id=id)

    if request.method == "POST":
        from_dates = request.POST.get("date").split(" - ")[0].split("/")
        to_dates = request.POST.get("date").split(" - ")[1].split("/")

       


        from_date = datetime.datetime(int(from_dates[2]), int(from_dates[0]), int(from_dates[1]))
        to_date = datetime.datetime(int(to_dates[2]), int(to_dates[0]), int(to_dates[1]))



        date_off.from_date = from_date
        date_off.to_date = to_date
        date_off.save()



        return redirect("dashboard:off-dates")




    context = {
        "form": form
    }

    return render(request, "dashboard/set_date_off.html", context)



@user_passes_test(check_admin, login_url="/dashboard/login")
def delete_date_off(request, id):
    date_off = DateOff.objects.get(id=id)
    date_off.delete()

    return redirect("dashboard:off-dates")

class UpdatePopUp(UserPassesTestMixin, UpdateView):
    
    

    def test_func(self):
        return self.request.user.is_superuser
    

    def get_object(self):
        return PopUp.objects.first()


    login_url = "/dashboard/login"


    success_url = "/dashboard"

    

    model = PopUp
    form_class = PopUpForm
    template_name = "dashboard/update_pop_up.html"



class CreateCoupon(UserPassesTestMixin, CreateView):

    login_url="/dashboard/login"

    def test_func(self):
        return self.request.user.is_superuser
    

    model = Coupon
    form_class = CreateCouponForm
    template_name = "dashboard/create_coupon.html"



class UpdateCoupon(UserPassesTestMixin, UpdateView):

    login_url="/dashboard/login"

    def test_func(self):
        return self.request.user.is_superuser
    

    model = Coupon
    form_class = CreateCouponForm
    template_name = "dashboard/create_coupon.html"



@user_passes_test(check_admin, login_url="/dashboard/login")
def coupons_list(request):
    
    coupons = Coupon.objects.all()

    context = {
        "coupons": coupons
    }

    return render(request, "dashboard/coupons_list.html", context)




@user_passes_test(check_admin, login_url="/dashboard/login")
def delete_coupon(request, id):
    
    coupon = Coupon.objects.get(id=id)

    coupon.delete()

    return redirect('dashboard:coupons-list')





class UpdateDiscount(UserPassesTestMixin, UpdateView):
    
    login_url = "/dashboard/login"


    success_url = "/dashboard"

    def test_func(self):
        return self.request.user.is_superuser
    

    def get_object(self):
        return Discount.objects.first()

    model = Discount
    form_class = DiscountForm
    template_name = "dashboard/discount.html"





def colors_list(request):

    colors = Color.objects.all()
    
    context = {
        "colors": colors
    }


    return render(request, "dashboard/colors_list.html", context)



class CreateColor(UserPassesTestMixin, CreateView):
    

    login_url = "/dashboard/login"


    success_url = "/dashboard/colors"

    def test_func(self):
        return self.request.user.is_superuser
    

    

    model = Color
    form_class = ColorForm
    template_name = "dashboard/create_color.html"


class UpdateColor(UserPassesTestMixin, UpdateView):
    

    login_url = "/dashboard/login"


    success_url = "/dashboard/colors"

    def test_func(self):
        return self.request.user.is_superuser


    model = Color
    form_class = ColorForm
    template_name = "dashboard/create_color.html"

def delete_color(request, pk):

    if Color.objects.filter(id=pk).exists():
        color = Color.objects.get(id=pk)
        color.delete()
        
    return redirect("dashboard:colors-list")



def flavors_list(request):

    flavors = Flavor.objects.all()
    
    context = {
        "flavors": flavors
    }


    return render(request, "dashboard/flavors_list.html", context)



class CreateFlavor(UserPassesTestMixin, CreateView):
    

    login_url = "/dashboard/login"


    success_url = "/dashboard/flavors"

    def test_func(self):
        return self.request.user.is_superuser
    

    

    model = Flavor
    form_class = FlavorForm
    template_name = "dashboard/create_flavor.html"


class UpdateFlavor(UserPassesTestMixin, UpdateView):
    

    login_url = "/dashboard/login"


    success_url = "/dashboard/flavors"

    def test_func(self):
        return self.request.user.is_superuser


    model = Flavor
    form_class = FlavorForm
    template_name = "dashboard/create_flavor.html"



def delete_flavor(request, pk):

    if Flavor.objects.filter(id=pk).exists():
        flavor = Flavor.objects.get(id=pk)
        flavor.delete()
        
    return redirect("dashboard:flavors-list")