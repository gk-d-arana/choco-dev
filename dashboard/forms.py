from re import A
from django import forms
from django.contrib.auth.models import User
from django.forms import widgets
from app.models import Color, Discount, Coupon, Choice, DateOff, DeliveryCharge, Flavor, Item, Category, WorkingDay, AboutUsContent, AboutUsMainImage, AboutUsTopic, ChocolateType, Box, SubCategory, Tray, Flower, Ribbon, Topper, Card, MinimumOrder, BoxItem, BannerImage, Package, PopUp



class PopUpForm(forms.ModelForm):

    class Meta:
        model = PopUp
        fields = "__all__"
        widgets = {
            "picture": forms.FileInput(attrs={"class": "custom-file-input"}),
            "link": forms.URLInput(attrs={"class": "form-control", "placeholder": "Link"}),
            "ar_link": forms.URLInput(attrs={"class": "form-control", "placeholder": "Ar Link"}),

            "text": forms.TextInput(attrs={"class": "form-control", "placeholder": "Text"}),
            "ar_text": forms.TextInput(attrs={"class": "form-control", "placeholder": "Ar Text"}),
            "disabled": forms.CheckboxInput(attrs={"class": "custom-control-input", "id": "customCheckBox1"}),
            "has_link": forms.CheckboxInput(attrs={"class": "custom-control-input", "id": "customCheckBox2"})
        }


class PackageForm(forms.ModelForm):

    class Meta:
        model = Package
        fields = "__all__"
        widgets = {
            "en_name": forms.TextInput(attrs={"class": "form-control", "placeholder": "Name"}),
            "ar_name": forms.TextInput(attrs={"class": "form-control", "placeholder": "Ar Name"}),


            "weight": forms.TextInput(attrs={"class": "form-control", "placeholder": "Weight"}),
            "pieces": forms.TextInput(attrs={"class": "form-control", "placeholder": "Pieces"}),


            "en_unit": forms.Select(attrs={"class": "form-control"}),
            "ar_unit": forms.Select(attrs={"class": "form-control"}),

            
            "subcategory": forms.Select(attrs={"class": "form-control"}),


            



            "price": forms.TextInput(attrs={"class": "form-control", "placeholder": "Price"}),
            
            "image": forms.FileInput(attrs={"class": "custom-file-input"}),

        }


class ChoiceForm(forms.ModelForm):
    
    class Meta:
        model = Choice
        fields = "__all__"
        widgets = {
            "en_choice": forms.TextInput(attrs={"class": "form-control"}),
            "ar_choice": forms.TextInput(attrs={"class": "form-control"}),

        }


class BannerImageForm(forms.ModelForm):

    class Meta:
        model = BannerImage
        fields = "__all__"
        widgets = {
            "image1": forms.FileInput(attrs={"class": "custom-file-input"}),
            "image2": forms.FileInput(attrs={"class": "custom-file-input"}),
            "image3": forms.FileInput(attrs={"class": "custom-file-input"}),
            "image4": forms.FileInput(attrs={"class": "custom-file-input"}),
            "image5": forms.FileInput(attrs={"class": "custom-file-input"}),
        }



class BoxItemForm(forms.ModelForm):

    class Meta:
        model = BoxItem
        fields = ["title", "ar_title", "price", "image", "type"]
        widgets = {
            "title": forms.TextInput(attrs={"class": "form-control", "placeholder": "Name"}),
            "ar_title": forms.TextInput(attrs={"class": "form-control", "placeholder": "Ar Name"}),

            

            "price": forms.TextInput(attrs={"class": "form-control", "placeholder": "Price"}),
            
            "image": forms.FileInput(attrs={"class": "custom-file-input"}),

            "type": forms.Select(attrs={"class": "form-control"})
           


        }


class MinimumOrderForm(forms.ModelForm):

    class Meta:
        model = MinimumOrder
        fields = "__all__"
        widgets = {
            "price": forms.TextInput(attrs={"class": "form-control", "placeholder": "Price"}),
        }


class DeliveryChargeForm(forms.ModelForm):

    class Meta:
        model = DeliveryCharge
        fields = "__all__"
        widgets = {
            "price": forms.TextInput(attrs={"class": "form-control", "placeholder": "Price"}),
        }


class DiscountForm(forms.ModelForm):

    class Meta:
        model = Discount
        fields = "__all__"
        widgets = {
            "value": forms.TextInput(attrs={"class": "form-control", "placeholder": "Value"}),
            "start_date": forms.DateInput(attrs={"class": "form-control", "type": "date"}),
            "end_date": forms.DateInput(attrs={"class": "form-control", "type": "date"}),
        }


class ItemForm(forms.ModelForm):

    class Meta:
        model = Item
        fields = ["title", "ar_title", "description", "ar_description", "price", "category", "sub_category", "chocolate_type", "picture", "picture2", "picture3", "quantity_type", "has_choices", "choices", "has_flavors", "has_colors", "colors", "flavors", "width", "height", "length", "diameter", "hidden", "sold_out", "not_available", "coming_soon", "weight_maximum", "weight_minimum", "pieces_maximum", "pieces_minimum", "delivery_date"]
        widgets = {
            "title": forms.TextInput(attrs={"class": "form-control", "placeholder": "Name"}),
            "ar_title": forms.TextInput(attrs={"class": "form-control", "placeholder": "Ar Name"}),

            


            "description": forms.Textarea(attrs={"class": "form-control", "placeholder": "Description"}),
            "ar_description": forms.Textarea(attrs={"class": "form-control", "placeholder": "Ar Description"}),

            "price": forms.TextInput(attrs={"class": "form-control", "placeholder": "Price"}),
            # "index": forms.TextInput(attrs={"class": "form-control", "placeholder": "Number"}),

            "category": forms.Select(attrs={"class": "form-control"}),
            "chocolate_type": forms.Select(attrs={"class": "form-control"}),

            "delivery_date": forms.Select(attrs={"class": "form-control"}),
            

            

            "quantity_type": forms.Select(attrs={"class": "form-control"}),

            "weight_maximum": forms.TextInput(attrs={"class": "form-control", "placeholder": "Weight Maximum"}),
            "weight_minimum": forms.TextInput(attrs={"class": "form-control", "placeholder": "Weight Minimum"}),

            "pieces_maximum": forms.TextInput(attrs={"class": "form-control", "placeholder": "Pieces Maximum"}),
            "pieces_minimum": forms.TextInput(attrs={"class": "form-control", "placeholder": "Pieces Minimum"}),


            "sub_category": forms.Select(attrs={"class": "form-control"}),
            "picture": forms.FileInput(attrs={"class": "custom-file-input"}),
            "picture2": forms.FileInput(attrs={"class": "custom-file-input"}),
            "picture3": forms.FileInput(attrs={"class": "custom-file-input"}),

            "has_choices": forms.CheckboxInput(attrs={"class": "custom-control-input", "id": "customCheckBox1"}),
            "choices": forms.SelectMultiple(attrs={"class": "form-control"}),

            
            
            "has_colors": forms.CheckboxInput(attrs={"class": "custom-control-input", "id": "colorsCheckBox"}),
            "colors": forms.SelectMultiple(attrs={"class": "form-control"}),

            "has_flavors": forms.CheckboxInput(attrs={"class": "custom-control-input", "id": "flavorsCheckBox"}),
            "flavors": forms.SelectMultiple(attrs={"class": "form-control"}),


            "hidden": forms.CheckboxInput(attrs={"class": "custom-control-input", "id": "customCheckBox2"}),
            "sold_out": forms.CheckboxInput(attrs={"class": "custom-control-input", "id": "customCheckBox3"}),
            "not_available": forms.CheckboxInput(attrs={"class": "custom-control-input", "id": "customCheckBox4"}),
            "coming_soon": forms.CheckboxInput(attrs={"class": "custom-control-input", "id": "customCheckBox5"}),



        


            "width": forms.TextInput(attrs={"class": "form-control", "placeholder": "Width"}),
            "height": forms.TextInput(attrs={"class": "form-control", "placeholder": "Height"}),
            "length": forms.TextInput(attrs={"class": "form-control", "placeholder": "Length"}),
            "diameter": forms.TextInput(attrs={"class": "form-control", "placeholder": "Diameter"}),


            
        }

class SubCatItemForm(forms.ModelForm):

    class Meta:
        model = Item
        fields = ["title", "ar_title", "description", "ar_description", "price", "category", "sub_category", "chocolate_type", "picture", "picture2", "picture3", "quantity_type", "has_choices", "choices", "width", "height", "length", "diameter", "hidden", "sold_out", "not_available", "coming_soon", "weight_maximum", "weight_minimum", "pieces_maximum", "pieces_minimum", "delivery_date", "index"]
        widgets = {
            "title": forms.TextInput(attrs={"class": "form-control", "placeholder": "Name"}),
            "ar_title": forms.TextInput(attrs={"class": "form-control", "placeholder": "Ar Name"}),

            


            "description": forms.Textarea(attrs={"class": "form-control", "placeholder": "Description"}),
            "ar_description": forms.Textarea(attrs={"class": "form-control", "placeholder": "Ar Description"}),

            "price": forms.TextInput(attrs={"class": "form-control", "placeholder": "Price"}),
            "index": forms.TextInput(attrs={"class": "form-control", "placeholder": "Number"}),

            "category": forms.Select(attrs={"class": "form-control"}),
            "chocolate_type": forms.Select(attrs={"class": "form-control"}),

            "delivery_date": forms.Select(attrs={"class": "form-control"}),
            

            

            "quantity_type": forms.Select(attrs={"class": "form-control"}),

            "weight_maximum": forms.TextInput(attrs={"class": "form-control", "placeholder": "Weight Maximum"}),
            "weight_minimum": forms.TextInput(attrs={"class": "form-control", "placeholder": "Weight Minimum"}),

            "pieces_maximum": forms.TextInput(attrs={"class": "form-control", "placeholder": "Pieces Maximum"}),
            "pieces_minimum": forms.TextInput(attrs={"class": "form-control", "placeholder": "Pieces Minimum"}),


            "sub_category": forms.Select(attrs={"class": "form-control"}),
            "image": forms.FileInput(attrs={"class": "custom-file-input"}),
            "image2": forms.FileInput(attrs={"class": "custom-file-input"}),
            "image3": forms.FileInput(attrs={"class": "custom-file-input"}),

            "has_choices": forms.CheckboxInput(attrs={"class": "custom-control-input", "id": "customCheckBox1"}),
            "choices": forms.SelectMultiple(attrs={"class": "form-control"}),

            "hidden": forms.CheckboxInput(attrs={"class": "custom-control-input", "id": "customCheckBox2"}),
            "sold_out": forms.CheckboxInput(attrs={"class": "custom-control-input", "id": "customCheckBox3"}),
            "not_available": forms.CheckboxInput(attrs={"class": "custom-control-input", "id": "customCheckBox4"}),
            "coming_soon": forms.CheckboxInput(attrs={"class": "custom-control-input", "id": "customCheckBox5"}),



        


            "width": forms.TextInput(attrs={"class": "form-control", "placeholder": "Width"}),
            "height": forms.TextInput(attrs={"class": "form-control", "placeholder": "Height"}),
            "length": forms.TextInput(attrs={"class": "form-control", "placeholder": "Length"}),
            "diameter": forms.TextInput(attrs={"class": "form-control", "placeholder": "Diameter"}),


            
        }


class CategoryForm(forms.ModelForm):
    
    class Meta:
        model = Category
        fields = ['en_title', 'ar_title', 'number', "image", "banner_image", "hidden", "has_dimensions"]
        widgets = {
            "en_title": forms.TextInput(attrs={"class": "form-control", "placeholder": "En Title", "required": ""}),
            "ar_title": forms.TextInput(attrs={"class": "form-control", "placeholder": "Ar Title", "required": ""}),
            "number": forms.TextInput(attrs={"class": "form-control", "placeholder": "Number", "required": ""}),
            "image": forms.FileInput(attrs={"class": "custom-file-input", "required": ""}),
            "hidden": forms.CheckboxInput(attrs={"class": "custom-control-input", "id": "customCheckBox1"}),
            "has_dimensions": forms.CheckboxInput(attrs={"class": "custom-control-input", "id": "customCheckBox2"}),


            "banner_image": forms.FileInput(attrs={"class": "custom-file-input", "required": ""}),

        }


class CategoryItem(forms.ModelForm):

    class Meta:
        model = Item
        fields = ("title", "description", "price", "picture")
        widgets = {
            "title": forms.TextInput(attrs={"class": "form-control"}),
            "description": forms.Textarea(attrs={"class": "form-control"}),
            "price": forms.TextInput(attrs={"class": "form-control"}),
            "picture": forms.FileInput(attrs={"class": "custom-file-input"}),
        }


class WorkingHoursForm(forms.ModelForm):
    
    class Meta:
        model = WorkingDay
        fields = ("from_time1", "to_time1", "disabled_time1", "from_time2", "to_time2", "disabled_time2", "from_time3", "to_time3", "disabled_time3",)
        widgets = {
            "from_time1": forms.TimeInput(attrs={"type": "time", "class": "form-control"}),
            "to_time1": forms.TimeInput(attrs={"type": "time", "class": "form-control"}),
            "disabled_time1": forms.CheckboxInput(attrs={"class": "custom-control-input", "id": "customCheckBox1"}),

            "from_time2": forms.TimeInput(attrs={"type": "time", "class": "form-control"}),
            "to_time2": forms.TimeInput(attrs={"type": "time", "class": "form-control"}),
            "disabled_time2": forms.CheckboxInput(attrs={"class": "custom-control-input", "id": "customCheckBox2"}),

            "from_time3": forms.TimeInput(attrs={"type": "time", "class": "form-control"}),
            "to_time3": forms.TimeInput(attrs={"type": "time", "class": "form-control"}),
            "disabled_time3": forms.CheckboxInput(attrs={"class": "custom-control-input", "id": "customCheckBox3"}),

        }





class AboutUsContentForm(forms.ModelForm):


    class Meta:
        model = AboutUsContent
        fields = "__all__"
        widgets = {
            "main_text": forms.Textarea(attrs={"class": "form-control"}),
            "ar_main_text": forms.Textarea(attrs={"class": "form-control"}),

            "secondary_text": forms.Textarea(attrs={"class": "form-control"}),
            "ar_secondary_text": forms.Textarea(attrs={"class": "form-control"}),

            "image": forms.FileInput(attrs={"class": "custom-file-input"})
        }

class AboutMainImageForm(forms.ModelForm):


    class Meta:
        model = AboutUsMainImage
        fields = "__all__"
        widgets = {
            "image": forms.FileInput(attrs={"class": "custom-file-input"})

        }


class AboutTopicForm(forms.ModelForm):

    class Meta:
        model = AboutUsTopic
        fields = ["title", "paragraph", "ar_title", "ar_paragraph", "number"]
        widgets = {
            "title": forms.TextInput(attrs={"class": "form-control"}),
            "ar_title": forms.TextInput(attrs={"class": "form-control"}),
            "number": forms.TextInput(attrs={"class": "form-control"}),


            "paragraph": forms.Textarea(attrs={"class": "form-control"}),
            "ar_paragraph": forms.Textarea(attrs={"class": "form-control"}),
        
        }
    


class ChocolateTypeForm(forms.ModelForm):

    class Meta:
        model = ChocolateType
        fields = "__all__"
        widgets = {
            "title": forms.TextInput(attrs={"class": "form-control"}),
            "image": forms.FileInput(attrs={"class": "custom-file-input"})
        }
        



class BoxForm(forms.ModelForm):

    class Meta:
        model = Box
        fields = "__all__"
        widgets = {
            "name": forms.TextInput(attrs={"class": "form-control"}),
            "picture": forms.FileInput(attrs={"class": "custom-file-input"})
        }





class UserLogin(forms.Form):

   username = forms.CharField(max_length=100, widget=forms.TextInput(attrs={"class": "form-control", "placeholder": "Username"}))
   password = forms.CharField(max_length=100, widget=forms.PasswordInput(attrs={"class": "form-control", "placeholder": "Password"}))



class SubCategoryForm(forms.ModelForm):

    class Meta:
        model = SubCategory
        fields = ["en_title", "ar_title", "number", "coming_soon"]
        widgets = {
            "en_title": forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter the title"}),
            "ar_title": forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter the title"}),
            "number": forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter the number"}),
            "coming_soon": forms.CheckboxInput(attrs={"class": "custom-control-input", "id": "customCheckBox1"}),
        }




class TrayForm(forms.ModelForm):


    class Meta:
        model = Tray
        fields = ["en_name", "ar_name", "price", "image", "image", "weight", "unit", "height", "width", "length", "type", "delivery_date"]
        widgets = {
            "en_name": forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter the name"}),
            "ar_name": forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter the arabic name"}),

            "price": forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter the price"}),
            "image": forms.FileInput(attrs={"class": "custom-file-input"}),
            "unit": forms.Select(attrs={"class": "form-control"}),
            "weight": forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter the weight"}),
            "height": forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter the heigth"}),
            "width": forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter the width"}),
            "length": forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter the length"}),

            "type": forms.Select(attrs={"class": "form-control"}),
            "delivery_date": forms.Select(attrs={"class": "form-control"}),




        }



class FlowerForm(forms.ModelForm):


    class Meta:
        model = Flower
        fields = ["en_name", "ar_name", "image", "price"]
        widgets = {
            "en_name": forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter the name"}),
            "ar_name": forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter the name"}),

            "price": forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter the price"}),
            "image": forms.FileInput(attrs={"class": "custom-file-input"}),

        }


class RibbonForm(forms.ModelForm):


    class Meta:
        model = Ribbon
        fields = ["en_name", "image", "price"]
        widgets = {
            "en_name": forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter the name"}),
            "price": forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter the price"}),
            "image": forms.FileInput(attrs={"class": "custom-file-input"}),

        }



class TopperForm(forms.ModelForm):


    class Meta:
        model = Topper
        fields = ["en_name", "ar_name", "image", "price"]
        widgets = {
            "en_name": forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter the name"}),
            "ar_name": forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter the name"}),

            "price": forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter the price"}),
            "image": forms.FileInput(attrs={"class": "custom-file-input"}),

        }



class CardForm(forms.ModelForm):


    class Meta:
        model = Card
        fields = ["en_name", "ar_name", "image", "price"]
        widgets = {
            "en_name": forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter the name"}),
            "ar_name": forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter the name"}),

            "price": forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter the price"}),
            "image": forms.FileInput(attrs={"class": "custom-file-input"}),
        }



class SetDateOffForm(forms.ModelForm):


    class Meta:
        model = DateOff
        fields = "__all__"

        widgets = {
            "date": forms.DateInput(attrs={"class": "form-control input-daterange-datepicker"})
        }



class CreateCouponForm(forms.ModelForm):

    class Meta:
        model = Coupon
        fields = "__all__"



        widgets = {
            "code": forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter the Code"}),
            "value": forms.TextInput(attrs={"class": "form-control", "placeholder": "Enter the Value"}),
            "end_date": forms.DateInput(attrs={"class": "form-control", "type": "date"})
        }



class FlavorForm(forms.ModelForm):

    class Meta:
        model = Flavor
        fields = "__all__"


        widgets = {
            "en_name": forms.TextInput(attrs={"class": "form-control", "placeholder": "En Flavor"}),
            "ar_name": forms.TextInput(attrs={"class": "form-control", "placeholder": "Ar Flavor"}),
            "index": forms.TextInput(attrs={"class": "form-control", "placeholder": "Number"}),

        }


class ColorForm(forms.ModelForm):

    class Meta:
        model = Color
        fields = "__all__"


        widgets = {
            "en_name": forms.TextInput(attrs={"class": "form-control", "placeholder": "En Color"}),
            "ar_name": forms.TextInput(attrs={"class": "form-control", "placeholder": "Ar Color"}),
            "index": forms.TextInput(attrs={"class": "form-control", "placeholder": "Number"}),

        }